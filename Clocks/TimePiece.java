
/**
 * An interface for interacting with <code>Clocks</code>.
 * @see Clock
 * @see Time
 *@author Prince Kannah
 */
public interface TimePiece {
	
	/**
	 * Resets the <code>TimePiece</code> to its starting position.
	 */
	public void reset();
	
	/**
	 * Stimulates one tick (second) of time passing.
	 */
	public void tick();
	
	/**
	 * Displays the current time for this <code>TimePiece</code>.
	 */
	public void display();

}
