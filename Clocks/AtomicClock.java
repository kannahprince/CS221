/**
 * The <code>AtomicClock</code> class is a type of <code>Clock</code>.
 * @see Clock
 * @see ClockType
 *@author Prince Kannah
 */

public class AtomicClock extends Clock{

	/**
	 * Constructor for the AtomicClock Class.
	 * @param time :the start time for the clock
	 */
	public AtomicClock(Time time) {
		super(ClockType.QUANTUM,0.0, time);
	}

	@Override
	public void display() {
		System.out.printf("Atomic Clock time [");
		time.displayTime();
		System.out.printf("]-total drift = %s%s",time.totalDrift(), " seconds\n");
	}
}
