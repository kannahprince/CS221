/**
 * The <code>Sundial</code> class is a type of <code>Clock</code>.
 * @see Clock
 * @see ClockType
 *@author Prince Kannah
 */

public class Sundial extends Clock{

	/**
	 * Constructor for the Sundial Class.
	 * @param time :the start time for the clock
	 */
	public Sundial(Time time) {
		super(ClockType.NATURAL, 0.0, time);
	}

	@Override
	public void display() {
		System.out.printf("Sundial Clock time [");
		time.displayTime();
		System.out.printf("]-total drift = %s%s",time.totalDrift(), " seconds\n");
	}

}
