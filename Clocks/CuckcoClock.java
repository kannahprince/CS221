/**
 * The <code>CuckcoClock</code> class is a type of <code>Clock</code>.
 * @see Clock
 * @see ClockType
 *@author Prince Kannah
 */

public class CuckcoClock extends Clock{
	
	/**
	 * Constructor for the CuckcoClock Class.
	 * @param time :the start time for the clock
	 */
	public CuckcoClock(Time time) {
		super(ClockType.MECHANICAL, 0.000694444, time);
	}

	@Override
	public void display() {
		System.out.printf("CuckcoClock Clock time [");
		time.displayTime();
		System.out.printf("]-total drift = ");
		System.out.printf("%.4f %s\n", time.totalDrift(),"seconds");
	}
	

}
