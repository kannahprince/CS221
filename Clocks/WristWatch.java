/**
 * The <code>WristWatch</code> class is a type of <code>Clock</code>.
 * @see Clock
 * @see ClockType
 *@author Prince Kannah
 */

public class WristWatch extends Clock{

	/**
	 * Constructor for the WristWatch Class.
	 * @param time :the start time for the clock
	 */
	public WristWatch(Time time) {
		super(ClockType.DIGITAL, 0.000034722, time);
	}

	@Override
	public void display() {
		System.out.printf("Wrist watch time [");
		time.displayTime();
		System.out.printf("]-total drift = ");
		System.out.printf("%.4f %s\n", time.totalDrift(),"seconds");
	}

}
