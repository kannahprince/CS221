
/**
 * <code>Clock</code> can be use to represent a variety of clock types and their drifts - time the clock looses as it runs. It implements the TimePiece interface and 
 * uses the <code>Time</code> class to represent time.
 * @see TimePiece
 * @see Time
 *@author Prince Kannah
 */

public abstract class Clock implements TimePiece{
	
	/**
	 * Use to identify the kind of clock being created
	 */
	public enum ClockType  {
		NATURAL, MECHANICAL, DIGITAL, QUANTUM
	}
	
	//clock properties
	private ClockType clockType;
	private double driftPerSecond;
	protected Time time;

	/**
	 * Constructor for <code>Clock</code>
	 * @param type :The type of clock
	 * @param time :The start time
	 * @param drift :This clock's drift per seconds
	 */
	
	public Clock(ClockType type, double drift, Time time){
		setClockType(type);
		setDrift(drift);
		this.time = new Time(time.hour(), time.minute(), time.second(), time.secondsPerTick(),drift);
	}
	
	/**
	 * Sets the drift of this <code>Clock</code> to the specified drift per seconds.
	 * @param drift :drift per seconds to use for this <cod>Clock</code>.
	 */
	public void setDrift(double drift){
		driftPerSecond = drift;
	}
	
	/**
	 * Sets the clock type for this <code>Clock</code> object.
	 * @param type :the kind the type of clock this <code>Clock</code> object represents
	 */
	public void setClockType(ClockType type){
		clockType = type;
	}
	
	/**
	 * Gets the clock type of this <code>Clock</code>.
	 * @return :this <code>Clock's</code> type
	 */
	public ClockType getClockType(){
		return clockType;
	}
	
	/**
	 * Gets the drift of this <code>Clock</code>
	 * @return :this <code>Clock's</code> drift per second
	 */
	public double getDrift(){
		return driftPerSecond;
	}

	@Override
	public void reset() {
		time.resetTime(0);
	}

	@Override
	public void tick() {
		time.incrementTime();
	}
	
}
