/**
 * The <code>GrandfatherClock</code> class is a type of <code>Clock</code>.
 * @see Clock
 * @see ClockType
 *@author Prince Kannah
 */

public class GrandfatherClock extends Clock{

	/**
	 * Constructor for the GrandfatherClock Class.
	 * @param time :the start time for the clock
	 */
	public GrandfatherClock(Time time) {
		super(ClockType.MECHANICAL, 0.000347222, time);
	}

	@Override
	public void display() {
		System.out.printf("Grandfather Clock time [");
		time.displayTime();
		System.out.printf("]-total drift = ");
		System.out.printf("%.4f %s\n", time.totalDrift(),"seconds");	
	}

}
