public class ClockSimulation {

	private static Bag<Clock> clocks;
	private static final int DAY_SECONDS = 86400;
	private static final int WEEK_SECONDS = 604800;
	private static final int MONTH_SECONDS = 2592000;
	private static final int YEAR_SECONDS = 3153600;
	/**
	 * Runs the clock simulation for 1 day, week, month and year.
	 * @param args command-line arguments
	 */
	public static void main(String[] args) {
		initBag(); //setup bag with clocks in them
		
		System.out.print("******************\n");
		System.out.println("*ONE DAY LATER...*");
		System.out.println("******************");

		for (long p = 0; p < DAY_SECONDS; p++) {
			for (int j = 0; j < clocks.size(); j++) {
				clocks.retrieve(j).tick();
			}
		}
		displayClocks();

		System.out.print("*******************\n");
		System.out.println("*ONE WEEK LATER...*");
		System.out.println("*******************");
		for (long p = 0; p < WEEK_SECONDS; p++) {
			for (int j = 0; j < clocks.size(); j++) {
				clocks.retrieve(j).tick();
			}
		}
		displayClocks();

		System.out.print("********************\n");
		System.out.println("*ONE MONTH LATER...*");
		System.out.println("********************");

		for (long p = 0; p < MONTH_SECONDS; p++) {
			for (int j = 0; j < clocks.size(); j++) {
				clocks.retrieve(j).tick();
			}
		}
		displayClocks();

		System.out.print("*******************\n");
		System.out.println("*ONE YEAR LATER...*");
		System.out.println("*******************");

		for (long p = 0; p < YEAR_SECONDS; p++) {
			for (int j = 0; j < clocks.size(); j++) {
				clocks.retrieve(j).tick();
			}
		}
		displayClocks();	
	}

	/**
	 * Sets up bag of clocks.
	 */
	private static void initBag() {
		clocks = new Bag<Clock>();
		clocks.insert(new Sundial(new Time()));
		clocks.insert(new AtomicClock(new Time()));
		clocks.insert(new WristWatch(new Time()));
		clocks.insert(new GrandfatherClock(new Time()));
		clocks.insert(new CuckcoClock(new Time()));
	}
	
	/**
	 * Calls the display() method of each clock.
	 */
	private static void displayClocks() {
		for (int i = 0; i < clocks.size(); i++) {
			clocks.retrieve(i).display();
			System.out.println();
		}
		resetClocks();
	}
	
	/**
	 * Calls the reset() & setTotalDrift() method of each clock.
	 */
	private static void resetClocks(){
		for(int i = 0; i < clocks.size(); i++){
			clocks.retrieve(i).reset();
			clocks.retrieve(i).time.setTotalDrift(0);
		}
	}

}
