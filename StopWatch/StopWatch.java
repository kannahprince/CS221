import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * A window application simulating a stop watch.
 * @author Matt T
 * @version summer 2015
 */
public class StopWatch extends JFrame
{

	private static final long serialVersionUID = 2481273590514578402L;
	// main panel the other panels are added to
	private JPanel mainPanel;
	// panel for organizing the buttons in the application
	private JPanel buttonPanel;
	// panel for organizing labels and seven-segment displays
	private JPanel watchPanel;
	// seven-segment displays for each digit in the time
	private SevenSegment hourDigit1;
	private SevenSegment hourDigit0;
	private SevenSegment minuteDigit1;
	private SevenSegment minuteDigit0;
	private SevenSegment secondDigit1;
	private SevenSegment secondDigit0;
	// labels separating hour, minute, second in the time
	private JLabel colon1;
	private JLabel colon2;
	// buttons for the operation of the stop watch 
	private JButton startButton;
	private JButton stopButton;
	private JButton resetButton;
	// drives the Time class 
	private Timer timer;
	// stores the time of the stop watch 
	private Time time;
	// window sizes 
	private final int WINDOW_WIDTH = 910;
	private final int WINDOW_HEIGHT = 370;
	// delay of Timer in milliseconds  == 1 second 
	private final int DELAY = 1000;

	/**
	 * Constructor - initializes components
	 */
	public StopWatch()
	{
		// initialize the model for the GUI 
		time = new Time();
		startAnimation(); //starts timer

		// sets up window for application 
		setTitle("Stop Watch");
		setSize(WINDOW_WIDTH,WINDOW_HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		
		// key method to creating view for the GUI
		
		buildPanels();
		add(mainPanel);
		pack(); //I added this method call.
		setLocationRelativeTo(null);
		setVisible(true);
	}

	/**
	 * Initializes components, assembles them onto panels
	 */
	private void buildPanels()
	{
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		initButtonsPanel();
		initWatchPanel();
	}

	private void initButtonsPanel()
	{
		//action listeners
		StartButtonActionListener start = new StartButtonActionListener();
		StopButtonActionListener stop = new StopButtonActionListener();
		ResetButtonActionListener reset = new ResetButtonActionListener();

		buttonPanel = new JPanel();
		buttonPanel.setLayout(new BoxLayout(buttonPanel,BoxLayout.Y_AXIS));
		
		
		startButton = new JButton("START");
		startButton.setFont(new Font("Helvetica", Font.BOLD, 13));
		startButton.addActionListener(start);
		
		stopButton = new JButton("STOP");
		stopButton.setFont(new Font("Helvetica", Font.BOLD, 13));
		stopButton.addActionListener(stop);
		
		resetButton = new JButton("RESET");
		stopButton.setFont(new Font("Helvetica", Font.BOLD, 13));
		resetButton.addActionListener(reset);

		buttonPanel.add(startButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		buttonPanel.add(stopButton);
		buttonPanel.add(Box.createRigidArea(new Dimension(0, 10)));
		buttonPanel.add(resetButton);

		mainPanel.add(buttonPanel,BorderLayout.WEST);
	}

	private void initDigits(){
		hourDigit0 = new SevenSegment(time.hour(), Color.RED);
		hourDigit0.setPreferredSize(new Dimension(getHeight() /2, getWidth()/2));
		hourDigit1 = new SevenSegment(time.hour(), Color.RED);
		hourDigit1.setPreferredSize(new Dimension(getHeight() / 2, getWidth()/2));		

		minuteDigit0 = new SevenSegment(time.minute(), Color.WHITE);
		minuteDigit0.setPreferredSize(new Dimension(getHeight() / 2, getWidth()/2));
		minuteDigit1 = new SevenSegment(time.minute(), Color.WHITE);
		minuteDigit1.setPreferredSize(new Dimension(getHeight() / 2, getWidth()/2));
		
		secondDigit0 = new SevenSegment(time.second(), Color.BLUE);
		secondDigit0.setPreferredSize(new Dimension(getHeight() / 2, getWidth()/2));
		secondDigit1 = new SevenSegment(time.second(), Color.BLUE);
		secondDigit1.setPreferredSize(new Dimension(getHeight() / 2, getWidth()/2));
		
	}
	private void initWatchPanel()
	{
		colon1 = new JLabel(" : ");
		colon1.setFont(new Font("Helvetica",Font.PLAIN,120));
		colon2 = new JLabel(" : ");
		colon2.setFont(new Font("Helvetica",Font.PLAIN,120));
		colon2.setToolTipText("'Merica, hell yea!");

		initDigits();
		watchPanel = new JPanel();
		
		watchPanel.add(hourDigit0);
		watchPanel.add(hourDigit1);
		watchPanel.add(colon1);
		watchPanel.add(minuteDigit0);
		watchPanel.add(minuteDigit1);
		watchPanel.add(colon2);
		watchPanel.add(secondDigit0);
		watchPanel.add(secondDigit1);
		
		mainPanel.add(watchPanel,BorderLayout.CENTER);
	}
	
	private void startAnimation(){
		timer = new Timer(DELAY,new TimerActionListener());
		timer.start();
	}
	/**
	 * Sets, updates seven-segment displays using time stored in the Time class
	 * object
	 */
	private void drawDigits()
	{
		hourDigit0.setDigit(time.hour() / 10);
		hourDigit0.redraw();
		hourDigit1.setDigit(time.hour() % 10);
		hourDigit1.redraw();
		
		minuteDigit0.setDigit(time.minute() / 10);
		minuteDigit0.redraw();
		minuteDigit1.setDigit(time.minute() % 10);
		minuteDigit1.redraw();
		
		secondDigit0.setDigit(time.second() / 10);
		secondDigit0.redraw();
		secondDigit1.setDigit(time.second() % 10);
		secondDigit1.redraw();
	}

	/**
	 * ActionListener class for the Timer object
	 * @author Matt T
	 */
	private class TimerActionListener implements ActionListener
	{
		/**
		 * Updates components, variables every 1000 milliseconds
		 */
		@Override
		public void actionPerformed(ActionEvent ae)
		{
			time.incrementTime();
			drawDigits();
		}
	}
	
	/**
	 * ActionListener for the startButton
	 * @author Matt T
	 */
	private class StartButtonActionListener implements ActionListener
	{
		/**
		 * Updates components, variables when start button is clicked
		 */
		@Override
		public void actionPerformed(ActionEvent ae)
		{
			timer.start();
		}
	}

	/**
	 * ActionListener for the stopButton
	 * @author Matt T
	 */
	private class StopButtonActionListener implements ActionListener
	{
		/**
		 * Updates components, variables when stop button is clicked
		 */
		@Override
		public void actionPerformed(ActionEvent ae)
		{
			timer.stop();
		}
	}

	/**
	 * ActionListener for the resetButton
	 * @author Matt T
	 */
	private class ResetButtonActionListener implements ActionListener
	{
		/**
		 * Updates components, variables when reset button is clicked
		 */
		@Override
		public void actionPerformed(ActionEvent ae)
		{
			time.resetTime(0);
			timer.start();
		}
	}

	/**
	 * main method - initiates the GUI, which runs until window is closed.
	 * @param args- not used
	 */
	public static void main(String[] args)
	{
		new StopWatch();
	}

}
