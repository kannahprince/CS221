*******************************************************************************
* Project: 1/Magic Square
* Class:   CS221 - 002
* Date:    05/24/15
* Name:    Prince Kannah
********************************************************************************

OVERVIEW:

 Magic Square Tester is a command-line program that a either check or create a magic squares. MagicSquare is the underlying class that is
 use to create and validate magic squares.


INCLUDED FILES:

 MagicSquare.java - the underlying class that is use to validate and create magic squares.
 
 MagicSquareTester.java - driver class for MagicSquare. Checks command-line arguments before creating or validating magic squares.
 
 README - contains instructions on running the program and serves as a development diary.


BUILDING AND RUNNING:

 All program files should be in the same directory.
 
  To compile the program using command-line:
  	javac MagicSquareTester.java
  
  To run the compiled program using command-line:
  	java MagicSquareTester [-­check | ­-create] [filename] [size]
 
 Command-line flags:
 	To check if a matrix within a specific file is a valid magic square:
 		java MagicSquareTester -check NameOfFile
 	
 	To create a magic square with the specified size and write it to the specified file:
 		java MagicSquareTester -create NameOfFile size
 

PROGRAM DESIGN:

 The driver class, MagicSquareTester, uses MagicSquare to create and validate magic squares. MagicSquare has two constructors for this purpose, since there's never a time when we're creating and checking a magic square. Within MagicSquare are four important methods: readMatrix(), checkMatrix, createMagicSquare and finally writeMatrix(). The readMatrix() method is used when a matrix is being checked. It reads the content of a file and stores it in a 2D array. checkMatrix() then verifies - using helper methods - that the array contains a valid magic square by checking that all rows, columns and two diagonals add up to the same number as well as checking the occurrence of 1,2,3...(n*n) in the matrix.
 
 The createMagicSquare() method creates a magic square with the specified size. The method follows the provided pseudo code to create a valid magic square. The magic square can then be written to a file using writeMatrix().

PROGRAM DEVELOPMENT AND TESTING DISCUSSION:

	My initial approach with the Scanner was to read in the file as a string then parse the strings to integers. While that would have worked, it included unnecessary work. Since everything in the file is supposed to be integers I could simply use nextInt() to fill in my array. The hardest part for me was writing the createMagicSquare() method and the various helper methods that checks the rows, columns and diagonals. The pseudo code for creating the square was a bit confusing but made a lot of sense after going over it. In order to write the helper methods I had to draw out a magic square with the row and columns filled in. Summing up the rows and columns were fairly easy since going both are always zero depending on what direction you're going. With it visually laid out I noticed that from left to right row + column = size -1 and from bottom right to left row & column equaled each other. So, I used those two checks when looking at the diagonals.
	
	My testing for this project was a bit more involved than previous ones. After testing with the provided sample files and was confident that the program was good I went ahead and created some of my own invalid input files. This actually helped me catch a NoSuchElementException thrown by the Scanner in this case when the matrix size is greater than the actual matrix. I use the tester class to create several valid squares then tested that those were indeed valid. I then went in and change the file by either removing or adding numbers. All in those instances, the program was able to successfully determine them as invalid. For fun, I decided to create a 1001 magic square...I stopped it after running for about 20 minutes (it did create it but was taking too long to write it to the file).
	
	I had several "ah-ha" moments while working on this project but my biggest is finally understanding and seeing the importance of try/catch. I got several exceptions thrown while testing my driver class by inputting commands in the wrong order. Using try/catch I'm able to gracefully recover from them and give a message as to what went wrong. When I finally saw the utility I was almost tempted to try and catch every possible exception but quickly realized that this made my code extensive and it was easy to quickly get lost. So, I only caught the ones that were likely to happen.
	
	Overall, I liked this project. It was a great refresher for the last two projects of 121. The only part that was understanding the pseudo code. My biggest take away from this project is exception handling and testing that my code meets not only meets the requirements but also go beyond and anticipate worst case scenarios. A fun extra credit could be writing the matrix to a GUI or perhaps make the entire project a GUI that shows the square being created instead of being written a file!
 
