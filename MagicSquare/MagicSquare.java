import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * The <code>MagicSquare</code> class can create a magic square with the
 * specified <b>odd number</b> of size as well as determine if a provided matrix
 * is a valid magic square.
 * @author Prince Kannah
 */

public class MagicSquare
{

    private Scanner fileScan;
    private File readFile;// file being read from
    private PrintWriter pWriter;
    private int size;// the size of the magic square
    private int[][] magicSquare;
    private ArrayList<Integer> numOccurrence;
    private int magicSquareEquation;
    private boolean readMatrix = false;// to know when readMatrix() is
                                       // called

    /**
     * Constructor: Creates a new <code>MagicSquare</code> object with the
     * specified file. Useful for checking if the file contains a valid magic
     * square matrix. <b>NOTE: calls readMatrix()</b>.
     * @param file :The file containing the matrix
     */
    public MagicSquare(File file)
    {
        readFile = file;
        readMatrix();
    }

    /**
     * Constructor: Creates a new <code>MagicSquare</code> object with the
     * specified size. Useful for creating magic square matrices. <b>NOTE: calls
     * createMagicSquare().</b>
     * @param size :The size of the matrix.
     */
    public MagicSquare(int size)
    {
        this.size = size;
        magicSquareEquation = (this.size * (this.size * this.size + 1) / 2);
        createMagicSquare();
    }

    /**
     * Scans the input file into a 2D array.
     */
    public void readMatrix()
    {
        readMatrix = true;
        try
        {
            numOccurrence = new ArrayList<Integer>();
            fileScan = new Scanner(readFile);
            size = fileScan.nextInt();
            magicSquareEquation = (size * (size * size + 1) / 2);
            magicSquare = new int[size][size];

            for(int row = 0; row < magicSquare.length; row++)
            {
                for(int col = 0; col < magicSquare[row].length; col++)
                {
                    magicSquare[row][col] = fileScan.nextInt();
                    numOccurrence.add(magicSquare[row][col]);
                }
            }
        }
        catch(FileNotFoundException e)
        {
            System.err.println(readFile + " could not be found.");
            System.exit(1);
        }
        catch(InputMismatchException e)
        {
            System.err.println(readFile + " does not contain valid inputs.");
            System.exit(1);
        }
        catch(NoSuchElementException e)
        {
            //does not exit since the invalid matrix should still be printed out.
        }
    }

    /**
     * Creates a magic square with the specified size.
     */
    public void createMagicSquare()
    {
        initSquare();// set everything to zero
        int row = size - 1;
        int col = size / 2;
        int previousRow;// holds onto the previous value of row & column
        int previousCol;

        for(int i = 1; i <= size * size; i++)
        {
            previousRow = row;
            previousCol = col;
            magicSquare[row][col] = i;
            row++;
            col++;

            if(row == size)
            {
                row = 0;
            }
            if(col == size)
            {
                col = 0;
            }

            if(magicSquare[row][col] != 0)
            {
                row = previousRow;
                col = previousCol;
                row--;
            }
        }
    }

    /**
     * Writes the magic square matrix to the specified file.
     * @param file :The file to write to.
     */
    public void writeMatrix(File file)
    {
        try
        {
            pWriter = new PrintWriter(file);
            pWriter.println(getSize());
            printMatrix();
            pWriter.close();
        }
        catch(FileNotFoundException e)
        {
            System.err.println(file + " not found");
            e.printStackTrace();
        }
    }

    /**
     * Checks if the values in the matrix creates a magic square.
     * @return Returns <code>true</code> if the values add up to a magic square;
     *         <code>false</code> otherwise.
     */
    public boolean checkMatrix()
    {
        if(sumRow() == magicSquareEquation && sumCol() == magicSquareEquation
                && sumLeftDiag() == magicSquareEquation
                && sumRightDiag() == magicSquareEquation)
        {
            if(readMatrix)
            {
                checkOccurrence();
                return true;
            }
            return true;
        }
        return false;
    }

    /**
     * Prints magicSquare to file using printf.
     */
    @SuppressWarnings("unused")
    private void printMatrix()
    {
        String tmp = "";
        for(int p = 0; p < magicSquare.length; p++)
        {
            for(int j = 0; j < magicSquare[p].length; j++)
            {
                tmp += pWriter.printf("%2d ",magicSquare[p][j]);
            }
            tmp += pWriter.printf("\n");
        }
    }

    /**
     * Initialize magicSquare elements to zero
     */
    private void initSquare()
    {
        magicSquare = new int[size][size];
        for(int row = 0; row < magicSquare.length; row++)
        {
            for(int col = 0; col < magicSquare[row].length; col++)
            {
                magicSquare[row][col] = 0;
            }
        }
    }

    /**
     * @return The sum of all the rows.
     */
    private int sumRow()
    {
        int rowTotal = 0;
        for(int i = 0; i <= size - 1; i++)
        {
            rowTotal += magicSquare[0][i];
        }
        return rowTotal;
    }

    /**
     * @return The sum of all the columns.
     */
    private int sumCol()
    {
        int colTotal = 0;
        for(int i = 0; i <= size - 1; i++)
        {
            colTotal += magicSquare[i][0];
        }
        return colTotal;
    }

    /**
     * @return The sum of the right diagonal.
     */
    private int sumRightDiag()
    {
        int rightDiagTotal = 0;

        for(int p = 0; p < magicSquare.length; p++)
        {
            for(int j = 0; j < magicSquare[p].length; j++)
            {
                if(p == j)
                {
                    rightDiagTotal += magicSquare[p][j];
                }
            }
        }
        return rightDiagTotal;
    }

    /**
     * @return The sum of the left diagonal.
     */
    private int sumLeftDiag()
    {
        int leftDiagTotal = 0;

        for(int p = 0; p < magicSquare.length; p++)
        {
            for(int j = 0; j < magicSquare[p].length; j++)
            {
                if(p + j == size - 1)
                {
                    leftDiagTotal += magicSquare[p][j];
                }
            }
        }
        return leftDiagTotal;
    }

    /**
     * Checks the occurrence of each integer from 1,2,3...(n*n) in the matrix
     * @return <code>true</code> if the integers occur; <code>false</code>
     *         otherwise.
     */
    private boolean checkOccurrence()
    {
        if(!readMatrix)
        {
            for(int j = 1; j < (size * size); j++)
            {
                if(numOccurrence.contains(j))
                {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Get the size of the current matrix.
     * @return The current size of the matrix.
     */
    public int getSize()
    {
        return size;
    }

    /**
     * @return A string representation (i.e. the magic square) of this
     *         <code>MagicSqaure</code> object.
     */
    public String toString()
    {
        String tmp = "";

        for(int p = 0; p < magicSquare.length; p++)
        {
            for(int j = 0; j < magicSquare[p].length; j++)
            {
                tmp += String.format("%-4d",magicSquare[p][j]);
            }
            tmp += "\n";
        }
        return tmp;
    }
}
