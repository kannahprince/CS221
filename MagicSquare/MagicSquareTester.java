import java.io.File;

public class MagicSquareTester
{

    private static enum ErrorCause
    {
        NO_FILES, SIZE_NOT_ODD, NOT_ENOUGH_ARGMENTS, NO_VALID_ARGUMENTS
    };

    /**
     * Execute MagicSquareTester.
     * @param args :The command line arguments
     */
    public static void main(String[] args)
    {

        if(args.length < 1)
        {
            printError(ErrorCause.NOT_ENOUGH_ARGMENTS);
        }

        final String CREATE_CHECK = args[0].substring(1,args[0].length());

        if(CREATE_CHECK.equalsIgnoreCase("check"))
        {
            /*
             * Making sure that files were provided. For loop doesn't throw an
             * exception since you can technically making a file that's an empty
             * string
             */
            if(args.length < 2)
            {
                printError(ErrorCause.NO_FILES);
            }

            //check each file in args array.
            for(int i = 1; i < args.length; i++)
            {
                MagicSquare s = new MagicSquare(new File(args[i].trim()));

                if(s.checkMatrix())
                {
                    System.out.println(
                            "The matrix:\n" + s + "is a magic square\n");
                }
                else
                {
                    System.err.println(
                            "The matrix:\n" + s + "is not a maagic square\n");
                }
            }
        }
        else if(CREATE_CHECK.equalsIgnoreCase("create"))
        {
            if(args.length < 3)
            {
                printError(ErrorCause.NOT_ENOUGH_ARGMENTS);
            }

            int size = Integer.parseInt(args[2]);

            if(size % 2 == 1)
            {
                MagicSquare s = new MagicSquare(size);
                s.writeMatrix(new File(args[1].trim()));
            }
            else
            {
                printError(ErrorCause.SIZE_NOT_ODD);
            }
        }
        else
        {
            printError(ErrorCause.NO_VALID_ARGUMENTS);
        }

    }

    /**
     * Use to print to error messages and their respective cause then exit the
     * program.
     * @param cause cause of the error
     */
    private static void printError(ErrorCause cause)
    {
        switch (cause)
        {
            case NO_FILES:
                System.err.println("No files were provided.");
                System.err.println(
                        "Usage: [-check | -create] [file1 file2...] [size]");
                System.exit(1);
            break;
            case SIZE_NOT_ODD:
                System.err.println(
                        "Use odd values to create a valid magic square.");
                System.exit(1);

            break;
            default:
                System.err.println(
                        "Usage: [-check | -create] [file1 file2...] [size]");
                System.exit(1);
            break;
        }
    }
}
