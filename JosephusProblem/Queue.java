/**
 * <code>Queue</code> represents a queue that can be use to store various data types.
 * @author Prince Kannah
 */
public class Queue<T> {
	private SinglyLinkedList<T> list;
	
	/**
	 * Default constructor :creates an empty <code>Queue</code>.
	 */
	public Queue(){
		list = new SinglyLinkedList<T>();
	}
	
	
	/**
	 * Adds an element to the back of the <code>Queue</code>.
	 * @param element :the item to be added to the <code>Queue</code>.
	 * @return
	 */
	public void enqueue(T element){
		list.insertAtBack(element);
	}
	
	/**
	 * Removes (delete) the first element in the <code>Queue</code>.
	 * @return
	 */
	public T dequeue(){
		T target = list.head().getElement();
		list.delete(target);
		return target;
	}
	
	/**
	 * Returns the first element in the <code>Queue</code>. 
	 * @return
	 */
	public T first(){
		return list.head().getElement();
	}
	
	public int size(){
		return list.length();
	}
	
	/**
	 * Returns the the status of the <code>Queue</code>.
	 * @return <code>true</code> if the <code>Queue</code> is empty; false otherwise.
	 */
	public boolean isEmpty(){
		if(size() != 0){
			return false;
		}
		return false;
	}
	
	/**
	 * Returns a string representation of the <code>Queue</code>.
	 */
	public String toString(){
		String str = "LinkedQueue [";
		SLLNode<T> current = list.head();
		
		while(current != null)
		{
			T temp = current.getElement();
			str += temp + " ";
			current = current.next;
		}
		str += "]";
		return str;
	}

}
