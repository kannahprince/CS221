/**
 * Prints out list of reindeer.
 * @version summer 2015
 */

public class Reindeer {

	/**
	 * Prints list of reindeer. 
	 * 
	 * @param reindeer
	 * 			how many reindeer to print out
	 */
	public static void printDeer(int reindeer) {
		switch (reindeer) {
		case 1:
			System.out.println("Dasher");
			break;
		case 2:
			System.out.println("Dancer");
			break;
		case 3:
			System.out.println("Comet");
			break;
		case 4:
			System.out.println("Cupid");
		}
	}

	/**
	 * Main method for calling the printDeer() method. 
	 * 
	 */
	public static void main(String[] args) {
		int i;
		for (i = 1; i <= 4; i++) {
			printDeer(i);
		}
	}

}
