

/**
 * An unordered container that holds up to 100 ints 
 */
public class Bag {
	private static final int CAPACITY = 100; // capacity of the Bag
	private int data[] = new int[CAPACITY]; // array to hold integers in the Bag
	private int count; // number of integers stored in the array

	/**
	 * Default constructor creates an empty Bag
	 */
	public Bag() {
		count = 0;
	}

	/**
	 * Member function used to insert a new value in the bag. PRE: none POST:
	 * entry is added to the contents of the bag and the count is incremented
	 * RETURNS true if successful, false if the bag was full
	 */
	public boolean insert(int entry) {
		if (count >= CAPACITY) {
			return false;
		} else {
			data[count] = entry;
			count++;
			return true;
		}
	}

	/**
	 * Member function to remove a value from the bag. PRE: none POST: if target
	 * is in the Bag, one instance of target is removed, otherwise nothing
	 * happens Returns true if an instance was removed, false otherwise
	 */
	public boolean remove(int target) {
		int i;
		for (i = 0; i < count; i++) {
			if (data[i] == target) {
				data[i] = data[count-1];
				data[count-1] = 0;
				count--;
				return true;
			}
		}
		return false;
	}

	/**
	 * Member function to count the number of occurrences of a bag element. *
	 * 
	 * @param target
	 *            The element to be counted
	 * @return The number of times target occurs in the Bag
	 */
	public int occurrences(int target) {
		int answer = 0;
		for (int i = 0; i < count; i++) {
			if (target == data[i]) {
				answer++;
			}
		}
		return answer;
	}

	/**
	 * @return the size of the bag.
	 */
	public int size() {
		return count;
	}

	/**
	 * Member function to print the elements of the bag.
	 */
	public void print() {
		System.out.println("The bag has " + count + " elements:");
		if (count > 0) {
			System.out.print(data[0]);
			for (int i = 1; i < count; i++) {
				System.out.print(", " + data[i]);
			}
			System.out.println();
		}
	}
}
