/**
 * Prints names in a list, changes all names to "Sally" and 
 * prints list again. 
 * 
 * @version summer 2015
 */

public class Names {
	private String[] names = { "John", "Bill", "Sheila", "Amit", "Jane" };

	private final int count = 4;
	
	/**
	 * Constructor not used. 
	 */
	public Names() {
	}

	/**
	 * Sets all values in Names to given name. 
	 * 
	 * @param name
	 * 			String change names to 
	 */
	public void setNames(String name) {
		for (int i = 0; i < count; i++) {
			names[i] = name;
		}
	}

	/**
	 * Prints names in the list.
	 */
	public void printNames() {
		System.out.println("Contents of Names:");
		for (int i=0; i < count; i++) {
			System.out.println("\t" + names[i]);
		}
	}

	/**
	 * Main method for creating Names object,
	 * calling its methods. 
	 */
	public static void main(String[] args) {
		Names n = new Names();
		n.printNames();
		System.out.println("Changing Names to \"Sally\"");
		n.setNames("Sally");
		n.printNames();

	}

}
