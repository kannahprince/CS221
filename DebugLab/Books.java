/**
 * Stores book titles as Strings, stores them in a list 
 * alphabetically. 
 * 
 * @version summer 2015
 */

public class Books {
	private String[] books;

	public Books(String[] books) {
		this.books = books;
	}

	/**
	 * Returns true if book1 belongs before book2 in an alphabetical ordering.
	 * 
	 * @param book1
	 * 			String book title
	 * @param book2
	 * 			String book title
	 * @return boolean - whether book1 goes before book2
	 */
	private static boolean goesBefore(String book1, String book2) {
		if(book1.compareToIgnoreCase(book2) > 0){ 
			return false;
		}
		else if(book1.compareToIgnoreCase(book2) < 0){
			return true;
		}
		else{
			return true;
		}
	}

	/**
	 * Return index of a book if its in the list,
	 * or index where would go in the list, if it doesn't.
	 * 
	 * @param book
	 * 			String book title
	 * @return int - index in the list where book should be 
	 */
	public int find(String book) {
		int i;
		for (i = 0; i < books.length; i++) {
			if (goesBefore(book, books[i])) {
				break;
			}
		}
		return i;
	}

	/**
	 * Prints list of books.
	 */
	public void print() {
		System.out.println("Contents of books:");
		for (String book : books) {
			System.out.println("\t" + book);
		}
	}

	/**
	 * Return the i'th book
	 */
	public String get(int i) {
		return books[i];
	}

	/**
	 * Main method for testing the Book class. 
	 */
	public static void main(String args[]) {
		Books books = new Books(new String[] {
				"Distributed Systems: A Modern Approach", "Franklin's Bad Day",
				"Hitchhiker's Guide to the Galaxy, The", "Pattern Recognition",
				"Veleveteen Rabbit, The" });
		books.print();

		String book = "Hitchhiker's Guide to the Galaxy, The";
		int loc = books.find(book);
		System.out.println("\"" + book + "\" belongs at location " + loc);

		book = "Moby Dick";
		loc = books.find(book);
		System.out.println("\"" + book + "\" belongs at location " + loc);
	}
}
