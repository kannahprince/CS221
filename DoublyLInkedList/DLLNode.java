/**
 * The <code>DLLNode</code> class represents a node in a doubly-linked list that
 * stores elements of type T.
 * @author Prince Kannah
 * @param <T> - type of objects to be stored by this node.
 */
public class DLLNode<T>
{

    private DLLNode<T> next, previous;
    private T element;

    /**
     * Default constructor- creates a new DLLNode containing the provided
     * element.
     * @param element - element to store at this node.
     */
    public DLLNode(T element)
    {
        this.element = element;
        next = previous = null;
    }

    /**
     * Sets the next node to the new, specified node.
     * @param newNode - the new node that should be this node's next.
     */
    public void setNext(DLLNode<T> newNode)
    {
        next = newNode;
    }

    /**
     * Gets the next node.
     * @return - the next node after this one.
     * @throws ElementNotFoundException if the next node is <code>null</code>.
     */
    public DLLNode<T> getNext()
    {
        //		if(next == null)
        //		{
        //			throw new ElementNotFoundException("DLLNode");
        //		}
        return next;
    }

    /**
     * Sets the previous node to the new, specified one.
     * @param newPrevious - the new node to set as previous.
     */
    public void setPrevious(DLLNode<T> newPrevious)
    {
        previous = newPrevious;
    }

    /**
     * Gets the previous node.
     * @return - the node before this one.
     * @throws ElementNotFoundException if the previous node is
     *         <code>null</code>.
     */
    public DLLNode<T> getPrevious()
    {
        if(previous == null)
        {
            throw new ElementNotFoundException("DLLNode");
        }
        return previous;
    }

    /**
     * Gets the element stored at this node.
     * @return the element at this node.
     * @throws ElementNotFoundException if the element is <code>null</code>.
     */
    public T getElement()
    {
        if(element == null)
        {
            throw new ElementNotFoundException("DLLNode");
        }
        return element;
    }

    /**
     * Sets the element of this node.
     * @param newElement - the new element to be stored at this node.
     */
    public void setElement(T newElement)
    {
        element = newElement;
    }
}
