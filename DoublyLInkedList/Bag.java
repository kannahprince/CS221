import java.util.Iterator;

/**
 * The <code>Bag</code> class is an unordered container that holds items of type
 * T using an Doubly Linked List implementation.
 * @author Prince Kannah
 */
public class Bag<T>
{

    private DoublyLinkedList<T> bag;// DLLNodes to hold objects of type T in the Bag
    private int modCount;
    private int itemCount;

    /**
     * Default constructor - creates an empty <code>Bag</code>.
     */
    public Bag()
    {
        bag = new DoublyLinkedList<T>();
        itemCount = 0;
        modCount = 0;
    }

    /**
     * Inserts a new item in the <code>Bag</code>.
     * @param item - element to add to the <code>Bag</code>.
     */
    public void insert(T item)
    {
        bag.insertAtBack(item);
        itemCount++;
        modCount++;
    }

    /**
     * Removes an item from the bag.
     * @param target - item to remove from the <code>Bag</code>.
     * @return returns <code>true</code> if the target is found and removed;
     *         <code>false</code> otherwise
     */
    public boolean remove(T target)
    {
        if(bag.find(target) != null)
        {
            bag.delete(target);
            itemCount--;
            modCount--;
            return true;
        }
        return false;
    }

    /**
     * Retrieve items from the <code>Bag</code>.
     * @param target - the element to retrieve from the <code>Bag</code>.
     * @throws ElementNotFoundException if target is not contained in the
     *         <code>Bag</code>.
     */
    public T retrieve(T target)
    {
        if(bag.find(target) == null)
        {
            throw new ElementNotFoundException("Bag");
        }
        return bag.find(target).getElement();
    }

    /**
     * Counts the number of occurrences of an item in the <code>Bag</code>.
     * @param target - the element to look for in the <code>Bag</code>.
     * @return the number of times the target appears in the <code>Bag</code>.
     */
    public int occurrences(T target)
    {
        Iterator<T> it = iterator();
        int numOccurrence = 0;

        while (it.hasNext())
        {
            T temp = it.next();
            if(temp == target)
            {
                numOccurrence++;
            }
        }
        return numOccurrence;
    }

    /**
     * Size of the bag.
     * @return the number of elements in the <code>Bag</code>.
     */
    public int size()
    {
        return itemCount;
    }

    /**
     * Print elements of the bag.
     */
    public void print()
    {
        Iterator<T> it = iterator();
        while (it.hasNext())
        {
            System.out.println(it.next());
        }
    }

    /**
     * Returns a BagIterator for element of the type T.
     * @return an Iterator for a Bag of the elements of type T
     */
    public Iterator<T> iterator()
    {
        return new BagIterator();
    }

    /**
     * An private iterator for the Bag class.
     * @author Prince Kannah
     */
    private class BagIterator implements Iterator<T>
    {

        // keeps track of number of modifications since the 
        // iterator was instantiated.
        private int iteratorModCount;
        DLLNode<T> currentNode;
        int currentIndex;//keeps track of Iterator position

        /**
         * Default constructor
         */
        public BagIterator()
        {
            // set modification count to count for the outer class 
            iteratorModCount = modCount;
            currentNode = bag.getHead();
            currentIndex = 0;
        }

        /**
         * Checks if a modification has been done since the Iterator was
         * instantiated
         * @throws ConcurrentModificationException
         */
        private void checkMod()
        {
            // counts don't match 
            if(iteratorModCount != modCount)
            {
                throw new ConcurrentModificationException("BagIterator");
            }
        }

        /**
         * Checks if the <code>Bag</code> has more elements in it.
         * @return true if there is another element in the <code>Bag</code>;
         *         false otherwise.
         * @throws ConcurrentModificationException if the <code>Bag</code> has
         *         been modified since the Iterator was instantiated.
         */
        @Override
        public boolean hasNext() throws ConcurrentModificationException
        {
            checkMod();
            return(currentIndex != size());
        }

        /**
         * Returns the element that is passed by the Iterator.
         * @return - the next element in the iteration.
         * @throws ElementNotFoundException if there are no more elements in the
         *         iteration.
         */
        @Override
        public T next()
        {
            if(!hasNext())
            {
                throw new ElementNotFoundException("Bag");
            }

            T nextElement = currentNode.getElement();
            currentNode = currentNode.getNext();
            currentIndex++;

            return nextElement;
        }
    }
}
