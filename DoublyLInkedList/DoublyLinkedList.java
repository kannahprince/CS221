/**
 * The <code>DoublyLinkedList</code> class represents a doubly linked list data
 * structure that stores elements of type T.
 * @author Prince Kannah
 * @param <T> - elements to store in the list.
 */
public class DoublyLinkedList<T>
{

    private DLLNode<T> head, tail;
    private int length;

    /**
     * Default constructor - creates an empty list.
     */
    public DoublyLinkedList()
    {
        length = 0;
        head = tail = null;
    }

    /**
     * Constructor - creates a new list with the specified element.
     * @param element - the element to store in the list.
     */
    public DoublyLinkedList(T element)
    {
        DLLNode<T> node = new DLLNode<T>(element);
        head = tail = node;
        length++;
    }

    /**
     * Inserts an element to the back of the list.
     * @param newElement - the element to add (insert) to the back of the list.
     */
    public void insertAtBack(T newElement)
    {
        // create new node with given element
        DLLNode<T> newNode = new DLLNode<T>(newElement);
        // if empty list, set head & tail to this node
        if(length == 0)
        {
            head = tail = newNode;
        }
        else if(length == 1)
        {
            tail = newNode;
            head.setNext(tail);
            tail.setPrevious(head);
        }
        // list with more than one node
        else
        {
            // attach new node to back of the list
            tail.setNext(newNode);
            newNode.setPrevious(tail);
            tail = newNode;
        }
        length++;
    }

    /**
     * Inserts an element to the front of the list.
     * @param newElement - the element to add (insert) to the front of the list.
     */
    public void insertAtFront(T newElement)
    {
        // create new node with given element
        DLLNode<T> newNode = new DLLNode<T>(newElement);
        // if empty list, set head & tail to this node
        if(length == 0)
        {
            head = tail = newNode;
        }
        else if(length == 1)
        {
            head = newNode;
            tail.setPrevious(head);
            head.setNext(tail);
        }
        else
        {
            // attach new node to the front of the list
            head.setPrevious(newNode);
            newNode.setNext(head);
            head = newNode;
        }
        length++;
    }

    /**
     * Finds the specified element in the list.
     * @param target - the element to find in the list.
     * @return the node containing the target element.
     * @throws ElementNotFoundException if the target is <code>null</code>.
     */
    public DLLNode<T> find(T target)
    {
        // begin search at the front of the list
        DLLNode<T> current = head;
        while (current != null)
        {
            // if element is found, return that node
            if(current.getElement() == target)
            {
                return current;
            }
            current = current.getNext();
        }
        return null;// assuming the target element is not found
    }

    /**
     * Removes a given element from the list.
     * @param target - the element to be removed list.
     * @throws ElementNotFoundException if the target is <code>null</code>.
     */
    public void delete(T target)
    {
        DLLNode<T> targetNode = find(target);// node containing the target to delete
        DLLNode<T> nextNode, previousNode;

        if(length == 1)
        {
            head = tail = null;
            length--;
        }
        // there is more than one element
        else
        {
            // the beginning of the list
            if(targetNode == head)
            {
                head = head.getNext();
                head.setPrevious(null);
                length--;
            }
            // at the end of the list
            else if(targetNode == tail)
            {
                tail = tail.getPrevious();
                tail.setNext(null);
                length--;
            }
            // somewhere in the list
            else
            {
                // establishing forward link
                previousNode = targetNode.getPrevious();// get previous from targetNode	
                previousNode.setNext(targetNode.getNext());// set previousNode to now point to targetNode's next

                // establishing backward link
                nextNode = targetNode.getNext();//get next from targetNode
                nextNode.setPrevious(targetNode.getPrevious());//set nextNode to now point to targetNode's previous

                targetNode = null;//finally delete targetNode
                length--;
            }
        }
    }

    /**
     * The size of the list.
     * @return the number of elements in the list.
     */
    public int size()
    {
        return length;
    }

    /**
     * Gets the head (beginning) of the list.
     * @return the head node of the list.
     * @throws ElementNotFoundException if head is <code>null</code>.
     */
    public DLLNode<T> getHead()
    {
        return head;
    }

    /**
     * Sets the head (beginning) of the list to the new, specified node.
     * @param newHead - the new node to be set as the head.
     */
    public void setHead(DLLNode<T> newHead)
    {
        head = newHead;
    }

    /**
     * Gets the tail (end) of the list.
     * @return the tail node of the list.
     * @throws ElementNotFoundException if tail is <code>null</code>.
     */
    public DLLNode<T> getTail()
    {
        return tail;
    }

    /**
     * Sets the tail (end) of the list to the new, specified node.
     * @param newTail - the new node to be set as the tail.
     */
    public void setTail(DLLNode<T> newTail)
    {
        tail = newTail;
    }
}
