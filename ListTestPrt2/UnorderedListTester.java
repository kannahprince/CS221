
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * A unit test class for UnorderedList. This is a set of black box tests that
 * should work for any implementation of UnorderedList/List.
 * @author mvail, Matt T
 */
public class UnorderedListTester
{

    // stats on test cases run 
    private int passes = 0;
    private int failures = 0;
    private int total = 0;
    // elements added to the lists 
    private int A = new Integer(1);
    private int B = new Integer(2);
    private int C = new Integer(3);
    private int D = new Integer(4);
    private int E = new Integer(5);

    // which class running tests on 
    private enum ListType
    {
        Good, Bad
    };
    // results of a given test
    private enum Results
    {
        EmptyCollection, True, ElementNotFound, NoException, False, Fail
    };

    // stores which class running tests on 
    private ListType listType;

    // which method performed in the change scenario
    private enum Methods
    {
        constructor, addToFront, addToRear, addAfter, removeFirst, removeLast, remove
    };

    /**
     * Processes command-line argument, calls method to run tests
     * @param args - "-g" or "-b" for good and bad lists, respectively
     */
    public static void main(String[] args)
    {
        //to avoid every method being static
        UnorderedListTester tester = new UnorderedListTester();
        if(args.length != 1)
        {
            System.err.println("Usuage: [-g | -b]");
            return;
        }
        else if(args[0].equals("-g"))
        {
            tester.runTests(ListType.Good);
        }
        else if(args[0].equals("-b"))
        {
            tester.runTests(ListType.Bad);
        }
    }

    /**
     * Returns a UnorderedList of a given type and with given elements added.
     * @param changeMethod - method applied to list to create current state
     * @param length - number of elements in the list before the change
     * @param elements - elements in the list before change, list of arguments
     *        to method
     * @return a new UnorderedList
     */
    private UnorderedList<Integer> newList(Methods changeMethod, int length,
            Integer... elements)
    {
        UnorderedList<Integer> newList;
        // determine which list to create
        switch (listType)
        {
            case Good:
                newList = new GoodUnorderedList<Integer>();
            break;
            case Bad:
                newList = new BadUnorderedList<Integer>();
            break;
            default:
                newList = null;
        }
        // replicate state of list before change
        // first size elements are in the list before the change
        for(int i = 0; i < length; i++)
        {
            newList.addToRear(elements[i]);
        }
        // apply change to the list
        // constructor just creates list, doesn't change it
        if(changeMethod != Methods.constructor)
        {
            applyChange(newList,changeMethod,elements);
        }

        return newList;
    }

    /**
     * Applies method that creates change scenario.
     * @param list apply change to
     * @param changeMethod method applying to list
     * @param list of arguments to the method, which are last elements, if any
     */
    @SuppressWarnings("incomplete-switch")
    private void applyChange(UnorderedList<Integer> list, Methods changeMethod,
            Integer... elements)
    {
        switch (changeMethod)
        {
            case addToFront:
                list.addToFront(elements[elements.length - 1]);
            break;
            case addToRear:
                list.addToRear(elements[elements.length - 1]);
            break;
            case addAfter:
                list.addAfter(elements[elements.length - 2],
                        elements[elements.length - 1]);
            break;
            case removeFirst:
                list.removeFirst();
            break;
            case removeLast:
                list.removeLast();
            break;
            case remove:
                list.remove(elements[elements.length - 1]);
            break;
        }
    }

    /**
     * Print test results in a consistent format
     * @param testDesc description of the test
     * @param result indicates if the test passed or failed
     */
    private void printTest(String testDesc, boolean result)
    {
        // update number of tests run
        total++;
        if(result)
        {
            passes++;// update number of tests passed
        }
        else
        {
            failures++;// update number of tests failed 
        }
        // print the results of the test 
        System.out.printf("%-46s\t%s\n",testDesc,
                (result ? "   PASS" : "***FAIL***"));
    }

    /**
     * Print results of all tests.
     */
    private void printFinalSummary()
    {
        System.out.printf("\nTotal Tests: %d,  Passed: %d,  Failed: %d\n",total,
                passes,failures);
    }

    /**
     * Run tests to confirm required functionality of list methods
     */
    private void runTests(ListType type)
    {
        listType = type;
        noList_constructorTests();//1
        emptyList_addToFrontTests();//2
        emptyList_addToRearTests();//3
        oneElementList_removeFirstTests();//4
        oneElementList_addToFrontTests();//7
        oneElementList_addToRearTests();//8
        twoElementList_removeSecondElementTests();//13
        twoElementList_addToFrontTests();//14
        twoElementList_addToRearTests();//15
        twoElementList_addAfterFirstElementTests();//16
        twoElementList_addAfterSecondElementTests();//17
        threeElementList_removeFirstTests();//18
        threeElementList_removeLastTests();//19
        threeElementList_removeFirstElementTests();//20

        printFinalSummary();
    }

    // Scenario 1 - no list -> constructor -> []
    private void noList_constructorTests()
    {
        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("noList_constructor_testAddToFront_A",testAddToFront(
                newList(Methods.constructor,0),A,Results.NoException));
        printTest("noList_constructor_testAddToRear_A",testAddToRear(
                newList(Methods.constructor,0),A,Results.NoException));
        printTest("noList_constructor_testAddAfter_B_A",testAddAfter(
                newList(Methods.constructor,0),B,A,Results.ElementNotFound));
        printTest("noList_constructor_testRemoveFirst",testRemoveFirst(
                newList(Methods.constructor,0),Results.EmptyCollection));
        printTest("noList_constructor_testRemoveLast",testRemoveLast(
                newList(Methods.constructor,0),Results.EmptyCollection));
        printTest("noList_constructor_testRemove_A",testRemove(
                newList(Methods.constructor,0),A,Results.ElementNotFound));
        printTest("noList_constructor_testFirst",testFirst(
                newList(Methods.constructor,0),Results.EmptyCollection));
        printTest("noList_constructor_testLast",testLast(
                newList(Methods.constructor,0),Results.EmptyCollection));
        printTest("noList_constructor_testContains_A",
                testContains(newList(Methods.constructor,0),A,Results.False));
        printTest("noList_constructor_testIsEmpty",
                testIsEmpty(newList(Methods.constructor,0),Results.True));
        printTest("noList_constructor_testSize",
                testSize(newList(Methods.constructor,0),0));
        printTest("noList_constructor_testIterator",testIterator(
                newList(Methods.constructor,0),Results.NoException));
        printTest("noList_constructor_testToString",testToString(
                newList(Methods.constructor,0),Results.NoException));
    }

    // Scenario 2 - [ ] -> addToFront(A) -> [A]
    private void emptyList_addToFrontTests()
    {
        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("emptyList_addToFront_testAddToFront_A",testAddToFront(
                newList(Methods.addToFront,0,A),A,Results.NoException));
        printTest("emptyList_addToFront_testAddToRear_B",testAddToRear(
                newList(Methods.addToFront,0,A),B,Results.NoException));
        printTest("emptyList_addToFront_testAddAfter_C_A",testAddAfter(
                newList(Methods.addToFront,0,A),C,A,Results.NoException));
        printTest("emptyList_addToFront_testAddAfter_C_B",testAddAfter(
                newList(Methods.addToFront,0,A),C,B,Results.ElementNotFound));
        printTest("emptyList_addToFront_testRemoveFirst",testRemoveFirst(
                newList(Methods.addToFront,0,A),Results.NoException));
        printTest("emptyList_addToFront_testRemoveLast",testRemoveLast(
                newList(Methods.addToFront,0,A),Results.NoException));
        printTest("emptyList_addToFront_testRemove_A",testRemove(
                newList(Methods.addToFront,0,A),A,Results.NoException));
        printTest("emptyList_addToFront_testRemove_B",testRemove(
                newList(Methods.addToFront,0,A),B,Results.ElementNotFound));
        printTest("emptyList_addToFront_testFirst",
                testFirst(newList(Methods.addToFront,0,A),Results.NoException));
        printTest("emptyList_addToFront_testLast",
                testLast(newList(Methods.addToFront,0,A),Results.NoException));
        printTest("emptyList_addToFront_testContains_A",
                testContains(newList(Methods.addToFront,0,A),A,Results.True));
        printTest("emptyList_addToFront_testContains_B",
                testContains(newList(Methods.addToFront,0,A),B,Results.False));
        printTest("emptyList_addToFront_testIsEmpty",
                testIsEmpty(newList(Methods.addToFront,0,A),Results.False));
        printTest("emptyList_addToFront_testSize",
                testSize(newList(Methods.addToFront,0,A),1));
        printTest("emptyList_addToFront_testIterator",testIterator(
                newList(Methods.addToFront,0,A),Results.NoException));
        printTest("emptyList_addToFront_testToString",testToString(
                newList(Methods.addToFront,0,A),Results.NoException));
    }

    // Scenario 3 - [ ] -> addToRear(A) -> [A]
    private void emptyList_addToRearTests()
    {
        printTest("emptyList_addToRear_testRemoveFirst",testRemoveFirst(
                newList(Methods.addToRear,0,A),Results.NoException));
        printTest("emptyList_addToRear_testRemoveLast",testRemoveLast(
                newList(Methods.addToRear,0,A),Results.NoException));
        printTest("emptyList_addToRear_testRemove_A",testRemove(
                newList(Methods.addToRear,0,A),A,Results.NoException));
        printTest("emptyList_addToRear_testRemove_B",testRemove(
                newList(Methods.addToRear,0,A),B,Results.ElementNotFound));
        printTest("emptyList_addToRear_testFirst",
                testFirst(newList(Methods.addToRear,0,A),Results.NoException));
        printTest("emptyList_addToRear_testLast",
                testLast(newList(Methods.addToRear,0,A),Results.NoException));
        printTest("emptyList_addToRear_testContains_A",
                testContains(newList(Methods.addToRear,0,A),A,Results.True));
        printTest("emptyList_addToRear_testContains_B",
                testContains(newList(Methods.addToRear,0,A),B,Results.False));
        printTest("emptyList_addToRear_testIsEmpty",
                testIsEmpty(newList(Methods.addToRear,0,A),Results.False));
        printTest("emptyList_addToRear_testSize",
                testSize(newList(Methods.addToRear,0,A),1));
        printTest("emptyList_addToRear_testIterator",testIterator(
                newList(Methods.addToRear,0,A),Results.NoException));
        printTest("oneElementList_addToRear_testToString",testToString(
                newList(Methods.addToRear,0,A),Results.NoException));
        printTest("emptyList_addToRear_testAddToRear_A",testAddToRear(
                newList(Methods.addToRear,0,A),A,Results.NoException));
        printTest("emptyList_addToRear_testAddToFront_A",testAddToFront(
                newList(Methods.addToRear,0,A),A,Results.NoException));
        printTest("emptyList_addToRear_testAddAfter_B_A",testAddAfter(
                newList(Methods.addToRear,0,A),B,A,Results.NoException));
        printTest("emptyList_addToRear_testAddAfter_C_B",testAddAfter(
                newList(Methods.addToRear,0,A),C,B,Results.ElementNotFound));
    }

    // Scenario 4 - [A] -> removeFirst -> []
    private void oneElementList_removeFirstTests()
    {
        printTest("oneElementList_RemoveFirst_testRemoveFirst",testRemoveFirst(
                newList(Methods.removeFirst,1,A),Results.EmptyCollection));
        printTest("oneElementList_RemoveFirst_testRemoveLast",testRemoveLast(
                newList(Methods.removeFirst,1,A),Results.EmptyCollection));
        printTest("oneElementList_RemoveFirst_testRemove_A",testRemove(
                newList(Methods.removeFirst,1,A),A,Results.ElementNotFound));
        printTest("oneElementList_RemoveFirst_testFirst",testFirst(
                newList(Methods.removeFirst,1,A),Results.EmptyCollection));
        printTest("oneElementList_RemoveFirst_testLast",testLast(
                newList(Methods.removeFirst,1,A),Results.EmptyCollection));
        printTest("oneElementList_RemoveFirst_testContains_A",
                testContains(newList(Methods.removeFirst,1,A),A,Results.False));
        printTest("oneELmentList_RemoveFirst_testIsEmpty",
                testIsEmpty(newList(Methods.removeFirst,1,A),Results.True));
        printTest("oneElementList_RemoveFirst_testSize",
                testSize(newList(Methods.removeFirst,1,A),0));
        printTest("oneElementList_RemoveFirst_testIterator",testIterator(
                newList(Methods.removeFirst,1,A),Results.NoException));
        printTest("oneElementList_RemoveFirst_testToString",testToString(
                newList(Methods.removeFirst,1,A),Results.NoException));
        printTest("oneElementList_RemoveFirst_testAddToFront_A",testAddToFront(
                newList(Methods.removeFirst,1,A),A,Results.NoException));
        printTest("oneElementList_RemoveFirst_testAddToRear_A",testAddToRear(
                newList(Methods.removeFirst,1,A),A,Results.NoException));
        printTest("oneElementList_RemoveFirst_testAddAfter_B_A",testAddAfter(
                newList(Methods.removeFirst,1,A),B,A,Results.ElementNotFound));
    }

    // Scenario 7 - [A] -> addToFront(B) -> [B, A] 
    private void oneElementList_addToFrontTests()
    {
        printTest("oneElementList_addToFront_testRemoveFirst",testRemoveFirst(
                newList(Methods.addToFront,2,B,A),Results.NoException));
        printTest("oneElementList_addToFront_testRemoveLast",testRemoveLast(
                newList(Methods.addToFront,2,B,A),Results.NoException));
        printTest("oneElementList_addToFront_testRemove_A",testRemove(
                newList(Methods.addToFront,2,B,A),A,Results.NoException));
        printTest("oneElementList_addToFront_testRemove_B",testRemove(
                newList(Methods.addToFront,2,B,A),B,Results.NoException));
        printTest("oneElementList_addToFront_testRemove_C",testRemove(
                newList(Methods.addToFront,2,B,A),C,Results.ElementNotFound));
        printTest("oneElementList_addToFront_testFirst",testFirst(
                newList(Methods.addToFront,2,B,A),Results.NoException));
        printTest("oneElementList_addToFront_testLast",testLast(
                newList(Methods.addToFront,2,B,A),Results.NoException));
        printTest("oneElementList_addToFront_testContains_A",
                testContains(newList(Methods.addToFront,2,B,A),A,Results.True));
        printTest("oneElementList_addToFront_testContains_B",
                testContains(newList(Methods.addToFront,2,B,A),B,Results.True));
        printTest("oneElementList_addToFront_testContains_C",testContains(
                newList(Methods.addToFront,2,B,A),C,Results.False));
        printTest("oneELmentList_addToFront_testIsEmpty",
                testIsEmpty(newList(Methods.addToFront,2,B,A),Results.False));
        printTest("oneElementList_addToFront_testSize",
                testSize(newList(Methods.addToFront,2,B,A),3));
        printTest("oneElementList_addToFront_testIterator",testIterator(
                newList(Methods.addToFront,2,B,A),Results.NoException));
        printTest("noList_constructor_testToString",testToString(
                newList(Methods.addToFront,2,B,A),Results.NoException));
        printTest("oneElementList_addToFront_testAddToFront_B",testAddToFront(
                newList(Methods.addToFront,2,B,A),B,Results.NoException));
        printTest("oneElementList_addToFront_testAddToRear_B",testAddToRear(
                newList(Methods.addToFront,2,B,A),B,Results.NoException));
        printTest("oneElementList_addToFront_testAddAfter_C_A",testAddAfter(
                newList(Methods.addToFront,2,B,A),C,A,Results.NoException));
        printTest("oneElementList_addToFront_testAddAfter_C_B",testAddAfter(
                newList(Methods.addToFront,2,B,A),C,B,Results.NoException));
        printTest("oneElementList_addToFront_testAddAfter_C_D",testAddAfter(
                newList(Methods.addToFront,2,B,A),C,D,Results.ElementNotFound));
    }

    // Scenario 8 - [A] -> addToRear(B) -> [A, B]
    private void oneElementList_addToRearTests()
    {
        printTest("oneElementList_addToRear_testRemoveFirst",testRemoveFirst(
                newList(Methods.addToRear,2,A,B),Results.NoException));
        printTest("oneElementList_addToRear_testRemoveLast",testRemoveLast(
                newList(Methods.addToRear,2,A,B),Results.NoException));
        printTest("oneElementList_addToRear_testRemove_A",testRemove(
                newList(Methods.addToRear,2,B,A),A,Results.NoException));
        printTest("oneElementList_addToRear_testRemove_B",testRemove(
                newList(Methods.addToRear,2,A,B),B,Results.NoException));
        printTest("oneElementList_addToRear_testRemove_C",testRemove(
                newList(Methods.addToRear,2,A,B),C,Results.ElementNotFound));
        printTest("oneElementList_addToRear_testFirst",testFirst(
                newList(Methods.addToRear,2,A,B),Results.NoException));
        printTest("oneElementList_addToRear_testLast",
                testLast(newList(Methods.addToRear,2,A,B),Results.NoException));
        printTest("oneElementList_addToRear_testContains_A",
                testContains(newList(Methods.addToRear,2,A,B),A,Results.True));
        printTest("oneElementList_addToRear_testContains_B",
                testContains(newList(Methods.addToRear,2,A,B),B,Results.True));
        printTest("oneElementList_addToRear_testContains_C",
                testContains(newList(Methods.addToRear,2,A,B),C,Results.False));
        printTest("oneELmentList_addToRear_testIsEmpty",
                testIsEmpty(newList(Methods.addToRear,2,A,B),Results.False));
        printTest("oneElementList_addToRear_testSize",
                testSize(newList(Methods.addToRear,2,A,B),3));
        printTest("oneElementList_addToRear_testIterator",testIterator(
                newList(Methods.addToRear,2,A,B),Results.NoException));
        printTest("oneELementList_addToRear_testToString",testToString(
                newList(Methods.addToRear,2,A,B),Results.NoException));
        printTest("oneElementList_addToRear_testAddToFront_B",testAddToFront(
                newList(Methods.addToRear,2,A,B),B,Results.NoException));
        printTest("oneElementList_addToRear_testAddToRear_B",testAddToRear(
                newList(Methods.addToFront,2,A,B),B,Results.NoException));
        printTest("oneElementList_addToRear_testAddAfter_C_A",testAddAfter(
                newList(Methods.addToRear,2,A,B),C,A,Results.NoException));
        printTest("oneElementList_addToRear_testAddAfter_C_B",testAddAfter(
                newList(Methods.addToRear,2,A,B),C,B,Results.NoException));
        printTest("oneElementList_addToRear_testAddAfter_C_D",testAddAfter(
                newList(Methods.addToRear,2,A,B),C,D,Results.ElementNotFound));
    }

    // Scenario 13 - [A, B] -> remove(B) -> [A]
    private void twoElementList_removeSecondElementTests()
    {
        printTest("twoElementList_Remove_testRemoveFirst",testRemoveFirst(
                newList(Methods.remove,2,A,B),Results.NoException));
        printTest("twoElementList_Remove_testRemoveLast",testRemoveLast(
                newList(Methods.remove,2,A,B),Results.NoException));
        printTest("twoElementList_Remove_testRemove_A",testRemove(
                newList(Methods.remove,2,A,B),A,Results.NoException));
        printTest("twoElementList_Remove_testRemove_B",testRemove(
                newList(Methods.remove,2,A,B),B,Results.ElementNotFound));
        printTest("twoElementList_Remove_testFirst",
                testFirst(newList(Methods.remove,2,A,B),Results.NoException));
        printTest("twoElementList_Remove_testLast",
                testLast(newList(Methods.remove,2,A,B),Results.NoException));
        printTest("twoElementList_Remove_testContains_A",
                testContains(newList(Methods.remove,2,A,B),A,Results.True));
        printTest("twoElementList_Remove_testContains_B",
                testContains(newList(Methods.remove,2,A,B),B,Results.False));
        printTest("twoELmentList_Remove_testIsEmpty",
                testIsEmpty(newList(Methods.remove,2,A,B),Results.False));
        printTest("twoElementList_Remove_testSize",
                testSize(newList(Methods.remove,2,A,B),1));
        printTest("twoElementList_Remove_testIterator",testIterator(
                newList(Methods.remove,2,A,B),Results.NoException));
        printTest("twoELementList_Remove_testToString",testToString(
                newList(Methods.remove,2,A,B),Results.NoException));
        printTest("twoElementList_Remove_testAddToFront_B",testAddToFront(
                newList(Methods.addToRear,2,A,B),B,Results.NoException));
        printTest("twoElementList_Remove_testAddToRear_B",testAddToRear(
                newList(Methods.addToFront,2,A,B),B,Results.NoException));
        printTest("twoElementList_Remove_testAddAfter_C_A",testAddAfter(
                newList(Methods.remove,2,A,B),C,A,Results.NoException));
        printTest("twoElementList_Remove_testAddAfter_C_B",testAddAfter(
                newList(Methods.remove,2,A,B),C,B,Results.ElementNotFound));
    }

    // Scenario 14 - [A, B] -> addToFront(C) -> [C, A, B]
    private void twoElementList_addToFrontTests()
    {
        printTest("twoElementList_addToFront_testRemoveFirst",testRemoveFirst(
                newList(Methods.addToFront,3,C,A,B),Results.NoException));
        printTest("twoElementList_addToFront_testRemoveLast",testRemoveLast(
                newList(Methods.addToFront,3,C,A,B),Results.NoException));
        printTest("twoElementList_addToFront_testRemove_A",testRemove(
                newList(Methods.addToFront,3,C,A,B),A,Results.NoException));
        printTest("twoElementList_addToFront_testRemove_B",testRemove(
                newList(Methods.addToFront,3,C,A,B),B,Results.NoException));
        printTest("twoElementList_addToFront_testRemove_C",testRemove(
                newList(Methods.addToFront,3,C,A,B),C,Results.NoException));
        printTest("twoElementList_addToFront_testRemove_D",testRemove(
                newList(Methods.addToFront,3,C,A,B),D,Results.ElementNotFound));
        printTest("twoElementList_addToFront_testFirst",testFirst(
                newList(Methods.addToFront,3,C,A,B),Results.NoException));
        printTest("oneElementList_addToFront_testLast",testLast(
                newList(Methods.addToFront,3,C,A,B),Results.NoException));
        printTest("twoElementList_addToFront_testContains_A",testContains(
                newList(Methods.addToFront,3,C,A,B),A,Results.True));
        printTest("twoElementList_addToFront_testContains_B",testContains(
                newList(Methods.addToFront,3,C,A,B),B,Results.True));
        printTest("twoElementList_addToFront_testContains_C",testContains(
                newList(Methods.addToFront,3,C,A,B),C,Results.True));
        printTest("twoElementList_addToFront_testContains_D",testContains(
                newList(Methods.addToFront,3,C,A,B),D,Results.False));
        printTest("twoELmentList_addToFront_testIsEmpty",
                testIsEmpty(newList(Methods.addToFront,3,C,A,B),Results.False));
        printTest("twoElementList_addToFront_testSize",
                testSize(newList(Methods.addToFront,3,C,A,B),4));
        printTest("twoElementList_addToFront_testIterator",testIterator(
                newList(Methods.addToFront,3,C,A,B),Results.NoException));
        printTest("twoELementList_addToFront_testToString",testToString(
                newList(Methods.addToFront,3,C,A,B),Results.NoException));
        printTest("twoElementList_addToFront_testAddToFront_C",testAddToFront(
                newList(Methods.addToFront,3,C,A,B),C,Results.NoException));
        printTest("twoElementList_addToFront_testAddToRear_C",testAddToFront(
                newList(Methods.addToFront,3,C,A,B),C,Results.NoException));
        printTest("twoElementList_addToFront_testAddAfter_D_C",testAddAfter(
                newList(Methods.addToFront,3,C,A,B),D,C,Results.NoException));
        printTest("twoElementList_addToFront_testAddAfter_D_B",testAddAfter(
                newList(Methods.addToFront,3,C,A,B),D,B,Results.NoException));
        printTest("twoElementList_addToFront_testAddAfter_D_A",testAddAfter(
                newList(Methods.addToFront,3,C,A,B),D,A,Results.NoException));
        printTest("twoElementList_addToFront_testAddAfter_D_E",
                testAddAfter(newList(Methods.addToFront,3,C,A,B),D,E,
                        Results.ElementNotFound));
    }

    // Scenario 15 - [A, B] -> addToRear -> [A, B, C]
    private void twoElementList_addToRearTests()
    {
        printTest("twoElementList_addToRear_testRemoveFirst",testRemoveFirst(
                newList(Methods.addToRear,3,A,B,C),Results.NoException));
        printTest("twoElementList_addToRear_testRemoveLast",testRemoveLast(
                newList(Methods.addToRear,3,A,B,C),Results.NoException));
        printTest("twoElementList_addToRear_testRemove_A",testRemove(
                newList(Methods.addToRear,3,A,B,C),A,Results.NoException));
        printTest("twoElementList_addToRear_testRemove_B",testRemove(
                newList(Methods.addToRear,3,A,B,C),B,Results.NoException));
        printTest("twoElementList_addToRear_testRemove_C",testRemove(
                newList(Methods.addToRear,3,A,B,C),C,Results.NoException));
        printTest("twoElementList_addToRear_testRemove_D",testRemove(
                newList(Methods.addToRear,3,A,B,C),D,Results.ElementNotFound));
        printTest("twoElementList_addToFront_testFirst",testFirst(
                newList(Methods.addToRear,3,A,B,C),Results.NoException));
        printTest("oneElementList_addToRear_testLast",testLast(
                newList(Methods.addToRear,3,A,B,C),Results.NoException));
        printTest("twoElementList_addToRear_testContains_A",testContains(
                newList(Methods.addToRear,3,A,B,C),A,Results.True));
        printTest("twoElementList_addToRear_testContains_B",testContains(
                newList(Methods.addToRear,3,A,B,C),B,Results.True));
        printTest("twoElementList_addToRear_testContains_C",testContains(
                newList(Methods.addToRear,3,A,B,C),C,Results.True));
        printTest("twoElementList_addToRear_testContains_D",testContains(
                newList(Methods.addToRear,3,A,B,C),D,Results.False));
        printTest("twoELmentList_addToRear_testIsEmpty",
                testIsEmpty(newList(Methods.addToRear,3,A,B,C),Results.False));
        printTest("twoElementList_addToRear_testSize",
                testSize(newList(Methods.addToRear,3,A,B,C),4));
        printTest("twoElementList_addToRear_testIterator",testIterator(
                newList(Methods.addToRear,3,A,B,C),Results.NoException));
        printTest("twoELementList_addToRear_testToString",testToString(
                newList(Methods.addToRear,3,A,B,C),Results.NoException));
        printTest("twoElementList_addToRear_testAddToFront_C",testAddToFront(
                newList(Methods.addToRear,3,A,B,C),C,Results.NoException));
        printTest("twoElementList_addToRear_testAddToRear_C",testAddToFront(
                newList(Methods.addToRear,3,A,B,C),C,Results.NoException));
        printTest("twoElementList_addToRear_testAddAfter_D_C",testAddAfter(
                newList(Methods.addToRear,3,A,B,C),D,C,Results.NoException));
        printTest("twoElementList_addToRear_testAddAfter_D_B",testAddAfter(
                newList(Methods.addToRear,3,A,B,C),D,B,Results.NoException));
        printTest("twoElementList_addToRear_testAddAfter_D_A",testAddAfter(
                newList(Methods.addToRear,3,A,B,C),D,A,Results.NoException));
        printTest("twoElementList_addToRear_testAddAfter_D_E",
                testAddAfter(newList(Methods.addToRear,3,A,B,C),D,E,
                        Results.ElementNotFound));
    }

    // Scenario 16 - [A, B] -> addAfter(C, A) -> [A, C, B]
    private void twoElementList_addAfterFirstElementTests()
    {
        printTest("twoElementList_addAfter(C, A)_testRemoveFirst",
                testRemoveFirst(newList(Methods.addAfter,3,A,C,B),
                        Results.NoException));
        printTest("twoElementList_addAfter(C, A)_testRemoveLast",testRemoveLast(
                newList(Methods.addAfter,3,A,C,B),Results.NoException));
        printTest("twoElementList_addAfter(C, A)_testRemove_A",testRemove(
                newList(Methods.addAfter,3,A,C,B),A,Results.NoException));
        printTest("twoElementList_addAfter(C, A)_testRemove_B",testRemove(
                newList(Methods.addAfter,3,A,C,B),B,Results.NoException));
        printTest("twoElementList_addAfter(C, A)_testRemove_C",testRemove(
                newList(Methods.addAfter,3,A,C,B),C,Results.NoException));
        printTest("twoElementList_addAfter(C, A)_testRemove_D",testRemove(
                newList(Methods.addAfter,3,A,C,B),D,Results.ElementNotFound));
        printTest("twoElementList_addAfter(C, A)_testFirst",testFirst(
                newList(Methods.addAfter,3,A,C,B),Results.NoException));
        printTest("oneElementList_addAfter(C, A)_testLast",testLast(
                newList(Methods.addToFront,3,A,C,B),Results.NoException));
        printTest("twoElementList_addAfter(C, A)_testContains_A",
                testContains(newList(Methods.addAfter,3,A,C,B),A,Results.True));
        printTest("twoElementList_addAfter(C, A)_testContains_B",
                testContains(newList(Methods.addAfter,3,A,C,B),B,Results.True));
        printTest("twoElementList_addAfter(C, A)_testContains_C",
                testContains(newList(Methods.addAfter,3,A,C,B),C,Results.True));
        printTest("twoElementList_addAfter(C, A)_testContains_D",testContains(
                newList(Methods.addAfter,3,A,C,B),D,Results.False));
        printTest("twoELmentList_addAfter(C, A)_testIsEmpty",
                testIsEmpty(newList(Methods.addAfter,3,A,C,B),Results.False));
        printTest("twoElementList_addAfter(C, A)_testSize",
                testSize(newList(Methods.addAfter,3,A,C,B),4));
        printTest("twoElementList_addAfter(C, A)_testIterator",testIterator(
                newList(Methods.addAfter,3,A,C,B),Results.NoException));
        printTest("twoELementList_addAfter(C, A)_testToString",testToString(
                newList(Methods.addAfter,3,A,C,B),Results.NoException));
        printTest("twoElementList_addAfter(C, A)_testAddToFront_C",
                testAddToFront(newList(Methods.addAfter,3,A,C,B),C,
                        Results.NoException));
        printTest("twoElementList_addAfter(C, A)_testAddToRear_C",
                testAddToFront(newList(Methods.addAfter,3,A,C,B),C,
                        Results.NoException));
        printTest("twoElementList_addAfter(C, A)_testAddAfter_C_A",testAddAfter(
                newList(Methods.addAfter,3,A,C,B),C,A,Results.NoException));
        printTest("twoElementList_addAfter(C, A)_testAddAfter_C_B",testAddAfter(
                newList(Methods.addAfter,3,A,C,B),C,B,Results.NoException));
        printTest("twoElementList_addAfter(C, A)_testAddAfter_C_C",testAddAfter(
                newList(Methods.addAfter,3,A,C,B),C,C,Results.NoException));
        printTest("twoElementList_addAfter(C, A)_testAddAfter_C_E",testAddAfter(
                newList(Methods.addAfter,3,A,C,B),C,E,Results.ElementNotFound));
    }

    // Scenario 17 - [A, B] -> addAfter(C, B) -> [A, B, C]
    private void twoElementList_addAfterSecondElementTests()
    {
        printTest("twoElementList_addAfter(C, B)_testRemoveFirst",
                testRemoveFirst(newList(Methods.addAfter,3,A,B,C),
                        Results.NoException));
        printTest("twoElementList_addAfter(C, B)_testRemoveLast",testRemoveLast(
                newList(Methods.addAfter,3,A,B,C),Results.NoException));
        printTest("twoElementList_addAfter(C, B)_testRemove_A",testRemove(
                newList(Methods.addAfter,3,A,B,C),A,Results.NoException));
        printTest("twoElementList_addAfter(C, B)_testRemove_B",testRemove(
                newList(Methods.addAfter,3,A,B,C),B,Results.NoException));
        printTest("twoElementList_addAfter(C, B)_testRemove_C",testRemove(
                newList(Methods.addAfter,3,A,B,C),C,Results.NoException));
        printTest("twoElementList_addAfter(C, B)_testRemove_D",testRemove(
                newList(Methods.addAfter,3,A,B,C),D,Results.ElementNotFound));
        printTest("twoElementList_addAfter(C, B)_testFirst",testFirst(
                newList(Methods.addAfter,3,A,B,C),Results.NoException));
        printTest("oneElementList_addAfter(C, B)_testLast",testLast(
                newList(Methods.addAfter,3,A,B,C),Results.NoException));
        printTest("twoElementList_addAfter(C, B)_testContains_A",
                testContains(newList(Methods.addAfter,3,A,B,C),A,Results.True));
        printTest("twoElementList_addAfter(C, B)_testContains_B",
                testContains(newList(Methods.addAfter,3,A,B,C),B,Results.True));
        printTest("twoElementList_addAfter(C, B)_testContains_C",
                testContains(newList(Methods.addAfter,3,A,B,C),C,Results.True));
        printTest("twoElementList_addAfter(C, B)_testContains_D",testContains(
                newList(Methods.addAfter,3,A,B,C),D,Results.False));
        printTest("twoELmentList_addAfter(C, B)_testIsEmpty",
                testIsEmpty(newList(Methods.addAfter,3,A,B,C),Results.False));
        printTest("twoElementList_addAfter(C, B)_testSize",
                testSize(newList(Methods.addAfter,3,A,B,C),4));
        printTest("twoElementList_addAfter(C, B)_testIterator",testIterator(
                newList(Methods.addAfter,3,A,B,C),Results.NoException));
        printTest("twoELementList_addAfter(C, B)_testToString",testToString(
                newList(Methods.addAfter,3,A,B,C),Results.NoException));
        printTest("twoElementList_addAfter(C, B)_testAddToFront_C",
                testAddToFront(newList(Methods.addAfter,3,A,B,C),C,
                        Results.NoException));
        printTest("twoElementList_addAfter(C, B)_testAddToRear_C",
                testAddToFront(newList(Methods.addAfter,3,A,B,C),C,
                        Results.NoException));
        printTest("twoElementList_addAfter(C, B)_testAddAfter_D_C",testAddAfter(
                newList(Methods.addAfter,3,A,B,C),D,C,Results.NoException));
        printTest("twoElementList_addAfter(C, B)_testAddAfter_D_B",testAddAfter(
                newList(Methods.addAfter,3,A,B,C),D,B,Results.NoException));
        printTest("twoElementList_addAfter(C, B)_testAddAfter_D_A",testAddAfter(
                newList(Methods.addAfter,3,A,B,C),D,A,Results.NoException));
        printTest("twoElementList_addAfter(C, B)_testAddAfter_D_E",testAddAfter(
                newList(Methods.addAfter,3,A,B,C),D,E,Results.ElementNotFound));
    }

    // Scenario 18 - [A, B, C] -> removeFirst -> [B, C]
    private void threeElementList_removeFirstTests()
    {
        printTest("threeElementList_RemoveFirst_testRemoveFirst",
                testRemoveFirst(newList(Methods.removeFirst,2,B,C),
                        Results.NoException));
        printTest("threeElementList_RemoveFirst_testRemoveLast",testRemoveLast(
                newList(Methods.removeFirst,2,B,C),Results.NoException));
        printTest("threeElementList_RemoveFirst_testRemove_A",testRemove(
                newList(Methods.removeFirst,2,B,C),A,Results.ElementNotFound));
        printTest("threeElementList_RemoveFirst_testRemove_B",testRemove(
                newList(Methods.removeFirst,2,B,C),B,Results.ElementNotFound));
        printTest("threeElementList_RemoveFirst_testRemove_C",testRemove(
                newList(Methods.removeFirst,2,B,C),C,Results.NoException));
        printTest("threeElementList_RemoveFirsAfter t_testRemove_D",testRemove(
                newList(Methods.removeFirst,2,B,C),D,Results.ElementNotFound));
        printTest("threeElementList_RemoveFirst_testFirst",testFirst(
                newList(Methods.removeFirst,2,B,C),Results.NoException));
        printTest("threeElementList_RemoveFirst_testLast",testLast(
                newList(Methods.removeFirst,2,B,C),Results.NoException));
        printTest("threeElementList_RemoveFirst_testContains_A",testContains(
                newList(Methods.removeFirst,2,B,C),A,Results.False));
        printTest("threeElementList_RemoveFirst_testContains_B",testContains(
                newList(Methods.removeFirst,2,B,C),B,Results.False));
        printTest("threeElementList_RemoveFirst_testContains_C",testContains(
                newList(Methods.removeFirst,2,B,C),C,Results.True));
        printTest("threeElementList_RemoveFirst_testContains_D",testContains(
                newList(Methods.removeFirst,2,B,C),D,Results.False));
        printTest("threeELmentList_RemoveFirst_testIsEmpty",
                testIsEmpty(newList(Methods.removeFirst,2,B,C),Results.False));
        printTest("threeElementList_RemoveFirst_testSize",
                testSize(newList(Methods.removeFirst,2,B,C),1));
        printTest("threeElementList_RemoveFirst_testIterator",testIterator(
                newList(Methods.removeFirst,2,B,C),Results.NoException));
        printTest("threeELementList_RemoveFirst_testToString",testToString(
                newList(Methods.addAfter,2,B,C),Results.NoException));
        printTest("threeElementList_RemoveFirst_testAddToFront_C",
                testAddToFront(newList(Methods.addAfter,2,B,C),C,
                        Results.NoException));
        printTest("threeElementList_RemoveFirst_testAddToRear_C",testAddToFront(
                newList(Methods.addAfter,2,B,C),C,Results.NoException));
        printTest("threeElementList_RemoveFirst_testAddAfter_D_C",testAddAfter(
                newList(Methods.addAfter,2,B,C),D,C,Results.NoException));
        printTest("threeElementList_RemoveFirst_testAddAfter_D_B",testAddAfter(
                newList(Methods.addAfter,2,B,C),D,B,Results.NoException));
        printTest("threeElementList_RemoveFirst_testAddAfter_D_A",testAddAfter(
                newList(Methods.addAfter,2,B,C),D,A,Results.ElementNotFound));
    }

    // Scenario 19 - [A, B, C] -> removeLast -> [A, B]
    private void threeElementList_removeLastTests()
    {
        printTest("threeElementList_RemoveLast_testRemoveFirst",testRemoveFirst(
                newList(Methods.removeLast,2,A,B),Results.NoException));
        printTest("threeElementList_RemoveLast_testRemoveLast",testRemoveLast(
                newList(Methods.removeLast,2,A,B),Results.NoException));
        printTest("threeElementList_RemoveLast_testRemove_A",testRemove(
                newList(Methods.removeLast,2,A,B),A,Results.NoException));
        printTest("threeElementList_RemoveLast_testRemove_B",testRemove(
                newList(Methods.removeLast,2,A,B),B,Results.ElementNotFound));
        printTest("threeElementList_RemoveLast_testRemove_C",testRemove(
                newList(Methods.removeLast,2,A,B),C,Results.ElementNotFound));
        printTest("threeElementList_RemoveLast_testFirst",testFirst(
                newList(Methods.removeLast,2,A,B),Results.NoException));
        printTest("threeElementList_RemoveLast_testLast",testLast(
                newList(Methods.removeLast,2,A,B),Results.NoException));
        printTest("threeElementList_RemoveLast_testContains_A",
                testContains(newList(Methods.removeLast,2,A,B),A,Results.True));
        printTest("threeElementList_RemoveLast_testContains_B",testContains(
                newList(Methods.removeFirst,2,A,B),B,Results.True));
        printTest("threeElementList_RemoveLast_testContains_C",testContains(
                newList(Methods.removeLast,2,A,B),C,Results.False));
        printTest("threeELmentList_RemoveLast_testIsEmpty",
                testIsEmpty(newList(Methods.removeFirst,2,A,B),Results.False));
        printTest("threeElementList_RemoveLast_testSize",
                testSize(newList(Methods.removeFirst,2,A,B),1));
        printTest("threeElementList_RemoveLast_testIterator",testIterator(
                newList(Methods.removeFirst,2,A,B),Results.NoException));
        printTest("threeELementList_RemoveLast_testToString",testToString(
                newList(Methods.removeFirst,2,A,B),Results.NoException));
        printTest("threeElementList_RemoveLast_testAddToFront_C",testAddToFront(
                newList(Methods.removeFirst,2,A,B),C,Results.NoException));
        printTest("threeElementList_RemoveLast_testAddToRear_C",testAddToFront(
                newList(Methods.removeFirst,2,A,B),C,Results.NoException));
        printTest("threeElementList_RemoveLast_testAddAfter_C_A",testAddAfter(
                newList(Methods.addAfter,2,A,B),C,A,Results.NoException));
        printTest("threeElementList_RemoveLast_testAddAfter_C_B",testAddAfter(
                newList(Methods.addAfter,2,A,B),C,B,Results.NoException));
        printTest("threeElementList_RemoveLast_testAddAfter_C_C",testAddAfter(
                newList(Methods.addAfter,2,A,B),C,C,Results.ElementNotFound));
    }

    // Scenario 20 - [A, B, C] -> remove(A) -> [B, C]
    private void threeElementList_removeFirstElementTests()
    {
        printTest("threeElementList_Remove_testRemoveFirst",testRemoveFirst(
                newList(Methods.remove,2,B,C),Results.NoException));
        printTest("threeElementList_Remove_testRemoveLast",testRemoveLast(
                newList(Methods.remove,2,B,C),Results.NoException));
        printTest("threeElementList_Remove_testRemove_A",testRemove(
                newList(Methods.remove,2,B,C),A,Results.ElementNotFound));
        printTest("threeElementList_Remove_testRemove_B",testRemove(
                newList(Methods.remove,2,B,C),B,Results.NoException));
        printTest("threeElementList_Remove_testRemove_C",testRemove(
                newList(Methods.remove,2,B,C),C,Results.ElementNotFound));
        printTest("threeElementList_Remove_testFirst",
                testFirst(newList(Methods.remove,2,B,C),Results.NoException));
        printTest("threeElementList_Remove_testLast",
                testLast(newList(Methods.remove,2,B,C),Results.NoException));
        printTest("threeElementList_Remove_testContains_A",
                testContains(newList(Methods.remove,2,B,C),A,Results.False));
        printTest("threeElementList_Remove_testContains_B",
                testContains(newList(Methods.remove,2,B,C),B,Results.True));
        printTest("threeElementList_Remove_testContains_C",
                testContains(newList(Methods.remove,2,B,C),C,Results.False));
        printTest("threeELmentList_Remove_testIsEmpty",
                testIsEmpty(newList(Methods.remove,2,B,C),Results.False));
        printTest("threeElementList_Remove_testSize",
                testSize(newList(Methods.remove,2,B,C),1));
        printTest("threeElementList_Remove_testIterator",testIterator(
                newList(Methods.remove,2,B,C),Results.NoException));
        printTest("threeELementList_Remove_testToString",testToString(
                newList(Methods.remove,2,B,C),Results.NoException));
        printTest("threeElementList_Remove_testAddToFront_C",testAddToFront(
                newList(Methods.removeFirst,2,B,C),C,Results.NoException));
        printTest("threeElementList_Remove_testAddToRear_C",testAddToFront(
                newList(Methods.removeFirst,2,B,C),C,Results.NoException));
        printTest("threeElementList_Remove_testAddAfter_C_A",testAddAfter(
                newList(Methods.addAfter,2,B,C),C,A,Results.ElementNotFound));
        printTest("threeElementList_Remove_testAddAfter_C_B",testAddAfter(
                newList(Methods.addAfter,2,B,C),C,B,Results.NoException));
        printTest("threeElementList_Remove_testAddAfter_C_C",testAddAfter(
                newList(Methods.addAfter,2,B,C),C,C,Results.NoException));
    }

    /**
     * Runs test for addToFront method
     * @return test success
     */
    private boolean testAddToFront(UnorderedList<Integer> list, Integer i,
            Results expectedResult)
    {
        Results result;
        try
        {
            list.addToFront(i);
            result = Results.NoException;
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n",
                    "newList_testAddToFront",e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for addToRear method
     * @return test success
     */
    private boolean testAddToRear(UnorderedList<Integer> list, Integer i,
            Results expectedResult)
    {
        Results result;
        try
        {
            list.addToRear(i);
            result = Results.NoException;
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n",
                    "newList_testAddToRear",e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for addToAfter method
     * @return test success
     */
    private boolean testAddAfter(UnorderedList<Integer> list, Integer i,
            Integer j, Results expectedResult)
    {
        Results result;
        try
        {
            list.addAfter(i,j);
            result = Results.NoException;
        }
        catch(ElementNotFoundException e)
        {
            result = Results.ElementNotFound;
        }
        catch(Exception e)
        {
            System.out.printf("%s expected %s, caught %s\n",
                    "newList_testAddAfter","ElementNotFoundException",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for removeFirst method
     * @return test success
     */
    private boolean testRemoveFirst(UnorderedList<Integer> list,
            Results expectedResult)
    {
        Results result;
        try
        {
            list.removeFirst();
            result = Results.NoException;
        }
        catch(EmptyCollectionException e)
        {
            result = Results.EmptyCollection;
        }
        catch(Exception e)
        {
            System.out.printf("%s expected %s, caught %s\n",
                    "newList_testRemoveFirst","EmptyCollectionException",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for removeLast method
     * @return test success
     */
    private boolean testRemoveLast(UnorderedList<Integer> list,
            Results expectedResult)
    {
        Results result;
        try
        {
            list.removeLast();
            result = Results.NoException;
        }
        catch(EmptyCollectionException e)
        {
            result = Results.EmptyCollection;
        }
        catch(Exception e)
        {
            System.out.printf("%s expected %s, caught %s\n",
                    "newList_testRemoveLast","EmptyCollectionException",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for remove method
     * @return test success
     */
    private boolean testRemove(UnorderedList<Integer> list, Integer i,
            Results expectedResult)
    {
        Results result;
        try
        {
            list.remove(i);
            result = Results.NoException;
        }
        catch(ElementNotFoundException e)
        {
            result = Results.ElementNotFound;
        }
        catch(Exception e)
        {
            System.out.printf("%s expected %s, caught %s\n",
                    "newList_testRemoveElement","ElementNotFoundException",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for first method
     * @return test success
     */
    private boolean testFirst(UnorderedList<Integer> list,
            Results expectedResult)
    {
        Results result;
        try
        {
            list.first();
            result = Results.NoException;
        }
        catch(EmptyCollectionException e)
        {
            result = Results.EmptyCollection;
        }
        catch(Exception e)
        {
            System.out.printf("%s expected %s, caught %s\n","newList_testFirst",
                    "EmptyCollectionException",e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for last method
     * @return test success
     */
    private boolean testLast(UnorderedList<Integer> list,
            Results expectedResult)
    {
        Results result;
        try
        {
            list.last();
            result = Results.NoException;
        }
        catch(EmptyCollectionException e)
        {
            result = Results.EmptyCollection;
        }
        catch(Exception e)
        {
            System.out.printf("%s expected %s, caught %s\n","newList_testLast",
                    "EmptyCollectionException",e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for contains method
     * @return test success
     */
    private boolean testContains(UnorderedList<Integer> list, Integer i,
            Results expectedResult)
    {
        Results result;
        try
        {
            if(list.contains(i))
            {
                result = Results.True;
            }
            else
            {
                result = Results.False;
            }
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n",
                    "newList_testContains",e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for isEmpty method
     * @return test success
     */
    private boolean testIsEmpty(UnorderedList<Integer> list,
            Results expectedResult)
    {
        Results result;
        try
        {
            if(list.isEmpty())
            {
                result = Results.True;
            }
            else
            {
                result = Results.False;
            }
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","newList_testIsEmpty",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for size method
     * @return test success
     */
    private boolean testSize(UnorderedList<Integer> list, int expectedSize)
    {
        try
        {
            return(list.size() == expectedSize);
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","newList_testSize",
                    e.toString());
            return false;
        }
    }

    /**
     * Runs test for iterator method
     * @return test success
     */
    @SuppressWarnings("unused")
    private boolean testIterator(UnorderedList<Integer> list,
            Results expectedResult)
    {
        Results result;
        try
        {
            Iterator<Integer> it = list.iterator();
            result = Results.NoException;
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n",
                    "newList_testIterator",e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for hasNext method for iterator
     * @return test success
     */
    @SuppressWarnings("unused")
    private boolean testIteratorHasNext(UnorderedList<Integer> list,
            Results expectedResult)
    {
        Results result;
        try
        {
            Iterator<Integer> it = list.iterator();
            if(it.hasNext())
            {
                result = Results.True;
            }
            else
            {
                result = Results.False;
            }
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n",
                    "newList_testIterator",e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for next method for iterator
     * @return test success
     */
    @SuppressWarnings("unused")
    private boolean testIteratorNext(UnorderedList<Integer> list,
            Results expectedResult)
    {
        Results result;
        try
        {
            Iterator<Integer> it = list.iterator();
            it.next();
            result = Results.NoException;
        }
        catch(NoSuchElementException e)
        {
            result = Results.ElementNotFound;
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n",
                    "newList_testIterator",e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for toString method difficult to test - just confirm that
     * default address output has been overridden
     * @return test success
     */
    private boolean testToString(UnorderedList<Integer> list,
            Results expectedResult)
    {
        Results result;
        try
        {
            String str = list.toString();
            System.out.println("toString() output: " + str);
            if(str.length() == 0)
            {
                result = Results.Fail;
            }
            char lastChar = str.charAt(str.length() - 1);
            if(str.contains("@") && !str.contains(" ")
                    && Character.isLetter(str.charAt(0))
                    && (Character.isDigit(lastChar)
                            || (lastChar >= 'a' && lastChar <= 'f')))
            {
                result = Results.Fail;//looks like default toString()
            }
            else
            {
                result = Results.NoException;
            }
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n",
                    "newList_testToString",e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }
}
