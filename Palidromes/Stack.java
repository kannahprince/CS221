/**
 * <code>Stack</code> represents a stack that can store various data types.
 * @author Prince Kannah
 *
 */
public class Stack<T>{
	private Array<T> stack;
	
	/**
	 * Default constructor that creates an empty <code>Stack</code>.
	 */
	public Stack(){
		stack = new Array<T>();
	}
	
	/**
	 * Add elements to the top of the <code>Stack</code>
	 * @param element :the element to add to the stack.
	 */
	public void push(T element){
		stack.insertAt(0, element);
	}
	
	/**
	 * Return the first element in the <code>Stack</code>.
	 * @return :the top element in the <code>Stack</code>.
	 */
	public T peek(){
		return stack.get(0);
	}
	
	/**
	 * Remove the first (top) element in the <code>Stack</code>.
	 * @return :the deleted element
	 */
	public T pop(){
		T delete = stack.get(0); //get element before deleting so it can be returned
		
		if(stack.count() == 0){
			throw new EmptyCollectionException("Empty collection");
		}
		
		stack.deleteAt(0);
		return delete;	
	}
	
	/**
	 * Returns the the status of the <code>Stack</code>.
	 * @return <code>true</code> if the <code>Stack</code> is empty; false otherwise.
	 */
	public boolean isEmpty(){
		if(stack.count() != 0){
			return false;
		}
		return true;
	}
	
	/**
	 * Returns the <code>Stack</code> as a string.
	 */
	public String toString(){
		String str = "";
		for(int i = stack.count(); i >= stack.count(); i--){
			str += stack.toString();
		}
		return str;
	}
	
}
