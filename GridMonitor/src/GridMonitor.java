import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by kanna on 5/11/2017.
 */
public class GridMonitor implements GridMonitorInterface {
	private final String TOP_MIDDLE_ROW = "MT";
	private final String BOTTOM_MIDDLE_ROW = "MB";
	private final String LEFT_MIDDLE_COL = "ML";
	private final String RIGHT_MIDDLE_COL = "MR";
	private Scanner scan;
	private int gridCol, gridRow;
	private double[][] baseGrid;

	public GridMonitor(String filename) throws FileNotFoundException {
		scan = new Scanner(new File(filename));
		gridRow = scan.nextInt();
		gridCol = scan.nextInt();
		baseGrid = new double[gridRow][gridCol];
		fillBase();
	}

	public static String printDangerZones(boolean[][] arr) {
		StringBuilder b = new StringBuilder("");
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				b.append(arr[i][j]).append(" ");
			}
			b.append("\n");
		}
		return b.toString();
	}

	public static String printArray(double[][] arr) {
		StringBuilder b = new StringBuilder("");
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				b.append(arr[i][j]).append(" ");
			}
			b.append("\n");
		}
		return b.toString();
	}

	private void fillBase() {
		for (int i = 0; i < baseGrid.length; i++) {
			for (int j = 0; j < baseGrid[i].length; j++) {
				if (scan.hasNext()) {
					baseGrid[i][j] = scan.nextDouble();
				}
			}
		}
	}

	private String[][] fillDummy() {
		String[][] dummy = new String[gridRow][gridCol];

		// top
		for (int i = 1; i < dummy[0].length - 1; i++) {
			dummy[0][i] = TOP_MIDDLE_ROW;
		}

		// bottom
		for (int i = 1; i < dummy[0].length - 1; i++) {
			dummy[dummy.length - 1][i] = BOTTOM_MIDDLE_ROW;
		}

		// left
		for (int i = 1; i < dummy.length - 1; i++) {
			dummy[i][0] = LEFT_MIDDLE_COL;
		}

		// right
		for (int i = 1; i < dummy.length - 1; i++) {
			dummy[i][dummy[0].length - 1] = RIGHT_MIDDLE_COL;
		}
		return dummy;
	}

	@Override
	public double[][] getBaseGrid() {
		double[][] copy = new double[gridRow][gridCol];
		for (int i = 0; i < copy.length; i++) {
			for (int j = 0; j < copy[i].length; j++) {
				copy[i][j] = baseGrid[i][j];
			}
		}
		return copy;
	}

	@Override
	public double[][] getSurroundingSumGrid() {
		double runningSum = 0;
		double[][] sumGrid = new double[gridRow][gridCol];
		String[][] dummy = fillDummy();

		// 1 x 1 square
		if (gridRow == 1 && gridCol == 1) {
			sumGrid[0][0] = (baseGrid[0][0] * 4);
			return sumGrid;
		}

		for (int row = 0; row < sumGrid.length; row++) {
			for (int col = 0; col < sumGrid[row].length; col++) {
				/* top left cell */
				if (row == 0 && col == 0) {
					runningSum += (baseGrid[0][0] * 2);

					// bottom cell
					runningSum += baseGrid[row + 1][col];

					// right cell
					runningSum += baseGrid[row][col + 1];
				}

				/* top right cell */
				if (row == 0 && col == gridCol - 1) {
					runningSum += (baseGrid[0][gridCol - 1] * 2);
					runningSum += baseGrid[0][gridCol - 2]; // left
					runningSum += baseGrid[1][gridCol - 1]; // bottom
				}

				/* bottom left cell */
				if (col == 0 && row == gridRow - 1) {
					runningSum += (baseGrid[gridRow - 1][0] * 2);
					runningSum += baseGrid[gridRow - 2][0]; // top
					runningSum += baseGrid[gridRow - 1][1]; // right
				}

				/* bottom right cell */
				if (row == gridRow - 1 && col == gridCol - 1) {
					runningSum += (baseGrid[gridRow - 1][gridCol - 1] * 2);
					runningSum += baseGrid[row - 1][col];
					runningSum += baseGrid[row][col - 1];
				}

				/* border cases */
				if (dummy[row][col] != null) {
					if (dummy[row][col].equalsIgnoreCase(BOTTOM_MIDDLE_ROW)) {
						// bottom
						runningSum += baseGrid[row][col];
						runningSum += baseGrid[row][col - 1]; // left
						runningSum += baseGrid[row][col + 1]; // right
						runningSum += baseGrid[row - 1][col]; // top
					} else if (dummy[row][col].equalsIgnoreCase(TOP_MIDDLE_ROW)) {
						// top
						runningSum += baseGrid[row][col];
						runningSum += baseGrid[row][col - 1]; // left
						runningSum += baseGrid[row][col + 1]; // right
						runningSum += baseGrid[row + 1][col]; // bottom

					} else if (dummy[row][col].equalsIgnoreCase(LEFT_MIDDLE_COL)) {
						// left
						runningSum += baseGrid[row][col];
						runningSum += baseGrid[row][col + 1]; // right
						runningSum += baseGrid[row + 1][col]; // bottom
						runningSum += baseGrid[row - 1][col]; // top
					} else // RIGHT_MIDDLE_COL
					{
						// right
						runningSum += baseGrid[row][col];
						runningSum += baseGrid[row][col - 1]; // left
						runningSum += baseGrid[row + 1][col]; // bottom
						runningSum += baseGrid[row - 1][col]; // top
					}
				} else if (row != 0 && col != 0 && col != gridCol - 1 && row != gridRow - 1 && col != gridCol - 1
						&& row != gridRow - 1 && col != gridCol - 1) {
					runningSum += baseGrid[row + 1][col];
					runningSum += baseGrid[row - 1][col];
					runningSum += baseGrid[row][col + 1];
					runningSum += baseGrid[row][col - 1];
				}
				sumGrid[row][col] = runningSum;
				runningSum = 0;
			}
		}
		return sumGrid;
	}

	@Override
	public double[][] getSurroundingAvgGrid() {
		double[][] averageGrid = new double[gridRow][gridCol];
		double[][] sumGrid = getSurroundingSumGrid();
		for (int i = 0; i < averageGrid.length; i++) {
			for (int j = 0; j < averageGrid[i].length; j++) {
				averageGrid[i][j] = (sumGrid[i][j] / 4);
			}
		}
		return averageGrid;
	}

	@Override
	public double[][] getDeltaGrid() {
		double[][] avgGrid = getSurroundingAvgGrid();
		double[][] deltaGrid = new double[gridRow][gridCol];
		for (int i = 0; i < deltaGrid.length; i++) {
			for (int j = 0; j < deltaGrid[i].length; j++) {
				deltaGrid[i][j] = Math.abs((avgGrid[i][j] / 2));
			}
		}
		return deltaGrid;
	}

	public String toString() {
		StringBuilder b = new StringBuilder("");
		for (int i = 0; i < baseGrid.length; i++) {
			for (int j = 0; j < baseGrid[i].length; j++) {
				b.append(baseGrid[i][j]).append(" ");
			}
			b.append("\n");
		}

		return b.toString();
	}

	@Override
	public boolean[][] getDangerGrid() {
		boolean[][] dangerZone = new boolean[gridRow][gridCol];
		double[][] avgGrid = getSurroundingAvgGrid();
		double[][] deltaGrid = getDeltaGrid();
		for (int i = 0; i < dangerZone.length; i++) {
			for (int j = 0; j < dangerZone[i].length; j++) {
				double upperLimit = avgGrid[i][j] + deltaGrid[i][j];
				double lowerLimit = avgGrid[i][j] - deltaGrid[i][j];
				if (baseGrid[i][j] < lowerLimit || baseGrid[i][j] > upperLimit) {
					dangerZone[i][j] = true;
				}
			}
		}
		return dangerZone;
	}
}
