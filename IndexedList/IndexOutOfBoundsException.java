
/**
 * IndexOutOfBoundsException represents the situation in which 
 * the given index is not in the indexed list. 
 *
  * @version summer 2015
 */
public class IndexOutOfBoundsException extends RuntimeException
{
	private static final long serialVersionUID = -5300306259539652076L;

	/**
     * Sets up this exception with an appropriate message.
     */
    public IndexOutOfBoundsException (String collection)
    {
	    super ("The target element is not in this " + collection);
    }
}
