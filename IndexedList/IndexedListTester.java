
import java.util.ArrayList;
import java.util.Iterator;

/**
 * A unit test class for IndexedList. This is a set of black box tests that
 * should work for any implementation of IndexedList interface.
 * @author Matt T, mvail
 */
public class IndexedListTester
{

    // statistics on test cases run 
    private int passes = 0;
    private int failures = 0;
    private int total = 0;
    // elements added to the lists 
    private int A = new Integer(1);
    private int B = new Integer(2);
    private int C = new Integer(3);
    private int D = new Integer(4);
    private int E = new Integer(5);

    // results of a given test
    private enum Results
    {
        EmptyCollection, True, IndexOutOfBounds, ElementNotFound, NoException, False, Fail
    };
    // which method performed in the change scenario
    private enum Methods
    {
        constructor, add, addAt, set, removeFirst, removeLast, remove, removeAt
    };

    // current elements in a given list
    private ArrayList<Integer> originalList = new ArrayList<Integer>();
    // method causing change
    private Methods method;
    // argument for method causing change
    private Integer argument = null;
    // position argument for method causing change
    private int index = -1;

    /**
     * Calls method to run tests
     */
    public static void main(String[] args)
    {
        //to avoid every method being static
        IndexedListTester tester = new IndexedListTester();

        tester.runTests();
    }

    /**
     * Returns a new MyIndexedList object with given elements added and a given
     * change applied.
     * @return a new IndexedList
     */
    private IndexedList<Integer> newList()
    {
        IndexedList<Integer> newList = new MyIndexedList<Integer>();

        // replicate state of list before change is applied to the list
        for(int i = 0; i < originalList.size(); i++)
        {
            newList.add(originalList.get(i));
        }

        // apply change to the list - 
        // constructor doesn't change the list 
        if(method != Methods.constructor)
        {
            applyChange(newList);
        }

        return newList;
    }

    /**
     * Applies method that creates change scenario.
     * @param list apply change to
     * @param changeMethod method applying to list
     * @param list of arguments to the method, which are last elements, if any
     */
    @SuppressWarnings("incomplete-switch")
    private void applyChange(IndexedList<Integer> list)
    {
        switch (method)
        {
            case add:
                list.add(argument);
            break;
            case addAt:
                list.addAt(index,argument);
            break;
            case set:
                list.set(index,argument);
            break;
            case removeAt:
                list.removeAt(index);
            break;
            case removeFirst:
                list.removeFirst();
            break;
            case removeLast:
                list.removeLast();
            break;
        }
    }

    /**
     * Print test results in a consistent format
     * @param testDesc description of the test
     * @param result indicates if the test passed or failed
     */
    private void printTest(String testDesc, boolean result)
    {
        // update number of tests run
        total++;
        if(result)
        {
            passes++;// update number of tests passed
        }
        else
        {
            failures++;// update number of tests failed 
        }
        // print the results of the test 
        System.out.printf("%-46s\t%s\n",testDesc,
                (result ? "   PASS" : "***FAIL***"));
    }

    /**
     * Print results of all tests.
     */
    private void printFinalSummary()
    {
        System.out.printf("\nTotal Tests: %d,  Passed: %d,  Failed: %d\n",total,
                passes,failures);
    }

    /**
     * Run tests to confirm required functionality of IndexedList interface
     * methods
     */
    private void runTests()
    {
        noList_constructorTests();
        emptyList_addTests();
        oneElementList_removeAtElementTests();
        oneElementList_setElementTests();
        oneElementList_addAtElementTests();
        oneElementList_addElementTests();
        twoElementList_removeAtFirstElementTests();
        twoElementList_removeAtSecondElementTests();
        twoElementList_addAtFirstElementTests();
        twoElementList_addAtSecondElementTests();
        twoElementList_addElementTests();
        twoElementList_setFirstElementTests();
        twoElementList_setSecondElementTests();
        threeElementList_removeAtFirstElementTests();
        threeElementList_removeAtSecondElementTests();
        threeElementList_removeAtThirdElementTests();
        threeElementList_setFirstElementTests();
        threeElementList_setSecondElementTests();
        threeElementList_setThirdElementTests();
        threeElementList_addElementTests();
        threeElementList_addAtFirstElementTests();
        threeElementList_addAtSecondElementTests();
        threeElementList_addAtThirdElementTests();

        printFinalSummary();
    }

    // Scenario 1 - no list -> constructor -> []
    private void noList_constructorTests()
    {
        // create new empty list 
        method = Methods.constructor;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("noList_constructor_testAddAt_0_A",
                testAddAt(newList(),0,A,Results.IndexOutOfBounds));
        printTest("noList_constructor_testSet_0_A",
                testSet(newList(),0,A,Results.IndexOutOfBounds));
        printTest("noList_constructor_testAdd_A",
                testAdd(newList(),A,Results.NoException));
        printTest("noList_constructor_testGet_0",
                testGet(newList(),0,Results.IndexOutOfBounds));
        printTest("noList_constructor_testIndexOf_A",
                testIndexOf(newList(),A,Results.ElementNotFound));
        printTest("noList_constructor_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.IndexOutOfBounds));

        printTest("noList_constructor_testRemoveFirst",
                testRemoveFirst(newList(),Results.EmptyCollection));
        printTest("noList_constructor_testRemoveLast",
                testRemoveLast(newList(),Results.EmptyCollection));
        printTest("noList_constructor_testRemove_A",
                testRemove(newList(),A,Results.ElementNotFound));
        printTest("noList_constructor_testFirst",
                testFirst(newList(),Results.EmptyCollection));
        printTest("noList_constructor_testLast",
                testLast(newList(),Results.EmptyCollection));
        printTest("noList_constructor_testContains_A",
                testContains(newList(),A,Results.False));
        printTest("noList_constructor_testIsEmpty",
                testIsEmpty(newList(),Results.True));
        printTest("noList_constructor_testSize",testSize(newList(),0));
        printTest("noList_constructor_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("noList_constructor_testToString",
                testToString(newList(),Results.NoException));
    }

    // Scenario 2 - [] -> add(A) -> [A]
    private void emptyList_addTests()
    {
        // apply change to empty list 
        method = Methods.add;
        argument = A;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("emptyList_add_testAddAt_0_B",
                testAddAt(newList(),0,B,Results.NoException));
        printTest("emptyList_add_testAddAt_1_B",
                testAddAt(newList(),1,B,Results.IndexOutOfBounds));
        printTest("emptyList_add_testSet_0_B",
                testSet(newList(),0,B,Results.NoException));
        printTest("emptyList_add_testSet_1_B",
                testSet(newList(),1,B,Results.IndexOutOfBounds));
        printTest("emptyList_add_testAdd_B",
                testAdd(newList(),A,Results.NoException));
        printTest("emptyList_add_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("emptyList_add_testGet_1",
                testGet(newList(),1,Results.IndexOutOfBounds));
        printTest("emptyList_add_testIndexOf_A",
                testIndexOf(newList(),A,Results.NoException));
        printTest("emptyList_add_testIndexOf_B",
                testIndexOf(newList(),B,Results.ElementNotFound));
        printTest("emptyList_add_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("emptyList_add_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.IndexOutOfBounds));

        printTest("emptyList_add_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("emptyList_add_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("emptyList_add_testRemove_A",
                testRemove(newList(),A,Results.NoException));
        printTest("emptyList_add_testRemove_B",
                testRemove(newList(),B,Results.ElementNotFound));
        printTest("emptyList_add_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("emptyList_add_testLast",
                testLast(newList(),Results.NoException));
        printTest("emptyList_add_testContains_A",
                testContains(newList(),A,Results.True));
        printTest("emptyList_add_testContains_B",
                testContains(newList(),B,Results.False));
        printTest("emptyList_add_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("emptyList_add_testSize",testSize(newList(),1));
        printTest("emptyList_add_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("emptyList_add_testToString",
                testToString(newList(),Results.NoException));
    }

    // Scenario 3 - [A] -> removeAt(0) -> []
    private void oneElementList_removeAtElementTests()
    {
        // set-up original list
        originalList.add(A);
        // apply change to original list
        method = Methods.removeAt;
        index = 0;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("oneElementList_removeAt_testAddAt_0_A",
                testAddAt(newList(),0,A,Results.IndexOutOfBounds));
        printTest("oneElementList_removeAt_testSet_0_A",
                testSet(newList(),0,A,Results.IndexOutOfBounds));
        printTest("oneElementList_removeAt_testAdd_A",
                testAdd(newList(),A,Results.NoException));
        printTest("oneElementList_removeAt_testGet_0",
                testGet(newList(),0,Results.IndexOutOfBounds));
        printTest("oneElementList_removeAt_testIndexOf_A",
                testIndexOf(newList(),A,Results.ElementNotFound));
        printTest("oneElementList_removeAt_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.IndexOutOfBounds));

        printTest("oneElementList_removeAt_testRemoveFirst",
                testRemoveFirst(newList(),Results.EmptyCollection));
        printTest("oneElementList_removeAt_testRemoveLast",
                testRemoveLast(newList(),Results.EmptyCollection));
        printTest("oneElementList_removeAt_testRemove_A",
                testRemove(newList(),A,Results.ElementNotFound));
        printTest("oneElementList_removeAt_testFirst",
                testFirst(newList(),Results.EmptyCollection));
        printTest("oneElementList_removeAt_testLast",
                testLast(newList(),Results.EmptyCollection));
        printTest("oneElementList_removeAt_testContains_A",
                testContains(newList(),A,Results.False));
        printTest("oneElementList_removeAt_testIsEmpty",
                testIsEmpty(newList(),Results.True));
        printTest("oneElementList_removeAt_testSize",testSize(newList(),0));
        printTest("oneElementList_removeAt_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("oneElementList_removeAt_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 4 - [A] -> set(0, B) -> [B]
    private void oneElementList_setElementTests()
    {
        // set-up original list
        originalList.add(A);
        // apply change to original list
        method = Methods.set;
        index = 0;
        argument = B;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("oneElementList_set_testAddAt_0_A",
                testAddAt(newList(),0,A,Results.NoException));
        printTest("oneElementList_set_testAddAt_1_A",
                testAddAt(newList(),1,A,Results.IndexOutOfBounds));
        printTest("oneElementList_set_testSet_0_A",
                testSet(newList(),0,A,Results.NoException));
        printTest("oneElementList_set_testSet_1_A",
                testSet(newList(),1,A,Results.IndexOutOfBounds));
        printTest("oneElementList_set_testAdd_A",
                testAdd(newList(),A,Results.NoException));
        printTest("oneElementList_set_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("oneElementList_set_testGet_1",
                testGet(newList(),1,Results.IndexOutOfBounds));
        printTest("oneElementList_set_testIndexOf_B",
                testIndexOf(newList(),B,Results.NoException));
        printTest("oneElementList_set_testIndexOf_A",
                testIndexOf(newList(),A,Results.ElementNotFound));
        printTest("oneElementList_set_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("oneElementList_set_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.IndexOutOfBounds));

        printTest("oneElementList_set_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("oneElementList_set_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("oneElementList_set_testRemove_B",
                testRemove(newList(),B,Results.NoException));
        printTest("oneElementList_set_testRemove_A",
                testRemove(newList(),A,Results.ElementNotFound));
        printTest("oneElementList_set_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("oneElementList_set_testLast",
                testLast(newList(),Results.NoException));
        printTest("oneElementList_set_testContains_B",
                testContains(newList(),B,Results.True));
        printTest("oneElementList_set_testContains_A",
                testContains(newList(),A,Results.False));
        printTest("oneElementList_set_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("oneElementList_set_testSize",testSize(newList(),1));
        printTest("oneElementList_set_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("oneElementList_set_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 5 - [A] -> addAt(0, B) -> [B, A]
    private void oneElementList_addAtElementTests()
    {
        // set-up original list
        originalList.add(A);
        // apply change to original list
        method = Methods.addAt;
        index = 0;
        argument = B;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("oneElementList_addAt_testAddAt_0_C",
                testAddAt(newList(),0,C,Results.NoException));
        printTest("oneElementList_addAt_testAddAt_1_C",
                testAddAt(newList(),1,C,Results.NoException));
        printTest("oneElementList_addAt_testAddAt_2_C",
                testAddAt(newList(),2,C,Results.IndexOutOfBounds));
        printTest("oneElementList_addAt_testSet_0_C",
                testSet(newList(),0,C,Results.NoException));
        printTest("oneElementList_addAt_testSet_1_C",
                testSet(newList(),1,C,Results.NoException));
        printTest("oneElementList_addAt_testSet_2_C",
                testSet(newList(),2,C,Results.IndexOutOfBounds));
        printTest("oneElementList_addAt_testAdd_C",
                testAdd(newList(),C,Results.NoException));
        printTest("oneElementList_addAt_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("oneElementList_addAt_testGet_1",
                testGet(newList(),1,Results.NoException));
        printTest("oneElementList_addAt_testGet_2",
                testGet(newList(),2,Results.IndexOutOfBounds));
        printTest("oneElementList_addAt_testIndexOf_B",
                testIndexOf(newList(),B,Results.NoException));
        printTest("oneElementList_addAt_testIndexOf_A",
                testIndexOf(newList(),A,Results.NoException));
        printTest("oneElementList_addAt_testIndexOf_C",
                testIndexOf(newList(),C,Results.ElementNotFound));
        printTest("oneElementList_addAt_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("oneElementList_addAt_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.NoException));
        printTest("oneElementList_addAt_testRemoveAt_2",
                testRemoveAt(newList(),2,Results.IndexOutOfBounds));

        printTest("oneElementList_addAt_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("oneElementList_addAt_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("oneElementList_addAt_testRemove_B",
                testRemove(newList(),B,Results.NoException));
        printTest("oneElementList_addAt_testRemove_A",
                testRemove(newList(),A,Results.NoException));
        printTest("oneElementList_addAt_testRemove_C",
                testRemove(newList(),C,Results.ElementNotFound));
        printTest("oneElementList_addAt_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("oneElementList_addAt_testLast",
                testLast(newList(),Results.NoException));
        printTest("oneElementList_addAt_testContains_B",
                testContains(newList(),B,Results.True));
        printTest("oneElementList_addAt_testContains_A",
                testContains(newList(),A,Results.True));
        printTest("oneElementList_addAt_testContains_C",
                testContains(newList(),C,Results.False));
        printTest("oneElementList_addAt_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("oneElementList_addAt_testSize",testSize(newList(),2));
        printTest("oneElementList_addAt_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("oneElementList_addAt_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 6 - [A] -> add(B) -> [A, B]
    private void oneElementList_addElementTests()
    {
        // set-up original list
        originalList.add(A);
        // apply change to original list
        method = Methods.add;
        argument = B;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("oneElementList_add_testAddAt_0_C",
                testAddAt(newList(),0,C,Results.NoException));
        printTest("oneElementList_add_testAddAt_1_C",
                testAddAt(newList(),1,C,Results.NoException));
        printTest("oneElementList_add_testAddAt_2_C",
                testAddAt(newList(),2,C,Results.IndexOutOfBounds));
        printTest("oneElementList_add_testSet_0_C",
                testSet(newList(),0,C,Results.NoException));
        printTest("oneElementList_add_testSet_1_C",
                testSet(newList(),1,C,Results.NoException));
        printTest("oneElementList_add_testSet_2_C",
                testSet(newList(),2,C,Results.IndexOutOfBounds));
        printTest("oneElementList_add_testAdd_C",
                testAdd(newList(),C,Results.NoException));
        printTest("oneElementList_add_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("oneElementList_add_testGet_1",
                testGet(newList(),1,Results.NoException));
        printTest("oneElementList_add_testGet_2",
                testGet(newList(),2,Results.IndexOutOfBounds));
        printTest("oneElementList_add_testIndexOf_B",
                testIndexOf(newList(),B,Results.NoException));
        printTest("oneElementList_add_testIndexOf_A",
                testIndexOf(newList(),A,Results.NoException));
        printTest("oneElementList_add_testIndexOf_C",
                testIndexOf(newList(),C,Results.ElementNotFound));
        printTest("oneElementList_add_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("oneElementList_add_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.NoException));
        printTest("oneElementList_add_testRemoveAt_2",
                testRemoveAt(newList(),2,Results.IndexOutOfBounds));

        printTest("oneElementList_add_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("oneElementList_add_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("oneElementList_add_testRemove_B",
                testRemove(newList(),B,Results.NoException));
        printTest("oneElementList_add_testRemove_A",
                testRemove(newList(),A,Results.NoException));
        printTest("oneElementList_add_testRemove_C",
                testRemove(newList(),C,Results.ElementNotFound));
        printTest("oneElementList_add_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("oneElementList_add_testLast",
                testLast(newList(),Results.NoException));
        printTest("oneElementList_add_testContains_B",
                testContains(newList(),B,Results.True));
        printTest("oneElementList_add_testContains_A",
                testContains(newList(),A,Results.True));
        printTest("oneElementList_add_testContains_C",
                testContains(newList(),C,Results.False));
        printTest("oneElementList_add_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("oneElementList_add_testSize",testSize(newList(),2));
        printTest("oneElementList_add_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("oneElementList_add_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 7 - [A, B] -> removeAt(0) -> [B]
    private void twoElementList_removeAtFirstElementTests()
    {
        // set-up original list
        originalList.add(A);
        originalList.add(B);
        // apply change to original list
        method = Methods.removeAt;
        index = 0;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("twoElementList_removeAtFirst_testAddAt_0_A",
                testAddAt(newList(),0,A,Results.NoException));
        printTest("twoElementList_removeAtFirst_testAddAt_1_A",
                testAddAt(newList(),1,A,Results.IndexOutOfBounds));
        printTest("twoElementList_removeAtFirst_testSet_0_A",
                testSet(newList(),0,A,Results.NoException));
        printTest("twoElementList_removeAtFirst_testSet_1_A",
                testSet(newList(),1,A,Results.IndexOutOfBounds));
        printTest("twoElementList_removeAtFirst_testAdd_A",
                testAdd(newList(),A,Results.NoException));
        printTest("twoElementList_removeAtFirst_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("twoElementList_removeAtFirst_testGet_1",
                testGet(newList(),1,Results.IndexOutOfBounds));
        printTest("twoElementList_removeAtFirst_testIndexOf_B",
                testIndexOf(newList(),B,Results.NoException));
        printTest("twoElementList_removeAtFirst_testIndexOf_A",
                testIndexOf(newList(),A,Results.ElementNotFound));
        printTest("twoElementList_removeAtFirst_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("twoElementList_removeAtFirst_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.IndexOutOfBounds));

        printTest("twoElementList_removeAtFirst_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("twoElementList_removeAtFirst_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("twoElementList_removeAtFirst_testRemove_B",
                testRemove(newList(),B,Results.NoException));
        printTest("twoElementList_removeAtFirst_testRemove_A",
                testRemove(newList(),A,Results.ElementNotFound));
        printTest("twoElementList_removeAtFirst_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("twoElementList_removeAtFirst_testLast",
                testLast(newList(),Results.NoException));
        printTest("twoElementList_removeAtFirst_testContains_B",
                testContains(newList(),B,Results.True));
        printTest("twoElementList_removeAtFirst_testContains_A",
                testContains(newList(),A,Results.False));
        printTest("twoElementList_removeAtFirst_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("twoElementList_removeAtFirst_testSize",
                testSize(newList(),1));
        printTest("twoElementList_removeAtFirst_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("twoElementList_removeAtFirst_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 8 - [A, B] -> removeAt(1) -> [A]
    private void twoElementList_removeAtSecondElementTests()
    {
        // set-up original list
        originalList.add(A);
        originalList.add(B);
        // apply change to original list
        method = Methods.removeAt;
        index = 1;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("twoElementList_removeAt_testAddAt_0_B",
                testAddAt(newList(),0,B,Results.NoException));
        printTest("twoElementList_removeAt_testAddAt_1_B",
                testAddAt(newList(),1,B,Results.IndexOutOfBounds));
        printTest("twoElementList_removeAt_testSet_0_B",
                testSet(newList(),0,B,Results.NoException));
        printTest("twoElementList_removeAt_testSet_1_B",
                testSet(newList(),1,B,Results.IndexOutOfBounds));
        printTest("twoElementList_removeAt_testAdd_B",
                testAdd(newList(),B,Results.NoException));
        printTest("twoElementList_removeAt_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("twoElementList_removeAt_testGet_1",
                testGet(newList(),1,Results.IndexOutOfBounds));
        printTest("twoElementList_removeAt_testIndexOf_A",
                testIndexOf(newList(),A,Results.NoException));
        printTest("twoElementList_removeAt_testIndexOf_B",
                testIndexOf(newList(),B,Results.ElementNotFound));
        printTest("twoElementList_removeAt_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("twoElementList_removeAt_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.IndexOutOfBounds));

        printTest("twoElementList_removeAt_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("twoElementList_removeAt_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("twoElementList_removeAt_testRemove_A",
                testRemove(newList(),A,Results.NoException));
        printTest("twoElementList_removeAt_testRemove_B",
                testRemove(newList(),B,Results.ElementNotFound));
        printTest("twoElementList_removeAt_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("twoElementList_removeAt_testLast",
                testLast(newList(),Results.NoException));
        printTest("twoElementList_removeAt_testContains_A",
                testContains(newList(),A,Results.True));
        printTest("twoElementList_removeAt_testContains_B",
                testContains(newList(),B,Results.False));
        printTest("twoElementList_removeAt_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("twoElementList_removeAt_testSize",testSize(newList(),1));
        printTest("twoElementList_removeAt_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("twoElementList_removeAt_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 9 - [A, B] -> addAt(0, C) -> [C, A, B]
    private void twoElementList_addAtFirstElementTests()
    {
        // set-up original list
        originalList.add(A);
        originalList.add(B);
        // apply change to original list
        method = Methods.addAt;
        index = 0;
        argument = C;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("twoElementList_addAtFirst_testAddAt_0_D",
                testAddAt(newList(),0,D,Results.NoException));
        printTest("twoElementList_addAtFirst_testAddAt_1_D",
                testAddAt(newList(),1,D,Results.NoException));
        printTest("twoElementList_addAtFirst_testAddAt_2_D",
                testAddAt(newList(),2,D,Results.NoException));
        printTest("twoElementList_addAtFirst_testAddAt_3_D",
                testAddAt(newList(),3,D,Results.IndexOutOfBounds));
        printTest("twoElementList_addAtFirst_testSet_0_D",
                testSet(newList(),0,D,Results.NoException));
        printTest("twoElementList_addAtFirst_testSet_1_D",
                testSet(newList(),1,D,Results.NoException));
        printTest("twoElementList_addAtFirst_testSet_2_D",
                testSet(newList(),2,D,Results.NoException));
        printTest("twoElementList_addAtFirst_testSet_3_D",
                testSet(newList(),3,D,Results.IndexOutOfBounds));
        printTest("twoElementList_addAtFirst_testAdd_D",
                testAdd(newList(),D,Results.NoException));
        printTest("twoElementList_addAtFirst_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("twoElementList_addAtFirst_testGet_1",
                testGet(newList(),1,Results.NoException));
        printTest("twoElementList_addAtFirst_testGet_2",
                testGet(newList(),2,Results.NoException));
        printTest("twoElementList_addAtFirst_testGet_3",
                testGet(newList(),3,Results.IndexOutOfBounds));
        printTest("twoElementList_addAtFirst_testIndexOf_C",
                testIndexOf(newList(),C,Results.NoException));
        printTest("twoElementList_addAtFirst_testIndexOf_A",
                testIndexOf(newList(),A,Results.NoException));
        printTest("twoElementList_addAtFirst_testIndexOf_B",
                testIndexOf(newList(),B,Results.NoException));
        printTest("twoElementList_addAtFirst_testIndexOf_D",
                testIndexOf(newList(),D,Results.ElementNotFound));
        printTest("twoElementList_addAtFirst_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("twoElementList_addAtFirst_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.NoException));
        printTest("twoElementList_addAtFirst_testRemoveAt_2",
                testRemoveAt(newList(),2,Results.NoException));
        printTest("twoElementList_addAtFirst_testRemoveAt_3",
                testRemoveAt(newList(),3,Results.IndexOutOfBounds));

        printTest("twoElementList_addAtFirst_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("twoElementList_addAtFirst_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("twoElementList_addAtFirst_testRemove_C",
                testRemove(newList(),C,Results.NoException));
        printTest("twoElementList_addAtFirst_testRemove_A",
                testRemove(newList(),A,Results.NoException));
        printTest("twoElementList_addAtFirst_testRemove_B",
                testRemove(newList(),B,Results.NoException));
        printTest("twoElementList_addAtFirst_testRemove_D",
                testRemove(newList(),D,Results.ElementNotFound));
        printTest("twoElementList_addAtFirst_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("twoElementList_addAtFirst_testLast",
                testLast(newList(),Results.NoException));
        printTest("twoElementList_addAtFirst_testContains_C",
                testContains(newList(),C,Results.True));
        printTest("twoElementList_addAtFirst_testContains_A",
                testContains(newList(),A,Results.True));
        printTest("twoElementList_addAtFirst_testContains_B",
                testContains(newList(),B,Results.True));
        printTest("twoElementList_addAtFirst_testContains_D",
                testContains(newList(),D,Results.False));
        printTest("twoElementList_addAtFirst_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("twoElementList_addAtFirst_testSize",testSize(newList(),3));
        printTest("twoElementList_addAtFirst_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("twoElementList_addAtFirst_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 10 - [A, B] -> addAt(1, C) -> [A, C, B]
    private void twoElementList_addAtSecondElementTests()
    {
        // set-up original list
        originalList.add(A);
        originalList.add(B);
        // apply change to original list
        method = Methods.addAt;
        index = 1;
        argument = C;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("twoElementList_addAtSecond_testAddAt_0_D",
                testAddAt(newList(),0,D,Results.NoException));
        printTest("twoElementList_addAtSecond_testAddAt_1_D",
                testAddAt(newList(),1,D,Results.NoException));
        printTest("twoElementList_addAtSecond_testAddAt_2_D",
                testAddAt(newList(),2,D,Results.NoException));
        printTest("twoElementList_addAtSecond_testAddAt_3_D",
                testAddAt(newList(),3,D,Results.IndexOutOfBounds));
        printTest("twoElementList_addAtSecond_testSet_0_D",
                testSet(newList(),0,D,Results.NoException));
        printTest("twoElementList_addAtSecond_testSet_1_D",
                testSet(newList(),1,D,Results.NoException));
        printTest("twoElementList_addAtSecond_testSet_2_D",
                testSet(newList(),2,D,Results.NoException));
        printTest("twoElementList_addAtSecond_testSet_3_D",
                testSet(newList(),3,D,Results.IndexOutOfBounds));
        printTest("twoElementList_addAtSecond_testAdd_D",
                testAdd(newList(),D,Results.NoException));
        printTest("twoElementList_addAtSecond_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("twoElementList_addAtSecond_testGet_1",
                testGet(newList(),1,Results.NoException));
        printTest("twoElementList_addAtSecond_testGet_2",
                testGet(newList(),2,Results.NoException));
        printTest("twoElementList_addAtSecond_testGet_3",
                testGet(newList(),3,Results.IndexOutOfBounds));
        printTest("twoElementList_addAtSecond_testIndexOf_C",
                testIndexOf(newList(),C,Results.NoException));
        printTest("twoElementList_addAtSecond_testIndexOf_A",
                testIndexOf(newList(),A,Results.NoException));
        printTest("twoElementList_addAtSecond_testIndexOf_B",
                testIndexOf(newList(),B,Results.NoException));
        printTest("twoElementList_addAtSecond_testIndexOf_D",
                testIndexOf(newList(),D,Results.ElementNotFound));
        printTest("twoElementList_addAtSecond_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("twoElementList_addAtSecond_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.NoException));
        printTest("twoElementList_addAtSecond_testRemoveAt_2",
                testRemoveAt(newList(),2,Results.NoException));
        printTest("twoElementList_addAtSecond_testRemoveAt_3",
                testRemoveAt(newList(),3,Results.IndexOutOfBounds));

        printTest("twoElementList_addAtSecond_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("twoElementList_addAtSecond_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("twoElementList_addAtSecond_testRemove_C",
                testRemove(newList(),C,Results.NoException));
        printTest("twoElementList_addAtSecond_testRemove_A",
                testRemove(newList(),A,Results.NoException));
        printTest("twoElementList_addAtSecond_testRemove_B",
                testRemove(newList(),B,Results.NoException));
        printTest("twoElementList_addAtSecond_testRemove_D",
                testRemove(newList(),D,Results.ElementNotFound));
        printTest("twoElementList_addAtSecond_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("twoElementList_addAtSecond_testLast",
                testLast(newList(),Results.NoException));
        printTest("twoElementList_addAtSecond_testContains_C",
                testContains(newList(),C,Results.True));
        printTest("twoElementList_addAtSecond_testContains_A",
                testContains(newList(),A,Results.True));
        printTest("twoElementList_addAtSecond_testContains_B",
                testContains(newList(),B,Results.True));
        printTest("twoElementList_addAtSecond_testContains_D",
                testContains(newList(),D,Results.False));
        printTest("twoElementList_addAtSecond_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("twoElementList_addAtSecond_testSize",testSize(newList(),3));
        printTest("twoElementList_addAtSecond_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("twoElementList_addAtSecond_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 11 - [A, B] -> add(C) -> [A, B, C]
    private void twoElementList_addElementTests()
    {
        // set-up original list
        originalList.add(A);
        originalList.add(B);
        // apply change to original list
        method = Methods.add;
        argument = C;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("twoElementList_add_testAddAt_0_D",
                testAddAt(newList(),0,D,Results.NoException));
        printTest("twoElementList_add_testAddAt_1_D",
                testAddAt(newList(),1,D,Results.NoException));
        printTest("twoElementList_add_testAddAt_2_D",
                testAddAt(newList(),2,D,Results.NoException));
        printTest("twoElementList_add_testAddAt_3_D",
                testAddAt(newList(),3,D,Results.IndexOutOfBounds));
        printTest("twoElementList_add_testSet_0_D",
                testSet(newList(),0,D,Results.NoException));
        printTest("twoElementList_add_testSet_1_D",
                testSet(newList(),1,D,Results.NoException));
        printTest("twoElementList_add_testSet_2_D",
                testSet(newList(),2,D,Results.NoException));
        printTest("twoElementList_add_testSet_3_D",
                testSet(newList(),3,D,Results.IndexOutOfBounds));
        printTest("twoElementList_add_testAdd_D",
                testAdd(newList(),D,Results.NoException));
        printTest("twoElementList_add_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("twoElementList_add_testGet_1",
                testGet(newList(),1,Results.NoException));
        printTest("twoElementList_add_testGet_2",
                testGet(newList(),2,Results.NoException));
        printTest("twoElementList_add_testGet_3",
                testGet(newList(),3,Results.IndexOutOfBounds));
        printTest("twoElementList_add_testIndexOf_C",
                testIndexOf(newList(),C,Results.NoException));
        printTest("twoElementList_add_testIndexOf_A",
                testIndexOf(newList(),A,Results.NoException));
        printTest("twoElementList_add_testIndexOf_B",
                testIndexOf(newList(),B,Results.NoException));
        printTest("twoElementList_add_testIndexOf_D",
                testIndexOf(newList(),D,Results.ElementNotFound));
        printTest("twoElementList_add_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("twoElementList_add_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.NoException));
        printTest("twoElementList_add_testRemoveAt_2",
                testRemoveAt(newList(),2,Results.NoException));
        printTest("twoElementList_add_testRemoveAt_3",
                testRemoveAt(newList(),3,Results.IndexOutOfBounds));

        printTest("twoElementList_add_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("twoElementList_add_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("twoElementList_add_testRemove_C",
                testRemove(newList(),C,Results.NoException));
        printTest("twoElementList_add_testRemove_A",
                testRemove(newList(),A,Results.NoException));
        printTest("twoElementList_add_testRemove_B",
                testRemove(newList(),B,Results.NoException));
        printTest("twoElementList_add_testRemove_D",
                testRemove(newList(),D,Results.ElementNotFound));
        printTest("twoElementList_add_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("twoElementList_add_testLast",
                testLast(newList(),Results.NoException));
        printTest("twoElementList_add_testContains_C",
                testContains(newList(),C,Results.True));
        printTest("twoElementList_add_testContains_A",
                testContains(newList(),A,Results.True));
        printTest("twoElementList_add_testContains_B",
                testContains(newList(),B,Results.True));
        printTest("twoElementList_add_testContains_D",
                testContains(newList(),D,Results.False));
        printTest("twoElementList_add_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("twoElementList_add_testSize",testSize(newList(),3));
        printTest("twoElementList_add_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("twoElementList_add_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 12 - [A, B] -> set(0, C) -> [C, B]
    private void twoElementList_setFirstElementTests()
    {
        // set-up original list
        originalList.add(A);
        originalList.add(B);
        // apply change to original list
        method = Methods.set;
        index = 0;
        argument = C;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("twoElementList_setFirst_testAddAt_0_A",
                testAddAt(newList(),0,A,Results.NoException));
        printTest("twoElementList_setFirst_testAddAt_1_A",
                testAddAt(newList(),1,A,Results.NoException));
        printTest("twoElementList_setFirst_testAddAt_2_A",
                testAddAt(newList(),2,A,Results.IndexOutOfBounds));
        printTest("twoElementList_setFirst_testSet_0_A",
                testSet(newList(),0,A,Results.NoException));
        printTest("twoElementList_setFirst_testSet_1_A",
                testSet(newList(),1,A,Results.NoException));
        printTest("twoElementList_setFirst_testSet_2_A",
                testSet(newList(),2,A,Results.IndexOutOfBounds));
        printTest("twoElementList_setFirst_testAdd_A",
                testAdd(newList(),A,Results.NoException));
        printTest("twoElementList_setFirst_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("twoElementList_setFirst_testGet_1",
                testGet(newList(),1,Results.NoException));
        printTest("twoElementList_setFirst_testGet_2",
                testGet(newList(),2,Results.IndexOutOfBounds));
        printTest("twoElementList_setFirst_testIndexOf_B",
                testIndexOf(newList(),B,Results.NoException));
        printTest("twoElementList_setFirst_testIndexOf_C",
                testIndexOf(newList(),C,Results.NoException));
        printTest("twoElementList_setFirst_testIndexOf_A",
                testIndexOf(newList(),A,Results.ElementNotFound));
        printTest("twoElementList_setFirst_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("twoElementList_setFirst_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.NoException));
        printTest("twoElementList_setFirst_testRemoveAt_2",
                testRemoveAt(newList(),2,Results.IndexOutOfBounds));

        printTest("twoElementList_setFirst_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("twoElementList_setFirst_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("twoElementList_setFirst_testRemove_B",
                testRemove(newList(),B,Results.NoException));
        printTest("twoElementList_setFirst_testRemove_C",
                testRemove(newList(),C,Results.NoException));
        printTest("twoElementList_setFirst_testRemove_A",
                testRemove(newList(),A,Results.ElementNotFound));
        printTest("twoElementList_setFirst_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("twoElementList_setFirst_testLast",
                testLast(newList(),Results.NoException));
        printTest("twoElementList_setFirst_testContains_B",
                testContains(newList(),B,Results.True));
        printTest("twoElementList_setFirst_testContains_C",
                testContains(newList(),C,Results.True));
        printTest("twoElementList_setFirst_testContains_A",
                testContains(newList(),A,Results.False));
        printTest("twoElementList_setFirst_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("twoElementList_setFirst_testSize",testSize(newList(),2));
        printTest("twoElementList_setFirst_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("twoElementList_setFirst_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 13 - [A, B] -> set(1, C) -> [A, C]
    private void twoElementList_setSecondElementTests()
    {
        // set-up original list
        originalList.add(A);
        originalList.add(B);
        // apply change to original list
        method = Methods.set;
        index = 1;
        argument = C;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("twoElementList_setSecond_testAddAt_0_B",
                testAddAt(newList(),0,B,Results.NoException));
        printTest("twoElementList_setSecond_testAddAt_1_B",
                testAddAt(newList(),1,B,Results.NoException));
        printTest("twoElementList_setSecond_testAddAt_2_B",
                testAddAt(newList(),2,B,Results.IndexOutOfBounds));
        printTest("twoElementList_setSecond_testSet_0_B",
                testSet(newList(),0,B,Results.NoException));
        printTest("twoElementList_setSecond_testSet_1_B",
                testSet(newList(),1,B,Results.NoException));
        printTest("twoElementList_setSecond_testSet_2_B",
                testSet(newList(),2,B,Results.IndexOutOfBounds));
        printTest("twoElementList_setSecond_testAdd_B",
                testAdd(newList(),B,Results.NoException));
        printTest("twoElementList_setSecond_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("twoElementList_setSecond_testGet_1",
                testGet(newList(),1,Results.NoException));
        printTest("twoElementList_setSecond_testGet_2",
                testGet(newList(),2,Results.IndexOutOfBounds));
        printTest("twoElementList_setSecond_testIndexOf_A",
                testIndexOf(newList(),A,Results.NoException));
        printTest("twoElementList_setSecond_testIndexOf_C",
                testIndexOf(newList(),C,Results.NoException));
        printTest("twoElementList_setSecond_testIndexOf_B",
                testIndexOf(newList(),B,Results.ElementNotFound));
        printTest("twoElementList_setSecond_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("twoElementList_setSecond_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.NoException));
        printTest("twoElementList_setSecond_testRemoveAt_2",
                testRemoveAt(newList(),2,Results.IndexOutOfBounds));

        printTest("twoElementList_setSecond_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("twoElementList_setSecond_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("twoElementList_setSecond_testRemove_A",
                testRemove(newList(),A,Results.NoException));
        printTest("twoElementList_setSecond_testRemove_C",
                testRemove(newList(),C,Results.NoException));
        printTest("twoElementList_setSecond_testRemove_B",
                testRemove(newList(),B,Results.ElementNotFound));
        printTest("twoElementList_setSecond_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("twoElementList_setSecond_testLast",
                testLast(newList(),Results.NoException));
        printTest("twoElementList_setSecond_testContains_A",
                testContains(newList(),A,Results.True));
        printTest("twoElementList_setSecond_testContains_C",
                testContains(newList(),C,Results.True));
        printTest("twoElementList_setSecond_testContains_B",
                testContains(newList(),B,Results.False));
        printTest("twoElementList_setSecond_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("twoElementList_setSecond_testSize",testSize(newList(),2));
        printTest("twoElementList_setSecond_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("twoElementList_setSecond_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 14 - [A, B, C] -> removeAt(0) -> [B, C]
    private void threeElementList_removeAtFirstElementTests()
    {
        // set-up original list
        originalList.add(A);
        originalList.add(B);
        originalList.add(C);
        // apply change to original list
        method = Methods.removeAt;
        index = 0;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("threeElementList_removeAtFirst_testAddAt_0_A",
                testAddAt(newList(),0,A,Results.NoException));
        printTest("threeElementList_removeAtFirst_testAddAt_1_A",
                testAddAt(newList(),1,A,Results.NoException));
        printTest("threeElementList_removeAtFirst_testAddAt_2_A",
                testAddAt(newList(),2,A,Results.IndexOutOfBounds));
        printTest("threeElementList_removeAtFirst_testSet_0_A",
                testSet(newList(),0,A,Results.NoException));
        printTest("threeElementList_removeAtFirst_testSet_1_A",
                testSet(newList(),1,A,Results.NoException));
        printTest("threeElementList_removeAtFirst_testSet_2_A",
                testSet(newList(),2,A,Results.IndexOutOfBounds));
        printTest("threeElementList_removeAtFirst_testAdd_A",
                testAdd(newList(),A,Results.NoException));
        printTest("threeElementList_removeAtFirst_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("threeElementList_removeAtFirst_testGet_1",
                testGet(newList(),1,Results.NoException));
        printTest("threeElementList_removeAtFirst_testGet_2",
                testGet(newList(),2,Results.IndexOutOfBounds));
        printTest("threeElementList_removeAtFirst_testIndexOf_B",
                testIndexOf(newList(),B,Results.NoException));
        printTest("threeElementList_removeAtFirst_testIndexOf_C",
                testIndexOf(newList(),C,Results.NoException));
        printTest("threeElementList_removeAtFirst_testIndexOf_A",
                testIndexOf(newList(),A,Results.ElementNotFound));
        printTest("threeElementList_removeAtFirst_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("threeElementList_removeAtFirst_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.NoException));
        printTest("threeElementList_removeAtFirst_testRemoveAt_2",
                testRemoveAt(newList(),2,Results.IndexOutOfBounds));

        printTest("threeElementList_removeAtFirst_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("threeElementList_removeAtFirst_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("threeElementList_removeAtFirst_testRemove_B",
                testRemove(newList(),B,Results.NoException));
        printTest("threeElementList_removeAtFirst_testRemove_C",
                testRemove(newList(),C,Results.NoException));
        printTest("threeElementList_removeAtFirst_testRemove_A",
                testRemove(newList(),A,Results.ElementNotFound));
        printTest("threeElementList_removeAtFirst_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("threeElementList_removeAtFirst_testLast",
                testLast(newList(),Results.NoException));
        printTest("threeElementList_removeAtFirst_testContains_B",
                testContains(newList(),B,Results.True));
        printTest("threeElementList_removeAtFirst_testContains_C",
                testContains(newList(),C,Results.True));
        printTest("threeElementList_removeAtFirst_testContains_A",
                testContains(newList(),A,Results.False));
        printTest("threeElementList_removeAtFirst_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("threeElementList_removeAtFirst_testSize",
                testSize(newList(),2));
        printTest("threeElementList_removeAtFirst_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("threeElementList_removeAtFirst_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 15 - [A, B, C] -> removeAt(1) -> [A, C]
    private void threeElementList_removeAtSecondElementTests()
    {
        // set-up original list
        originalList.add(A);
        originalList.add(B);
        originalList.add(C);
        // apply change to original list
        method = Methods.removeAt;
        index = 1;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("threeElementList_removeAtSecond_testAddAt_0_B",
                testAddAt(newList(),0,B,Results.NoException));
        printTest("threeElementList_removeAtSecond_testAddAt_1_B",
                testAddAt(newList(),1,B,Results.NoException));
        printTest("threeElementList_removeAtSecond_testAddAt_2_B",
                testAddAt(newList(),2,B,Results.IndexOutOfBounds));
        printTest("threeElementList_removeAtSecond_testSet_0_B",
                testSet(newList(),0,B,Results.NoException));
        printTest("threeElementList_removeAtSecond_testSet_1_B",
                testSet(newList(),1,B,Results.NoException));
        printTest("threeElementList_removeAtSecond_testSet_2_B",
                testSet(newList(),2,B,Results.IndexOutOfBounds));
        printTest("threeElementList_removeAtSecond_testAdd_B",
                testAdd(newList(),B,Results.NoException));
        printTest("threeElementList_removeAtSecond_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("threeElementList_removeAtSecond_testGet_1",
                testGet(newList(),1,Results.NoException));
        printTest("threeElementList_removeAtSecond_testGet_2",
                testGet(newList(),2,Results.IndexOutOfBounds));
        printTest("threeElementList_removeAtSecond_testIndexOf_A",
                testIndexOf(newList(),A,Results.NoException));
        printTest("threeElementList_removeAtSecond_testIndexOf_C",
                testIndexOf(newList(),C,Results.NoException));
        printTest("threeElementList_removeAtSecond_testIndexOf_B",
                testIndexOf(newList(),B,Results.ElementNotFound));
        printTest("threeElementList_removeAtSecond_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("threeElementList_removeAtSecond_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.NoException));
        printTest("threeElementList_removeAtSecond_testRemoveAt_2",
                testRemoveAt(newList(),2,Results.IndexOutOfBounds));

        printTest("threeElementList_removeAtSecond_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("threeElementList_removeAtSecond_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("threeElementList_removeAtSecond_testRemove_A",
                testRemove(newList(),A,Results.NoException));
        printTest("threeElementList_removeAtSecond_testRemove_C",
                testRemove(newList(),C,Results.NoException));
        printTest("threeElementList_removeAtSecond_testRemove_B",
                testRemove(newList(),B,Results.ElementNotFound));
        printTest("threeElementList_removeAtSecond_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("threeElementList_removeAtSecond_testLast",
                testLast(newList(),Results.NoException));
        printTest("threeElementList_removeAtSecond_testContains_A",
                testContains(newList(),A,Results.True));
        printTest("threeElementList_removeAtSecond_testContains_C",
                testContains(newList(),C,Results.True));
        printTest("threeElementList_removeAtSecond_testContains_B",
                testContains(newList(),B,Results.False));
        printTest("threeElementList_removeAtSecond_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("threeElementList_removeAtSecond_testSize",
                testSize(newList(),2));
        printTest("threeElementList_removeAtSecond_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("threeElementList_removeAtSecond_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 16 - [A, B, C] -> removeAt(2) -> [A, B]
    private void threeElementList_removeAtThirdElementTests()
    {
        // set-up original list
        originalList.add(A);
        originalList.add(B);
        originalList.add(C);
        // apply change to original list
        method = Methods.removeAt;
        index = 2;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("threeElementList_removeAtThird_testAddAt_0_C",
                testAddAt(newList(),0,C,Results.NoException));
        printTest("threeElementList_removeAtThird_testAddAt_1_C",
                testAddAt(newList(),1,C,Results.NoException));
        printTest("threeElementList_removeAtThird_testAddAt_2_C",
                testAddAt(newList(),2,C,Results.IndexOutOfBounds));
        printTest("threeElementList_removeAtThird_testSet_0_C",
                testSet(newList(),0,C,Results.NoException));
        printTest("threeElementList_removeAtThird_testSet_1_C",
                testSet(newList(),1,C,Results.NoException));
        printTest("threeElementList_removeAtThird_testSet_2_C",
                testSet(newList(),2,C,Results.IndexOutOfBounds));
        printTest("threeElementList_removeAtThird_testAdd_C",
                testAdd(newList(),C,Results.NoException));
        printTest("threeElementList_removeAtThird_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("threeElementList_removeAtThird_testGet_1",
                testGet(newList(),1,Results.NoException));
        printTest("threeElementList_removeAtThird_testGet_2",
                testGet(newList(),2,Results.IndexOutOfBounds));
        printTest("threeElementList_removeAtThird_testIndexOf_B",
                testIndexOf(newList(),B,Results.NoException));
        printTest("threeElementList_removeAtThird_testIndexOf_A",
                testIndexOf(newList(),A,Results.NoException));
        printTest("threeElementList_removeAtThird_testIndexOf_C",
                testIndexOf(newList(),C,Results.ElementNotFound));
        printTest("threeElementList_removeAtThird_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("threeElementList_removeAtThird_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.NoException));
        printTest("threeElementList_removeAtThird_testRemoveAt_2",
                testRemoveAt(newList(),2,Results.IndexOutOfBounds));

        printTest("threeElementList_removeAtThird_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("threeElementList_removeAtThird_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("threeElementList_removeAtThird_testRemove_B",
                testRemove(newList(),B,Results.NoException));
        printTest("threeElementList_removeAtThird_testRemove_A",
                testRemove(newList(),A,Results.NoException));
        printTest("threeElementList_removeAtThird_testRemove_C",
                testRemove(newList(),C,Results.ElementNotFound));
        printTest("threeElementList_removeAtThird_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("threeElementList_removeAtThird_testLast",
                testLast(newList(),Results.NoException));
        printTest("threeElementList_removeAtThird_testContains_B",
                testContains(newList(),B,Results.True));
        printTest("threeElementList_removeAtThird_testContains_A",
                testContains(newList(),A,Results.True));
        printTest("threeElementList_removeAtThird_testContains_C",
                testContains(newList(),C,Results.False));
        printTest("threeElementList_removeAtThird_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("threeElementList_removeAtThird_testSize",
                testSize(newList(),2));
        printTest("threeElementList_removeAtThird_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("threeElementList_removeAtThird_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 17 - [A, B, C] -> set(0, D) -> [D, B, C]
    private void threeElementList_setFirstElementTests()
    {
        // set-up original list
        originalList.add(A);
        originalList.add(B);
        originalList.add(C);
        // apply change to original list
        method = Methods.set;
        index = 0;
        argument = D;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("threeElementList_setFirst_testAddAt_0_A",
                testAddAt(newList(),0,A,Results.NoException));
        printTest("threeElementList_setFirst_testAddAt_1_A",
                testAddAt(newList(),1,A,Results.NoException));
        printTest("threeElementList_setFirst_testAddAt_2_A",
                testAddAt(newList(),2,A,Results.NoException));
        printTest("threeElementList_setFirst_testAddAt_3_A",
                testAddAt(newList(),3,A,Results.IndexOutOfBounds));
        printTest("threeElementList_setFirst_testSet_0_A",
                testSet(newList(),0,A,Results.NoException));
        printTest("threeElementList_setFirst_testSet_1_A",
                testSet(newList(),1,A,Results.NoException));
        printTest("threeElementList_setFirst_testSet_2_A",
                testSet(newList(),2,A,Results.NoException));
        printTest("threeElementList_setFirst_testSet_3_A",
                testSet(newList(),3,A,Results.IndexOutOfBounds));
        printTest("threeElementList_setFirst_testAdd_A",
                testAdd(newList(),A,Results.NoException));
        printTest("threeElementList_setFirst_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("threeElementList_setFirst_testGet_1",
                testGet(newList(),1,Results.NoException));
        printTest("threeElementList_setFirst_testGet_2",
                testGet(newList(),2,Results.NoException));
        printTest("threeElementList_setFirst_testGet_3",
                testGet(newList(),3,Results.IndexOutOfBounds));
        printTest("threeElementList_setFirst_testIndexOf_C",
                testIndexOf(newList(),C,Results.NoException));
        printTest("threeElementList_setFirst_testIndexOf_D",
                testIndexOf(newList(),D,Results.NoException));
        printTest("threeElementList_setFirst_testIndexOf_B",
                testIndexOf(newList(),B,Results.NoException));
        printTest("threeElementList_setFirst_testIndexOf_A",
                testIndexOf(newList(),A,Results.ElementNotFound));
        printTest("threeElementList_setFirst_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("threeElementList_setFirst_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.NoException));
        printTest("threeElementList_setFirst_testRemoveAt_2",
                testRemoveAt(newList(),2,Results.NoException));
        printTest("threeElementList_setFirst_testRemoveAt_3",
                testRemoveAt(newList(),3,Results.IndexOutOfBounds));

        printTest("threeElementList_setFirst_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("threeElementList_setFirst_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("threeElementList_setFirst_testRemove_C",
                testRemove(newList(),C,Results.NoException));
        printTest("threeElementList_setFirst_testRemove_D",
                testRemove(newList(),D,Results.NoException));
        printTest("threeElementList_setFirst_testRemove_B",
                testRemove(newList(),B,Results.NoException));
        printTest("threeElementList_setFirst_testRemove_A",
                testRemove(newList(),A,Results.ElementNotFound));
        printTest("threeElementList_setFirst_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("threeElementList_setFirst_testLast",
                testLast(newList(),Results.NoException));
        printTest("threeElementList_setFirst_testContains_C",
                testContains(newList(),C,Results.True));
        printTest("threeElementList_setFirst_testContains_D",
                testContains(newList(),D,Results.True));
        printTest("threeElementList_setFirst_testContains_B",
                testContains(newList(),B,Results.True));
        printTest("threeElementList_setFirst_testContains_A",
                testContains(newList(),A,Results.False));
        printTest("threeElementList_setFirst_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("threeElementList_setFirst_testSize",testSize(newList(),3));
        printTest("threeElementList_setFirst_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("threeElementList_setFirst_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 18 - [A, B, C] -> set(1, D) -> [A, D, C]
    private void threeElementList_setSecondElementTests()
    {
        // set-up original list
        originalList.add(A);
        originalList.add(B);
        originalList.add(C);
        // apply change to original list
        method = Methods.set;
        index = 1;
        argument = D;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("threeElementList_setSecond_testAddAt_0_B",
                testAddAt(newList(),0,B,Results.NoException));
        printTest("threeElementList_setSecond_testAddAt_1_B",
                testAddAt(newList(),1,B,Results.NoException));
        printTest("threeElementList_setSecond_testAddAt_2_B",
                testAddAt(newList(),2,B,Results.NoException));
        printTest("threeElementList_setSecond_testAddAt_3_B",
                testAddAt(newList(),3,B,Results.IndexOutOfBounds));
        printTest("threeElementList_setSecond_testSet_0_B",
                testSet(newList(),0,B,Results.NoException));
        printTest("threeElementList_setSecond_testSet_1_B",
                testSet(newList(),1,B,Results.NoException));
        printTest("threeElementList_setSecond_testSet_2_B",
                testSet(newList(),2,B,Results.NoException));
        printTest("threeElementList_setSecond_testSet_3_B",
                testSet(newList(),3,B,Results.IndexOutOfBounds));
        printTest("threeElementList_setSecond_testAdd_B",
                testAdd(newList(),B,Results.NoException));
        printTest("threeElementList_setSecond_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("threeElementList_setSecond_testGet_1",
                testGet(newList(),1,Results.NoException));
        printTest("threeElementList_setSecond_testGet_2",
                testGet(newList(),2,Results.NoException));
        printTest("threeElementList_setSecond_testGet_3",
                testGet(newList(),3,Results.IndexOutOfBounds));
        printTest("threeElementList_setSecond_testIndexOf_C",
                testIndexOf(newList(),C,Results.NoException));
        printTest("threeElementList_setSecond_testIndexOf_D",
                testIndexOf(newList(),D,Results.NoException));
        printTest("threeElementList_setSecond_testIndexOf_A",
                testIndexOf(newList(),A,Results.NoException));
        printTest("threeElementList_setSecond_testIndexOf_B",
                testIndexOf(newList(),B,Results.ElementNotFound));
        printTest("threeElementList_setSecond_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("threeElementList_setSecond_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.NoException));
        printTest("threeElementList_setSecond_testRemoveAt_2",
                testRemoveAt(newList(),2,Results.NoException));
        printTest("threeElementList_setSecond_testRemoveAt_3",
                testRemoveAt(newList(),3,Results.IndexOutOfBounds));

        printTest("threeElementList_setSecond_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("threeElementList_setSecond_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("threeElementList_setSecond_testRemove_C",
                testRemove(newList(),C,Results.NoException));
        printTest("threeElementList_setSecond_testRemove_D",
                testRemove(newList(),D,Results.NoException));
        printTest("threeElementList_setSecond_testRemove_A",
                testRemove(newList(),A,Results.NoException));
        printTest("threeElementList_setSecond_testRemove_B",
                testRemove(newList(),B,Results.ElementNotFound));
        printTest("threeElementList_setSecond_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("threeElementList_setSecond_testLast",
                testLast(newList(),Results.NoException));
        printTest("threeElementList_setSecond_testContains_C",
                testContains(newList(),C,Results.True));
        printTest("threeElementList_setSecond_testContains_D",
                testContains(newList(),D,Results.True));
        printTest("threeElementList_setSecond_testContains_A",
                testContains(newList(),A,Results.True));
        printTest("threeElementList_setSecond_testContains_B",
                testContains(newList(),B,Results.False));
        printTest("threeElementList_setSecond_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("threeElementList_setSecond_testSize",testSize(newList(),3));
        printTest("threeElementList_setSecond_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("threeElementList_setSecond_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 19 - [A, B, C] -> set(2, D) -> [A, B, D]
    private void threeElementList_setThirdElementTests()
    {
        // set-up original list
        originalList.add(A);
        originalList.add(B);
        originalList.add(C);
        // apply change to original list
        method = Methods.set;
        index = 2;
        argument = D;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("threeElementList_setThird_testAddAt_0_C",
                testAddAt(newList(),0,C,Results.NoException));
        printTest("threeElementList_setThird_testAddAt_1_C",
                testAddAt(newList(),1,C,Results.NoException));
        printTest("threeElementList_setThird_testAddAt_2_C",
                testAddAt(newList(),2,C,Results.NoException));
        printTest("threeElementList_setThird_testAddAt_3_C",
                testAddAt(newList(),3,C,Results.IndexOutOfBounds));
        printTest("threeElementList_setThird_testSet_0_C",
                testSet(newList(),0,C,Results.NoException));
        printTest("threeElementList_setThird_testSet_1_C",
                testSet(newList(),1,C,Results.NoException));
        printTest("threeElementList_setThird_testSet_2_C",
                testSet(newList(),2,C,Results.NoException));
        printTest("threeElementList_setThird_testSet_3_C",
                testSet(newList(),3,C,Results.IndexOutOfBounds));
        printTest("threeElementList_setThird_testAdd_C",
                testAdd(newList(),C,Results.NoException));
        printTest("threeElementList_setThird_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("threeElementList_setThird_testGet_1",
                testGet(newList(),1,Results.NoException));
        printTest("threeElementList_setThird_testGet_2",
                testGet(newList(),2,Results.NoException));
        printTest("threeElementList_setThird_testGet_3",
                testGet(newList(),3,Results.IndexOutOfBounds));
        printTest("threeElementList_setThird_testIndexOf_B",
                testIndexOf(newList(),B,Results.NoException));
        printTest("threeElementList_setThird_testIndexOf_D",
                testIndexOf(newList(),D,Results.NoException));
        printTest("threeElementList_setThird_testIndexOf_A",
                testIndexOf(newList(),A,Results.NoException));
        printTest("threeElementList_setThird_testIndexOf_C",
                testIndexOf(newList(),C,Results.ElementNotFound));
        printTest("threeElementList_setThird_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("threeElementList_setThird_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.NoException));
        printTest("threeElementList_setThird_testRemoveAt_2",
                testRemoveAt(newList(),2,Results.NoException));
        printTest("threeElementList_setThird_testRemoveAt_3",
                testRemoveAt(newList(),3,Results.IndexOutOfBounds));

        printTest("threeElementList_setThird_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("threeElementList_setThird_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("threeElementList_setThird_testRemove_B",
                testRemove(newList(),B,Results.NoException));
        printTest("threeElementList_setThird_testRemove_D",
                testRemove(newList(),D,Results.NoException));
        printTest("threeElementList_setThird_testRemove_A",
                testRemove(newList(),A,Results.NoException));
        printTest("threeElementList_setThird_testRemove_C",
                testRemove(newList(),C,Results.ElementNotFound));
        printTest("threeElementList_setThird_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("threeElementList_setThird_testLast",
                testLast(newList(),Results.NoException));
        printTest("threeElementList_setThird_testContains_B",
                testContains(newList(),B,Results.True));
        printTest("threeElementList_setThird_testContains_D",
                testContains(newList(),D,Results.True));
        printTest("threeElementList_setThird_testContains_A",
                testContains(newList(),A,Results.True));
        printTest("threeElementList_setThird_testContains_C",
                testContains(newList(),C,Results.False));
        printTest("threeElementList_setThird_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("threeElementList_setThird_testSize",testSize(newList(),3));
        printTest("threeElementList_setThird_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("threeElementList_setThird_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 20 - [A, B, C] -> add(D) -> [A, B, C, D]
    private void threeElementList_addElementTests()
    {
        // set-up original list
        originalList.add(A);
        originalList.add(B);
        originalList.add(C);
        // apply change to original list
        method = Methods.add;
        argument = D;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("threeElementList_add_testAddAt_0_E",
                testAddAt(newList(),0,E,Results.NoException));
        printTest("threeElementList_add_testAddAt_1_E",
                testAddAt(newList(),1,E,Results.NoException));
        printTest("threeElementList_add_testAddAt_2_E",
                testAddAt(newList(),2,E,Results.NoException));
        printTest("threeElementList_add_testAddAt_3_E",
                testAddAt(newList(),3,E,Results.NoException));
        printTest("threeElementList_add_testAddAt_4_E",
                testAddAt(newList(),4,E,Results.IndexOutOfBounds));
        printTest("threeElementList_add_testSet_0_E",
                testSet(newList(),0,E,Results.NoException));
        printTest("threeElementList_add_testSet_1_E",
                testSet(newList(),1,E,Results.NoException));
        printTest("threeElementList_add_testSet_2_E",
                testSet(newList(),2,E,Results.NoException));
        printTest("threeElementList_add_testSet_3_E",
                testSet(newList(),3,E,Results.NoException));
        printTest("threeElementList_add_testSet_4_E",
                testSet(newList(),4,E,Results.IndexOutOfBounds));
        printTest("threeElementList_add_testAdd_E",
                testAdd(newList(),E,Results.NoException));
        printTest("threeElementList_add_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("threeElementList_add_testGet_1",
                testGet(newList(),1,Results.NoException));
        printTest("threeElementList_add_testGet_2",
                testGet(newList(),2,Results.NoException));
        printTest("threeElementList_add_testGet_3",
                testGet(newList(),3,Results.NoException));
        printTest("threeElementList_add_testGet_4",
                testGet(newList(),4,Results.IndexOutOfBounds));
        printTest("threeElementList_add_testIndexOf_A",
                testIndexOf(newList(),A,Results.NoException));
        printTest("threeElementList_add_testIndexOf_B",
                testIndexOf(newList(),B,Results.NoException));
        printTest("threeElementList_add_testIndexOf_C",
                testIndexOf(newList(),C,Results.NoException));
        printTest("threeElementList_add_testIndexOf_D",
                testIndexOf(newList(),D,Results.NoException));
        printTest("threeElementList_add_testIndexOf_E",
                testIndexOf(newList(),E,Results.ElementNotFound));
        printTest("threeElementList_add_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("threeElementList_add_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.NoException));
        printTest("threeElementList_add_testRemoveAt_2",
                testRemoveAt(newList(),2,Results.NoException));
        printTest("threeElementList_add_testRemoveAt_3",
                testRemoveAt(newList(),3,Results.NoException));
        printTest("threeElementList_add_testRemoveAt_4",
                testRemoveAt(newList(),4,Results.IndexOutOfBounds));

        printTest("threeElementList_add_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("threeElementList_add_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("threeElementList_add_testRemove_A",
                testRemove(newList(),A,Results.NoException));
        printTest("threeElementList_add_testRemove_B",
                testRemove(newList(),B,Results.NoException));
        printTest("threeElementList_add_testRemove_C",
                testRemove(newList(),C,Results.NoException));
        printTest("threeElementList_add_testRemove_D",
                testRemove(newList(),D,Results.NoException));
        printTest("threeElementList_add_testRemove_E",
                testRemove(newList(),E,Results.ElementNotFound));
        printTest("threeElementList_add_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("threeElementList_add_testLast",
                testLast(newList(),Results.NoException));
        printTest("threeElementList_add_testContains_A",
                testContains(newList(),A,Results.True));
        printTest("threeElementList_add_testContains_B",
                testContains(newList(),B,Results.True));
        printTest("threeElementList_add_testContains_C",
                testContains(newList(),C,Results.True));
        printTest("threeElementList_add_testContains_D",
                testContains(newList(),D,Results.True));
        printTest("threeElementList_add_testContains_E",
                testContains(newList(),E,Results.False));
        printTest("threeElementList_add_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("threeElementList_add_testSize",testSize(newList(),4));
        printTest("threeElementList_add_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("threeElementList_add_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 21 - [A, B, C] -> addAt(0, D) -> [D, A, B, C]
    private void threeElementList_addAtFirstElementTests()
    {
        // set-up original list
        originalList.add(A);
        originalList.add(B);
        originalList.add(C);
        // apply change to original list
        method = Methods.addAt;
        index = 0;
        argument = D;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("threeElementList_addAtFirst_testAddAt_0_E",
                testAddAt(newList(),0,E,Results.NoException));
        printTest("threeElementList_addAtFirst_testAddAt_1_E",
                testAddAt(newList(),1,E,Results.NoException));
        printTest("threeElementList_addAtFirst_testAddAt_2_E",
                testAddAt(newList(),2,E,Results.NoException));
        printTest("threeElementList_addAtFirst_testAddAt_3_E",
                testAddAt(newList(),3,E,Results.NoException));
        printTest("threeElementList_addAtFirst_testAddAt_4_E",
                testAddAt(newList(),4,E,Results.IndexOutOfBounds));
        printTest("threeElementList_addAtFirst_testSet_0_E",
                testSet(newList(),0,E,Results.NoException));
        printTest("threeElementList_addAtFirst_testSet_1_E",
                testSet(newList(),1,E,Results.NoException));
        printTest("threeElementList_addAtFirst_testSet_2_E",
                testSet(newList(),2,E,Results.NoException));
        printTest("threeElementList_addAtFirst_testSet_3_E",
                testSet(newList(),3,E,Results.NoException));
        printTest("threeElementList_addAtFirst_testSet_4_E",
                testSet(newList(),4,E,Results.IndexOutOfBounds));
        printTest("threeElementList_addAtFirst_testAdd_E",
                testAdd(newList(),E,Results.NoException));
        printTest("threeElementList_addAtFirst_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("threeElementList_addAtFirst_testGet_1",
                testGet(newList(),1,Results.NoException));
        printTest("threeElementList_addAtFirst_testGet_2",
                testGet(newList(),2,Results.NoException));
        printTest("threeElementList_addAtFirst_testGet_3",
                testGet(newList(),3,Results.NoException));
        printTest("threeElementList_addAtFirst_testGet_4",
                testGet(newList(),4,Results.IndexOutOfBounds));
        printTest("threeElementList_addAtFirst_testIndexOf_A",
                testIndexOf(newList(),A,Results.NoException));
        printTest("threeElementList_addAtFirst_testIndexOf_B",
                testIndexOf(newList(),B,Results.NoException));
        printTest("threeElementList_addAtFirst_testIndexOf_C",
                testIndexOf(newList(),C,Results.NoException));
        printTest("threeElementList_addAtFirst_testIndexOf_D",
                testIndexOf(newList(),D,Results.NoException));
        printTest("threeElementList_addAtFirst_testIndexOf_E",
                testIndexOf(newList(),E,Results.ElementNotFound));
        printTest("threeElementList_addAtFirst_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("threeElementList_addAtFirst_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.NoException));
        printTest("threeElementList_addAtFirst_testRemoveAt_2",
                testRemoveAt(newList(),2,Results.NoException));
        printTest("threeElementList_addAtFirst_testRemoveAt_3",
                testRemoveAt(newList(),3,Results.NoException));
        printTest("threeElementList_addAtFirst_testRemoveAt_4",
                testRemoveAt(newList(),4,Results.IndexOutOfBounds));

        printTest("threeElementList_addAtFirst_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("threeElementList_addAtFirst_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("threeElementList_addAtFirst_testRemove_A",
                testRemove(newList(),A,Results.NoException));
        printTest("threeElementList_addAtFirst_testRemove_B",
                testRemove(newList(),B,Results.NoException));
        printTest("threeElementList_addAtFirst_testRemove_C",
                testRemove(newList(),C,Results.NoException));
        printTest("threeElementList_addAtFirst_testRemove_D",
                testRemove(newList(),D,Results.NoException));
        printTest("threeElementList_addAtFirst_testRemove_E",
                testRemove(newList(),E,Results.ElementNotFound));
        printTest("threeElementList_addAtFirst_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("threeElementList_addAtFirst_testLast",
                testLast(newList(),Results.NoException));
        printTest("threeElementList_addAtFirst_testContains_A",
                testContains(newList(),A,Results.True));
        printTest("threeElementList_addAtFirst_testContains_B",
                testContains(newList(),B,Results.True));
        printTest("threeElementList_addAtFirst_testContains_C",
                testContains(newList(),C,Results.True));
        printTest("threeElementList_addAtFirst_testContains_D",
                testContains(newList(),D,Results.True));
        printTest("threeElementList_addAtFirst_testContains_E",
                testContains(newList(),E,Results.False));
        printTest("threeElementList_addAtFirst_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("threeElementList_addAtFirst_testSize",testSize(newList(),4));
        printTest("threeElementList_addAtFirst_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("threeElementList_addAtFirst_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 22 - [A, B, C] -> addAt(1, D) -> [A, D, B, C]
    private void threeElementList_addAtSecondElementTests()
    {
        // set-up original list
        originalList.add(A);
        originalList.add(B);
        originalList.add(C);
        // apply change to original list
        method = Methods.addAt;
        index = 1;
        argument = D;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("threeElementList_addAtSecond_testAddAt_0_E",
                testAddAt(newList(),0,E,Results.NoException));
        printTest("threeElementList_addAtSecond_testAddAt_1_E",
                testAddAt(newList(),1,E,Results.NoException));
        printTest("threeElementList_addAtSecond_testAddAt_2_E",
                testAddAt(newList(),2,E,Results.NoException));
        printTest("threeElementList_addAtSecond_testAddAt_3_E",
                testAddAt(newList(),3,E,Results.NoException));
        printTest("threeElementList_addAtSecond_testAddAt_4_E",
                testAddAt(newList(),4,E,Results.IndexOutOfBounds));
        printTest("threeElementList_addAtSecond_testSet_0_E",
                testSet(newList(),0,E,Results.NoException));
        printTest("threeElementList_addAtSecond_testSet_1_E",
                testSet(newList(),1,E,Results.NoException));
        printTest("threeElementList_addAtSecond_testSet_2_E",
                testSet(newList(),2,E,Results.NoException));
        printTest("threeElementList_addAtSecond_testSet_3_E",
                testSet(newList(),3,E,Results.NoException));
        printTest("threeElementList_addAtSecond_testSet_4_E",
                testSet(newList(),4,E,Results.IndexOutOfBounds));
        printTest("threeElementList_addAtSecond_testAdd_E",
                testAdd(newList(),E,Results.NoException));
        printTest("threeElementList_addAtSecond_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("threeElementList_addAtSecond_testGet_1",
                testGet(newList(),1,Results.NoException));
        printTest("threeElementList_addAtSecond_testGet_2",
                testGet(newList(),2,Results.NoException));
        printTest("threeElementList_addAtSecond_testGet_3",
                testGet(newList(),3,Results.NoException));
        printTest("threeElementList_addAtSecond_testGet_4",
                testGet(newList(),4,Results.IndexOutOfBounds));
        printTest("threeElementList_addAtSecond_testIndexOf_A",
                testIndexOf(newList(),A,Results.NoException));
        printTest("threeElementList_addAtSecond_testIndexOf_B",
                testIndexOf(newList(),B,Results.NoException));
        printTest("threeElementList_addAtSecond_testIndexOf_C",
                testIndexOf(newList(),C,Results.NoException));
        printTest("threeElementList_addAtSecond_testIndexOf_D",
                testIndexOf(newList(),D,Results.NoException));
        printTest("threeElementList_addAtSecond_testIndexOf_E",
                testIndexOf(newList(),E,Results.ElementNotFound));
        printTest("threeElementList_addAtSecond_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("threeElementList_addAtSecond_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.NoException));
        printTest("threeElementList_addAtSecond_testRemoveAt_2",
                testRemoveAt(newList(),2,Results.NoException));
        printTest("threeElementList_addAtSecond_testRemoveAt_3",
                testRemoveAt(newList(),3,Results.NoException));
        printTest("threeElementList_addAtSecond_testRemoveAt_4",
                testRemoveAt(newList(),4,Results.IndexOutOfBounds));

        printTest("threeElementList_addAtSecond_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("threeElementList_addAtSecond_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("threeElementList_addAtSecond_testRemove_A",
                testRemove(newList(),A,Results.NoException));
        printTest("threeElementList_addAtSecond_testRemove_B",
                testRemove(newList(),B,Results.NoException));
        printTest("threeElementList_addAtSecond_testRemove_C",
                testRemove(newList(),C,Results.NoException));
        printTest("threeElementList_addAtSecond_testRemove_D",
                testRemove(newList(),D,Results.NoException));
        printTest("threeElementList_addAtSecond_testRemove_E",
                testRemove(newList(),E,Results.ElementNotFound));
        printTest("threeElementList_addAtSecond_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("threeElementList_addAtSecond_testLast",
                testLast(newList(),Results.NoException));
        printTest("threeElementList_addAtSecond_testContains_A",
                testContains(newList(),A,Results.True));
        printTest("threeElementList_addAtSecond_testContains_B",
                testContains(newList(),B,Results.True));
        printTest("threeElementList_addAtSecond_testContains_C",
                testContains(newList(),C,Results.True));
        printTest("threeElementList_addAtSecond_testContains_D",
                testContains(newList(),D,Results.True));
        printTest("threeElementList_addAtSecond_testContains_E",
                testContains(newList(),E,Results.False));
        printTest("threeElementList_addAtSecond_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("threeElementList_addAtSecond_testSize",
                testSize(newList(),4));
        printTest("threeElementList_addAtSecond_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("threeElementList_addAtSecond_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    // Scenario 23 - [A, B, C] -> addAt(2, D) -> [A, B, D, C]
    private void threeElementList_addAtThirdElementTests()
    {
        // set-up original list
        originalList.add(A);
        originalList.add(B);
        originalList.add(C);
        // apply change to original list
        method = Methods.addAt;
        index = 2;
        argument = D;

        // run tests - description of test case, method testing, list tested, arguments to method tested, expected result
        printTest("threeElementList_addAtSecond_testAddAt_0_E",
                testAddAt(newList(),0,E,Results.NoException));
        printTest("threeElementList_addAtSecond_testAddAt_1_E",
                testAddAt(newList(),1,E,Results.NoException));
        printTest("threeElementList_addAtSecond_testAddAt_2_E",
                testAddAt(newList(),2,E,Results.NoException));
        printTest("threeElementList_addAtSecond_testAddAt_3_E",
                testAddAt(newList(),3,E,Results.NoException));
        printTest("threeElementList_addAtSecond_testAddAt_4_E",
                testAddAt(newList(),4,E,Results.IndexOutOfBounds));
        printTest("threeElementList_addAtSecond_testSet_0_E",
                testSet(newList(),0,E,Results.NoException));
        printTest("threeElementList_addAtSecond_testSet_1_E",
                testSet(newList(),1,E,Results.NoException));
        printTest("threeElementList_addAtSecond_testSet_2_E",
                testSet(newList(),2,E,Results.NoException));
        printTest("threeElementList_addAtSecond_testSet_3_E",
                testSet(newList(),3,E,Results.NoException));
        printTest("threeElementList_addAtSecond_testSet_4_E",
                testSet(newList(),4,E,Results.IndexOutOfBounds));
        printTest("threeElementList_addAtSecond_testAdd_E",
                testAdd(newList(),E,Results.NoException));
        printTest("threeElementList_addAtSecond_testGet_0",
                testGet(newList(),0,Results.NoException));
        printTest("threeElementList_addAtSecond_testGet_1",
                testGet(newList(),1,Results.NoException));
        printTest("threeElementList_addAtSecond_testGet_2",
                testGet(newList(),2,Results.NoException));
        printTest("threeElementList_addAtSecond_testGet_3",
                testGet(newList(),3,Results.NoException));
        printTest("threeElementList_addAtSecond_testGet_4",
                testGet(newList(),4,Results.IndexOutOfBounds));
        printTest("threeElementList_addAtSecond_testIndexOf_A",
                testIndexOf(newList(),A,Results.NoException));
        printTest("threeElementList_addAtSecond_testIndexOf_B",
                testIndexOf(newList(),B,Results.NoException));
        printTest("threeElementList_addAtSecond_testIndexOf_C",
                testIndexOf(newList(),C,Results.NoException));
        printTest("threeElementList_addAtSecond_testIndexOf_D",
                testIndexOf(newList(),D,Results.NoException));
        printTest("threeElementList_addAtSecond_testIndexOf_E",
                testIndexOf(newList(),E,Results.ElementNotFound));
        printTest("threeElementList_addAtSecond_testRemoveAt_0",
                testRemoveAt(newList(),0,Results.NoException));
        printTest("threeElementList_addAtSecond_testRemoveAt_1",
                testRemoveAt(newList(),1,Results.NoException));
        printTest("threeElementList_addAtSecond_testRemoveAt_2",
                testRemoveAt(newList(),2,Results.NoException));
        printTest("threeElementList_addAtSecond_testRemoveAt_3",
                testRemoveAt(newList(),3,Results.NoException));
        printTest("threeElementList_addAtSecond_testRemoveAt_4",
                testRemoveAt(newList(),4,Results.IndexOutOfBounds));

        printTest("threeElementList_addAtSecond_testRemoveFirst",
                testRemoveFirst(newList(),Results.NoException));
        printTest("threeElementList_addAtSecond_testRemoveLast",
                testRemoveLast(newList(),Results.NoException));
        printTest("threeElementList_addAtSecond_testRemove_A",
                testRemove(newList(),A,Results.NoException));
        printTest("threeElementList_addAtSecond_testRemove_B",
                testRemove(newList(),B,Results.NoException));
        printTest("threeElementList_addAtSecond_testRemove_C",
                testRemove(newList(),C,Results.NoException));
        printTest("threeElementList_addAtSecond_testRemove_D",
                testRemove(newList(),D,Results.NoException));
        printTest("threeElementList_addAtSecond_testRemove_E",
                testRemove(newList(),E,Results.ElementNotFound));
        printTest("threeElementList_addAtSecond_testFirst",
                testFirst(newList(),Results.NoException));
        printTest("threeElementList_addAtSecond_testLast",
                testLast(newList(),Results.NoException));
        printTest("threeElementList_addAtSecond_testContains_A",
                testContains(newList(),A,Results.True));
        printTest("threeElementList_addAtSecond_testContains_B",
                testContains(newList(),B,Results.True));
        printTest("threeElementList_addAtSecond_testContains_C",
                testContains(newList(),C,Results.True));
        printTest("threeElementList_addAtSecond_testContains_D",
                testContains(newList(),D,Results.True));
        printTest("threeElementList_addAtSecond_testContains_E",
                testContains(newList(),E,Results.False));
        printTest("threeElementList_addAtSecond_testIsEmpty",
                testIsEmpty(newList(),Results.False));
        printTest("threeElementList_addAtSecond_testSize",
                testSize(newList(),4));
        printTest("threeElementList_addAtSecond_testIterator",
                testIterator(newList(),Results.NoException));
        printTest("threeElementList_addAtSecond_testToString",
                testToString(newList(),Results.NoException));

        originalList.clear();// get ready for next test 
    }

    /**
     * Runs test for add method
     * @return test success
     */
    private boolean testAdd(IndexedList<Integer> list, Integer i,
            Results expectedResult)
    {
        Results result;
        try
        {
            list.add(i);
            result = Results.NoException;
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testAdd",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for addAt method
     * @return test success
     */
    private boolean testAddAt(IndexedList<Integer> list, int index, Integer i,
            Results expectedResult)
    {
        Results result;
        try
        {
            list.addAt(index,i);
            result = Results.NoException;
        }
        catch(IndexOutOfBoundsException e)
        {
            result = Results.IndexOutOfBounds;
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testAddAt",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for set method
     * @return test success
     */
    private boolean testSet(IndexedList<Integer> list, int index, Integer i,
            Results expectedResult)
    {
        Results result;
        try
        {
            list.set(index,i);
            result = Results.NoException;
        }
        catch(IndexOutOfBoundsException e)
        {
            result = Results.IndexOutOfBounds;
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testSet",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    private boolean testRemoveAt(IndexedList<Integer> list, int index,
            Results expectedResult)
    {
        Results result;
        try
        {
            list.removeAt(index);
            result = Results.NoException;
        }
        catch(IndexOutOfBoundsException e)
        {
            result = Results.IndexOutOfBounds;
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testRemoveAt",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    private boolean testIndexOf(IndexedList<Integer> list, Integer i,
            Results expectedResult)
    {
        Results result;
        try
        {
            list.indexOf(i);
            result = Results.NoException;
        }
        catch(ElementNotFoundException e)
        {
            result = Results.ElementNotFound;
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testIndexOf",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    private boolean testGet(IndexedList<Integer> list, int index,
            Results expectedResult)
    {
        Results result;
        try
        {
            list.get(index);
            result = Results.NoException;
        }
        catch(IndexOutOfBoundsException e)
        {
            result = Results.IndexOutOfBounds;
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testGet",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for removeFirst method
     * @return test success
     */
    private boolean testRemoveFirst(IndexedList<Integer> list,
            Results expectedResult)
    {
        Results result;
        try
        {
            list.removeFirst();
            result = Results.NoException;
        }
        catch(EmptyCollectionException e)
        {
            result = Results.EmptyCollection;
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testRemoveFirst",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for removeLast method
     * @return test success
     */
    private boolean testRemoveLast(IndexedList<Integer> list,
            Results expectedResult)
    {
        Results result;
        try
        {
            list.removeLast();
            result = Results.NoException;
        }
        catch(EmptyCollectionException e)
        {
            result = Results.EmptyCollection;
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testRemoveLast",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for remove method
     * @return test success
     */
    private boolean testRemove(IndexedList<Integer> list, Integer i,
            Results expectedResult)
    {
        Results result;
        try
        {
            list.remove(i);
            result = Results.NoException;
        }
        catch(ElementNotFoundException e)
        {
            result = Results.ElementNotFound;
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testRemove",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for first method
     * @return test success
     */
    private boolean testFirst(IndexedList<Integer> list, Results expectedResult)
    {
        Results result;
        try
        {
            list.first();
            result = Results.NoException;
        }
        catch(EmptyCollectionException e)
        {
            result = Results.EmptyCollection;
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testFirst",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for last method
     * @return test success
     */
    private boolean testLast(IndexedList<Integer> list, Results expectedResult)
    {
        Results result;
        try
        {
            list.last();
            result = Results.NoException;
        }
        catch(EmptyCollectionException e)
        {
            result = Results.EmptyCollection;
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testLast",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for contains method
     * @return test success
     */
    private boolean testContains(IndexedList<Integer> list, Integer i,
            Results expectedResult)
    {
        Results result;
        try
        {
            if(list.contains(i))
            {
                result = Results.True;
            }
            else
            {
                result = Results.False;
            }
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testContains",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for isEmpty method
     * @return test success
     */
    private boolean testIsEmpty(IndexedList<Integer> list,
            Results expectedResult)
    {
        Results result;
        try
        {
            if(list.isEmpty())
            {
                result = Results.True;
            }
            else
            {
                result = Results.False;
            }
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testIsEmpty",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for size method
     * @return test success
     */
    private boolean testSize(IndexedList<Integer> list, int expectedSize)
    {
        try
        {
            return(list.size() == expectedSize);
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testSize",
                    e.toString());
            return false;
        }
    }

    /**
     * Runs test for iterator method
     * @return test success
     */
    private boolean testIterator(IndexedList<Integer> list,
            Results expectedResult)
    {
        Results result;
        try
        {
            list.iterator();
            result = Results.NoException;
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testIterator",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for hasNext method for iterator
     * @return test success
     */
    @SuppressWarnings("unused")
    private boolean testIteratorHasNext(IndexedList<Integer> list,
            Results expectedResult)
    {
        Results result;
        try
        {
            Iterator<Integer> itr = list.iterator();
            if(itr.hasNext())
            {
                result = Results.True;
            }
            else
            {
                result = Results.False;
            }
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testIteratorHasNext",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for next method for iterator
     * @return test success
     */
    @SuppressWarnings("unused")
    private boolean testIteratorNext(IndexedList<Integer> list,
            Results expectedResult)
    {
        Results result;
        try
        {
            Iterator<Integer> itr = list.iterator();
            itr.next();
            result = Results.NoException;
        }
        catch(ElementNotFoundException e)
        {
            result = Results.ElementNotFound;
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testIteratorNext",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }

    /**
     * Runs test for toString method difficult to test - just confirm that
     * default address output has been overridden
     * @return test success
     */
    private boolean testToString(IndexedList<Integer> list,
            Results expectedResult)
    {
        Results result;
        try
        {
            String str = list.toString();
            System.out.println("toString() output: " + str);
            if(str.length() == 0)
            {
                result = Results.Fail;
            }
            else if(str.contains("@"))
            {
                result = Results.Fail;//looks like default toString()
            }
            else
            {
                result = Results.NoException;
            }
        }
        catch(Exception e)
        {
            System.out.printf("%s caught unexpected %s\n","testToString",
                    e.toString());
            result = Results.Fail;
        }
        return result == expectedResult;
    }
}
