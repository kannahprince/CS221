import java.util.Iterator;

@SuppressWarnings("rawtypes")
public class MyIndexedList<T> implements IndexedList<T>
{

    Array<T> list;

    public MyIndexedList()
    {
        list = new Array<T>();
    }

    @Override
    public T removeFirst()
    {

        if(isEmpty())
        {
            throw new EmptyCollectionException("MyIndexedList");
        }
        return list.delete(list.get(0));

    }

    @Override
    public T removeLast()
    {
        if(isEmpty())
        {
            throw new EmptyCollectionException("MyIndexedList");
        }
        return list.delete(list.get(list.count() - 1));
    }

    @Override
    public T remove(T element)
    {
        int temp = list.find(element);
        if(temp == -1)
        {
            throw new ElementNotFoundException("No such element ");
        }
        return list.delete(element);

    }

    @Override
    public T first()
    {
        if(isEmpty())
        {
            throw new EmptyCollectionException("MyIndexedList");
        }
        return list.get(0);
    }

    @Override
    public T last()
    {
        if(isEmpty())
        {
            throw new EmptyCollectionException("MyIndexedList");
        }
        return list.get((list.count() - 1));
    }

    @Override
    public boolean contains(T target)
    {

        if(list.find(target) != -1)
        {
            return true;
        }
        return false;
    }

    @Override
    public boolean isEmpty()
    {
        if(list.count() != 0)
        {
            return false;
        }
        return true;
    }

    @Override
    public int size()
    {
        return list.count();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Iterator iterator()
    {
        return null;
    }

    @Override
    public void addAt(int index, T element)
    {
        T temp = get(index);

        if(index > list.count())
        {
            throw new IndexOutOfBoundsException("MyIndexedList");
        }

        list.insertAfter(element,temp);
    }

    @SuppressWarnings("unused")
    @Override
    public void set(int index, T element)
    {
        T temp = get(index);//DO NOT CHANGE

        if(index > list.count())
        {
            throw new IndexOutOfBoundsException("MyIndexedList");
        }
        list.set(index,element);
    }

    @Override
    public void add(T element)
    {
        list.insertToBack(element);
    }

    @Override
    public T get(int index)
    {
        if(list.get(index) == null)
        {
            throw new IndexOutOfBoundsException("IndexOutOfBounds");
        }
        return list.get(index);
    }

    @Override
    public int indexOf(T element)
    {
        if(list.find(element) == -1)
        {
            throw new ElementNotFoundException("No such element " + element);
        }
        return list.find(element);

    }

    @Override
    public T removeAt(int index)
    {
        if(list.get(index) == null)
        {
            throw new IndexOutOfBoundsException("MyIndexedList");
        }
        return list.delete(list.get(index));
    }

    public String toString()
    {
        return list.toString();
    }

}
