/**
 * Sort class for sorting indexed lists using Quicksort or Mergesort.
 * @author Matt T
 * @version summer 2015
 * @param <T> - generic class of objects index list contains, must implement
 *        Comparable interface
 */
public class Sort<T extends Comparable<T>>
{

    // types of sort
    public enum SortAlgorithm
    {
        quicksort, mergesort
    };

    // instance variable for type of sort
    private SortAlgorithm sortType;

    /**
     * Constructor - specifies type of sort
     * @param sortType
     */
    public Sort(SortAlgorithm sortType)
    {
        this.sortType = sortType;
    }

    /**
     * Sorts a given list using specified algorithm.
     * @param list that implements IndexedList interface
     */
    public void sort(IndexedList<T> list)
    {
        switch (sortType)
        {
            case quicksort:
                quickSort(list);
            break;
            case mergesort:
                mergeSort(list);
            break;
            default:
        }
    }

    /**
     * Uses Quickort algorithm to sort list of values
     * @param list IndexedList of Comparable values
     */
    private void quickSort(IndexedList<T> list)
    {
        quickSort(list,0,list.size() - 1);
    }

    /**
     * Uses Mergesort algorithm to sort list of values
     * @param list IndexedList of Comparable values
     */
    private void mergeSort(IndexedList<T> list)
    {
        IndexedList<T> temp = new MyIndexedList<T>(list.size());
        mergeSort(list,temp,0,list.size() - 1);
    }

    /**
     * A recursive merge sort algorithm
     * @param list - the list to be sorted
     * @param tempList - a temporary list to hold the list as it's being sorted.
     * @param low - the beginning index of the list
     * @param high - the end index of the list.
     */
    private void mergeSort(IndexedList<T> list, IndexedList<T> tempList,
            int low, int high)
    {
        if(low < high)
        {
            int center = (low + high) / 2;
            mergeSort(list,tempList,low,center);
            mergeSort(list,tempList,center + 1,high);
            merge(list,tempList,low,center + 1,high);
        }
    }

    /**
     * Merges two lists
     * @param list - the list to sort
     * @param tempList - a temporary list to hold the list as it's being sorted.
     * @param leftPos - the starting point for the first half of the list.
     * @param rightPos - the starting point for the second half of the list.
     * @param rightEnd - the ending point for the second half of the list.
     */
    private void merge(IndexedList<T> list, IndexedList<T> tempList,
            int leftPos, int rightPos, int rightEnd)
    {
        int leftEnd = rightPos - 1;
        int tempPos = leftPos;
        int numElements = rightEnd - leftPos + 1;

        //main loop
        while (leftPos <= leftEnd && rightPos <= rightEnd)
        {
            if(list.get(leftPos).compareTo(list.get(rightPos)) <= 0)
            {
                tempList.set(tempPos,list.get(leftPos));
                leftPos++;
            }
            else
            {
                tempList.set(tempPos,list.get(rightPos));
                rightPos++;
            }
            tempPos++;
        }

        //copy rest of left half
        while (leftPos <= leftEnd)
        {
            tempList.set(tempPos,list.get(leftPos));
            tempPos++;
            leftPos++;
        }

        //copy rest of right half
        while (rightPos <= rightEnd)
        {
            tempList.set(tempPos,list.get(rightPos));
            tempPos++;
            rightPos++;
        }

        //copy temp back into list
        for(int i = 0; i < numElements; i++, rightEnd--)
        {
            list.set(rightEnd,tempList.get(rightEnd));
        }
    }

    /**
     * A recursive quick sort algorithm
     * @param list - the list to be sorted
     * @param startIndex - the starting index of the list.
     * @param endIndex - the end index of the list.
     */
    private void quickSort(IndexedList<T> list, int startIndex, int endIndex)
    {
        //base case
        if(startIndex >= endIndex)
        {
            return;
        }

        int pivotIndex = (startIndex + endIndex) / 2;

        //place pivot at start
        swap(list,pivotIndex,startIndex);
        T pivotElement = list.get(startIndex);

        //begin partitioning
        int p, j = startIndex;

        for(p = startIndex + 1; p <= endIndex; p++)
        {
            if(list.get(p).compareTo(pivotElement) <= 0)
            {
                j++;
                swap(list,p,j);
            }
        }

        //restore pivot to correct spot & repeat
        swap(list,startIndex,j);
        quickSort(list,startIndex,j - 1);
        quickSort(list,j + 1,endIndex);
    }

    /**
     * Swaps two elements in a list.
     * @param list - the list containing the elements to be swapped.
     * @param index1 - the index containing the first element.
     * @param index2 - the indexed containing the second element.
     */
    private void swap(IndexedList<T> list, int index1, int index2)
    {
        T temp = list.get(index1);
        list.set(index1,list.get(index2));
        list.set(index2,temp);
    }
}
