import java.io.File;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * CircuitValidator checks the specified files to make sure they meet the following
 * file format:
 * <ul>
 * <li>The first row contains two white-space-separated, positive integers.</li>
 * <ul>
 * <li>The first integer specifies the number of rows in a grid.</li>
 * <li>The second integer specifies the number of columns in the grid.</li>
 * </ul>
 * <li>Each subsequent row represents one row of the grid and should contain
 * exactly one white-space-separated double value for each grid column</li>
 * </ul>
 * For example:
 * <table>
 * <tr>
 * <td>5 6</td>
 * </tr>
 * <tr>
 * <td>2.5 0 1 0 0 0</td>
 * </tr>
 * <tr>
 * <td>0 -1 4 0 0 0</td>
 * </tr>
 * <tr>
 * <td>0 0 0 0 1.1 0</td>
 * </tr>
 * <tr>
 * <td>0 2 0 0 5 0</td>
 * </tr>
 * <tr>
 * <td>0 -3.14 0 0 0 0</td>
 * </tr>
 * </table>
 * 
 * @author kanna
 * @version 1.1
 */
public class CircuitValidator {
	/*
	 * The number of dimension cannot exceed this value. We only care about row
	 * & col; anything more the file is invalid
	 */
	private static final int MAX_GRID_DIMENSION = 2;
	private static final int MAX_START_COUNT = 1;
	private static final int MAX_FINISH_COUNT = 1;
	private static int row, col;
	private final static char START = '1';
	private final static char END = '2';
	private final static char OPEN = 'O'; // capital 'o'
	private final static char CLOSED = 'X';
	private final static char TRACE = 'T';
	private static String file;

	/**
	 * Validate the specified file
	 * 
	 * @param fileName
	 *            the grid file to validate
	 * @return <code>FormatError</code> if an error is encountered; null
	 *         otherwise
	 * @see FormatError
	 */
	public static FormatError check(String fileName) {
		try {
			file = fileName;
			Scanner firstLineScanner = new Scanner(new File(file));
			// get first line and remove all white space to
			// get accurate token count
			String[] firstLine = firstLineScanner.nextLine().split(" ");
			firstLineScanner.close();
			if (firstLine.length != MAX_GRID_DIMENSION) {
				return new FormatError("Grid dimensions exceed the expected max (" + MAX_GRID_DIMENSION
						+ "). Specified: " + firstLine.length);
			}
			// check for words/non-integer in first line
			for (String string : firstLine) {
				try {
					Integer.parseInt(string);
				} catch (NumberFormatException e) {
					return new FormatError(e.toString());
				}
			}

			FormatError error = readRow();
			if (error != null) {
				return error;
			}

			error = readCol();
			if (error != null) {
				return error;
			}

			error = rowMismatch();
			if (error != null) {
				return error;
			}

			error = colMismatch();
			if (error != null) {
				return error;
			}

			error = readFile();
			if (error != null) {
				return error;
			}
		} catch (FileNotFoundException e) {
			return new FormatError(e.toString());
		}
		// returning null means the file is valid as no errors were found
		return null;
	}

	/**
	 * MAIN method: valid the specified files
	 * 
	 * @param args
	 *            files to validate
	 */
	/*
	 * public static void main(String[] args) { final int EXIT_ERROR_CODE = 1;
	 * if (args.length < 1) { System.out.println("Usage: \n\t" +
	 * "java CircuitValidator file1 [file2 ... fileN]");
	 * System.exit(EXIT_ERROR_CODE); }
	 * 
	 * for (int i = 0; i < args.length; i++) { System.out.println(args[i]);
	 * FormatError e = check(args[i]); if (e != null) {
	 * System.out.println(e.getException()); } else System.out.println("VALID");
	 * } }
	 */

	/**
	 * Check the columns in the grid to determine if they match the specified
	 * dimenstion
	 * 
	 * @return <code>FormatError</code> if the grid column does not match the
	 *         specified column; null otherwise
	 * @throws FileNotFoundException
	 *             if the specified file is invalid
	 * @see FormatError
	 */
	private static FormatError colMismatch() throws FileNotFoundException {
		Scanner scan = new Scanner(new File(file));
		// skip row & col since those have been read
		scan.nextLine();
		while (scan.hasNext()) {
			String[] line = scan.nextLine().split(" ");
			if (col != line.length) {
				scan.close();
				return new FormatError(
						"Grid column does match the expected max (" + col + ")." + " Grid col: " + line.length);
			}
		}
		scan.close();
		return null;
	}

	/**
	 * Capture the grid column from the file
	 * 
	 * @return <code>FormatError</code> if an error is encountered; null
	 *         otherwise
	 * @throws FileNotFoundException
	 *             if the specified file is invalid
	 * @see FormatError
	 */
	private static FormatError readCol() throws FileNotFoundException {
		Scanner dimScanner = new Scanner(new File(file));
		// skip row
		dimScanner.nextInt();
		try {
			col = Integer.parseInt(dimScanner.next());
			dimScanner.close();
			return null;
		} catch (NumberFormatException e) {
			dimScanner.close();
			return new FormatError(e.toString());
		} catch (InputMismatchException e) {
			dimScanner.close();
			return new FormatError(e.toString());
		}
	}

	/**
	 * Read the body of the file
	 * 
	 * @return <code>FormatError</code> if any error is encountered; null
	 *         otherwise
	 * @throws FileNotFoundException
	 *             if the specified file is invalid
	 * @see FormatError
	 */
	private static FormatError readFile() throws FileNotFoundException {
		Scanner scan = new Scanner(new File(file));
		int startCount = 0;
		int finishCount = 0;
		// skip over row & col since
		// the scanner is reset to
		// the beginning of the file
		scan.nextLine();
		try {
			while (scan.hasNext()) {
				String[] line = scan.nextLine().split(" ");
				for (String string : line) {
					char[] strChar = string.toCharArray();
					for (char c : strChar) {
						if (c == START) {
							startCount++;
						}
						if (c == END) {
							finishCount++;
						}
						// if an invalid character is found, no need to keep
						// reading the file as it's automatically invalid
						if (c != START && c != END && c != OPEN && c != TRACE && c != CLOSED) {
							scan.close();
							return new FormatError(
									"The file contains an invalid character (" + c + ") expected [1, 2, O, T, X]");
						}
					}
				}
			}
			// circuit board files can only have one start & one finishing
			// position
			if (startCount != MAX_START_COUNT) {
				scan.close();
				return new FormatError("The number of starting positions (" + startCount
						+ ") does not match the expected (" + MAX_START_COUNT + ")");
			}
			if (finishCount != MAX_FINISH_COUNT) {
				scan.close();
				return new FormatError("The number of ending positions (" + finishCount
						+ ") does not match the expected (" + MAX_FINISH_COUNT + ")");
			}
			scan.close();
		} catch (NumberFormatException e) {
			scan.close();
			return new FormatError(e.toString());
		} catch (InputMismatchException e) {
			scan.close();
			return new FormatError(e.toString());
		}
		return null;
	}

	/**
	 * Capture the grid row from the file
	 * 
	 * @return <code>FormatError</code> if an error is encountered; null
	 *         otherwise
	 * @throws FileNotFoundException
	 *             if the specified file is invalid
	 * @see FormatError
	 */
	private static FormatError readRow() throws FileNotFoundException {
		Scanner dimScanner = new Scanner(new File(file));
		try {
			row = Integer.parseInt(dimScanner.next());
			dimScanner.close();
			return null;
		} catch (NumberFormatException e) {
			dimScanner.close();
			return new FormatError(e.toString());
		} catch (InputMismatchException e) {
			dimScanner.close();
			return new FormatError(e.toString());
		}
	}

	/**
	 * Check the rows in the grid to determine if they match the specified
	 * dimenstion
	 * 
	 * @return <code>FormatError</code> if the grid row does not match the
	 *         specified column; null otherwise
	 * @throws FileNotFoundException
	 *             if the specified file is invalid
	 * @see FormatError
	 */
	private static FormatError rowMismatch() throws FileNotFoundException {
		int lineCount = 0;
		Scanner scan = new Scanner(new File(file));
		// skip row & col since those have been read
		scan.nextLine();
		while (scan.hasNext()) {
			scan.nextLine();
			lineCount++;
		}
		scan.close();
		if (row != lineCount) {
			return new FormatError(
					"Grid row does not match the expected max (" + row + ")." + " Grid row: " + lineCount);
		}
		return null;
	}
}
