import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Represents a 2D circuit board as read from an input file.
 * 
 * @author mvail
 */
public class CircuitBoard {
	private char[][] board;
	private Point startingPoint;
	private Point endingPoint;

	// constants you may find useful
	private int rows; // initialized in constructor
	private int cols; // initialized in constructor
	private final char OPEN = 'O'; // capital 'o'
	private final char TRACE = 'T';
	private final char START = '1';
	private final char END = '2';

	/**
	 * Construct a CircuitBoard from a given board input file, where the first
	 * line contains the number of rows and columns as ints and each subsequent
	 * line is one row of characters representing the contents of that position.
	 * Valid characters are as follows: 'O' an open position 'X' an occupied,
	 * unavailable position '1' first of two components needing to be connected
	 * '2' second of two components needing to be connected 'T' is not expected
	 * in input files - represents part of the trace connecting components 1 and
	 * 2 in the solution
	 * 
	 * @param filename
	 *            file containing a grid of characters
	 * @throws FileNotFoundException
	 *             if Scanner cannot read the file
	 * @throws InvalidFileFormatException
	 *             for any other format or content issue that prevents reading a
	 *             valid input file
	 */
	public CircuitBoard(String filename) throws FileNotFoundException {
		FormatError error = CircuitValidator.check(filename);
		if (error != null) {
			throw new InvalidFileFormatException(error.getException());
		}

		Scanner fileScan = new Scanner(new File(filename));
		rows = fileScan.nextInt();
		cols = fileScan.nextInt();
		board = new char[rows][cols];
		fillBoard(fileScan);
		fileScan.close();
	}

	/**
	 * Populate the base board with file content
	 * @param fileScan Scanner object to read the file with
	 * @throws FileNotFoundException if the file is invalid
	 */
	private void fillBoard(Scanner fileScan) throws FileNotFoundException {
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[row].length; col++) {
				if (fileScan.hasNext()) {
					board[row][col] = fileScan.next().charAt(0);
					if (board[row][col] == START) {
						startingPoint = new Point(row, col);
					}
					if (board[row][col] == END) {
						endingPoint = new Point(row, col);
					}
				}
			}
		}
	}

	/**
	 * Copy constructor - duplicates original board
	 * 
	 * @param original
	 *            board to copy
	 */
	public CircuitBoard(CircuitBoard original) {
		board = original.getBoard();
		startingPoint = new Point(original.startingPoint);
		endingPoint = new Point(original.endingPoint);
		rows = original.numRows();
		cols = original.numCols();
	}

	/**
	 * utility method for copy constructor
	 * 
	 * @return copy of board array
	 */
	private char[][] getBoard() {
		char[][] copy = new char[board.length][board[0].length];
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[row].length; col++) {
				copy[row][col] = board[row][col];
			}
		}
		return copy;
	}

	public char[][] getBaseBoard(){
		return getBoard();
	}
	/**
	 * Return the char at board position x,y
	 * 
	 * @param row
	 *            row coordinate
	 * @param col
	 *            col coordinate
	 * @return char at row, col
	 */
	public char charAt(int row, int col) {
		return board[row][col];
	}

	/**
	 * Return whether given board position is open
	 * 
	 * @param row
	 * @param col
	 * @return true if position at (row, col) is open
	 */
	public boolean isOpen(int row, int col) {
		if (row < 0 || row >= board.length || col < 0 || col >= board[row].length) {
			return false;
		}
		return board[row][col] == OPEN;
	}

	/**
	 * Set given position to be a 'T'
	 * 
	 * @param row
	 * @param col
	 * @throws OccupiedPositionException
	 *             if given position is not open
	 */
	public void makeTrace(int row, int col) {
		if (isOpen(row, col)) {
			board[row][col] = TRACE;
		} else {
			throw new OccupiedPositionException("row " + row + ", col " + col + " contains '" + board[row][col] + "'");
		}
	}

	/** @return starting Point */
	public Point getStartingPoint() {
		return new Point(startingPoint);
	}

	/**
	 * Gather all points adjacent to the provided coordinates
	 * 
	 * @param x
	 *            x position of the coordinates
	 * @param y
	 *            y position of the coordinates
	 * @return all points adjacent to (x,y)
	 */
	public ArrayList<Point> adjacentPoints(int x, int y) {
		ArrayList<Point> ret = new ArrayList<>();
		Point source = new Point(x, y);
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				Point destination = new Point(i, j);
				if (TraceState.adjacent(source, destination) && isOpen(i, j)) {
					ret.add(destination);
				}
			}
		}
		return ret;
	}

	/** @return ending Point */
	public Point getEndingPoint() {
		return new Point(endingPoint);
	}

	/** @return number of rows in this CircuitBoard */
	public int numRows() {
		return rows;
	}

	/** @return number of columns in this CircuitBoard */
	public int numCols() {
		return cols;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder str = new StringBuilder();
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[row].length; col++) {
				str.append(board[row][col] + " ");
			}
			str.append("\n");
		}
		return str.toString();
	}
}
