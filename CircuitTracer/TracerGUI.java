import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * TracerGUI draws and display the solution for the provided CircuitBoard
 * 
 * @author kanna
 *
 */
@SuppressWarnings("serial")
public class TracerGUI extends JPanel {
	// A cell with 'T' indicates a valid move (i.e it's part of the path)
	private final char TRACE = 'T';
	private ArrayList<TraceState> bestPathSolutions;
	private JList<String> solutionList;
	private JMenuItem quitAction;
	private JMenuItem aboutAction;
	private JPanel gridPanel;
	// private JButton loadFileButton;
	JLabel grid[][];

	/**
	 * CONSTRUCTOR: draw and display the best paths on the provided circut board
	 * 
	 * @param bestPaths
	 *            best path solutions for the provided board
	 * @param originalCircuit
	 *            board that the best path solution corresponds to
	 */
	public TracerGUI(ArrayList<TraceState> bestPaths, CircuitBoard originalCircuit) {
		bestPathSolutions = bestPaths;
		quitAction = new JMenuItem("Quit");
		aboutAction = new JMenuItem("About");
		setLayout(new BorderLayout());
		initDisplayWindow();

		// show the original grid on start up with no visible path
		initGridPanel(originalCircuit, false);
		displayGUI();
	}

	/**
	 * Create JFrame to display the GUI
	 */
	private void displayGUI() {
		JFrame frame = new JFrame("Circuit Tracer");
		frame.setPreferredSize(new Dimension(700, 600));
		frame.setJMenuBar(initMenubar());
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.add(this);
		frame.pack();
		// place window in center of screen instead upper left
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	/**
	 * Populate solution name
	 * @return solution name
	 */
	private String[] fillListData() {
		String[] strArry = new String[bestPathSolutions.size()];
		for (int i = 0; i < strArry.length; i++) {
			strArry[i] = "Solution " + (i + 1);
		}
		return strArry;
	}

	/**
	 * Set up right panel to display list
	 */
	private void initDisplayWindow() {
		JPanel displayPanel = new JPanel();
		// aling content vertically
		displayPanel.setLayout(new BoxLayout(displayPanel, BoxLayout.Y_AXIS));

		solutionList = new JList<>(fillListData());
		solutionList.setToolTipText("Select a solution to view on the grid");
		solutionList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		solutionList.setLayoutOrientation(JList.VERTICAL);
		solutionList.addListSelectionListener(new ListListener());

		JScrollPane jScrollPane = new JScrollPane(solutionList);
		// jScrollPane.setPreferredSize(new Dimension(120, 80));
		jScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

		displayPanel.add(jScrollPane);
		displayPanel.add(initInfoPanel());
		add(displayPanel, BorderLayout.EAST);
	}

	/**
	 * Set up right JPanel to display usage info
	 * 
	 * @return the setup JPanel
	 */
	private JPanel initInfoPanel() {
		// main panel alings content vertically
		JPanel mainInfoPanel = new JPanel();
		mainInfoPanel.setLayout(new BoxLayout(mainInfoPanel, BoxLayout.Y_AXIS));

		// labels are align left to right by default
		JPanel labelPanel = new JPanel();
		final JLabel START_POSITION = new JLabel("START");
		START_POSITION.setBackground(Color.MAGENTA);
		START_POSITION.setOpaque(true);

		final JLabel DESTINATION = new JLabel("END");
		DESTINATION.setBackground(Color.CYAN);
		DESTINATION.setOpaque(true);

		final JLabel PATH = new JLabel("PATH");
		PATH.setBackground(Color.GREEN);
		PATH.setOpaque(true);

		labelPanel.add(PATH);
		labelPanel.add(START_POSITION);
		labelPanel.add(DESTINATION);

		JPanel buttonPanel = new JPanel();
		// loadFileButton = new JButton("Load file");
		// loadFileButton.setEnabled(false);
		// //loadFileButton.addActionListener(new ButtonListener());
		// buttonPanel.add(loadFileButton);

		mainInfoPanel.add(labelPanel);
		mainInfoPanel.add(buttonPanel);
		mainInfoPanel.add(Box.createRigidArea(new Dimension(120, 270)));
		return mainInfoPanel;
	}

	/**
	 * Draw the circuit panel
	 * 
	 * @param originalCircuit
	 *            circuit board to draw
	 * @param showSolution
	 *            display the correct path
	 */
	private void initGridPanel(CircuitBoard originalCircuit, boolean showSolution) {
		if (gridPanel != null) {
			remove(gridPanel);
		}
		gridPanel = new JPanel(new GridLayout(originalCircuit.numRows(), originalCircuit.numCols()));
		grid = new JLabel[originalCircuit.numRows()][originalCircuit.numCols()];
		char[][] baseBoard = originalCircuit.getBaseBoard();

		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[i].length; j++) {
				grid[i][j] = new JLabel(Character.toString(baseBoard[i][j]));
				grid[i][j].setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));

				// aling content to center
				grid[i][j].setHorizontalAlignment(SwingConstants.CENTER);

				if (showSolution) {
					if (baseBoard[i][j] == TRACE) {
						grid[i][j].setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, Color.GREEN));
					}
				}
				gridPanel.add(grid[i][j]);
			}
		}
		// highlight start & finish position on grid
		grid[originalCircuit.getStartingPoint().x][originalCircuit.getStartingPoint().y].setBackground(Color.MAGENTA);
		grid[originalCircuit.getEndingPoint().x][originalCircuit.getEndingPoint().y].setBackground(Color.CYAN);
		add(gridPanel, BorderLayout.CENTER);
		revalidate();
	}

	/**
	 * Set up navigation menu
	 * 
	 * @return the setup menu with File and Help
	 */
	private JMenuBar initMenubar() {
		JMenuBar menuBar = new JMenuBar();
		quitAction.addActionListener(new MenuListener());
		aboutAction.addActionListener(new MenuListener());

		// Define and add two drop down menu to the menu
		JMenu fileMenu = new JMenu("File");
		JMenu helpMenu = new JMenu("Help");
		menuBar.add(fileMenu);
		menuBar.add(helpMenu);

		fileMenu.add(quitAction);
		helpMenu.add(aboutAction);
		return menuBar;
	}

	private class MenuListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			JMenuItem source = (JMenuItem) e.getSource();
			if (source.getText().equals("Quit")) {
				System.exit(0);
			} else {
				JOptionPane.showMessageDialog(gridPanel, "Circuit Tracer \nWritten by Prince Kannah (THE BigCheeze)",
						"About Circuit Tracer", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}

	/*
	 * private class ButtonListener implements ActionListener {
	 * 
	 * @Override public void actionPerformed(ActionEvent e) { JFileChooser
	 * chooser = new JFileChooser("."); boolean invalidFormat = false; int
	 * status = chooser.showOpenDialog(null); File file = null;
	 * 
	 * do { if (status == JFileChooser.APPROVE_OPTION) { file =
	 * chooser.getSelectedFile(); }
	 * 
	 * if (status == JFileChooser.CANCEL_OPTION) { break; } // if file is null
	 * then a file was not selected (user cancel out // of // the prompt) if
	 * (file != null) { try { CircuitBoard board = new
	 * CircuitBoard(file.getPath()); initGridPanel(board, false);
	 * runTrace(board); } catch (Exception exp) { invalidFormat = true;
	 * JOptionPane.showMessageDialog(gridPanel, exp.toString(), "Invalid file",
	 * JOptionPane.ERROR_MESSAGE); status = chooser.showOpenDialog(null); } } }
	 * while (invalidFormat); } }
	 * 
	 * private void runTrace(CircuitBoard board) { // get open points adjacent
	 * to our starting position ArrayList<Point> openStart =
	 * board.adjacentPoints(board.getStartingPoint().x,
	 * board.getStartingPoint().y); Storage<TraceState> stateStore = new
	 * Storage<>(Storage.DataStructure.queue); int solutionCount = 0;
	 * 
	 * // create new TraceState starting at the open positions for (Point point
	 * : openStart) { stateStore.store(new TraceState(board, point.x, point.y));
	 * }
	 * 
	 * while (!stateStore.isEmpty()) { // get a possible valid state from the
	 * pile TraceState state = stateStore.retreive(); if (state.isComplete()) {
	 * if (state.pathLength() == getShortestPath()) {
	 * bestPathSolutions.add(state); solutionCount++; } else if
	 * (state.pathLength() < getShortestPath()) { bestPathSolutions.clear();
	 * bestPathSolutions.add(state); solutionCount = 0; solutionCount++; } }
	 * else { ArrayList<Point> adjacent =
	 * state.getBoard().adjacentPoints(state.getRow(), state.getCol()); for
	 * (Point point : adjacent) { stateStore.store(new TraceState(state,
	 * point.x, point.y)); } } //
	 * grid[state.getRow()][state.getCol()].setBorder(BorderFactory.
	 * createMatteBorder(2, 2, 2, 2, Color.RED));
	 * initGridPanel(state.getBoard(), true); } }
	 * 
	 * private int getShortestPath() { int shortestPath = Integer.MAX_VALUE; for
	 * (TraceState traceState : bestPathSolutions) { if (traceState.pathLength()
	 * < shortestPath) { shortestPath = traceState.pathLength(); } } return
	 * shortestPath; }
	 */

	/**
	 * List listener class. Update grid view when a new solution is selected
	 * 
	 * @author kanna
	 *
	 */
	private class ListListener implements ListSelectionListener {
		@Override
		public void valueChanged(ListSelectionEvent e) {
			int selectionIndex = solutionList.getSelectedIndex();
			initGridPanel(bestPathSolutions.get(selectionIndex).getBoard(), true);
		}
	}
}
