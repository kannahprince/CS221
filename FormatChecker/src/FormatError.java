/**
 * FormatError is a wrapper class for possible errors that could be encountered
 * when validating a file
 * 
 * @author kanna
 * @version 1.0
 * @see FormatChecker
 */
public class FormatError {
	private String exception;

	/**
	 * CONSTRUCTOR: Create a new FormatError with the specified exception or
	 * message
	 * 
	 * @param e
	 *            exception or message that is causing this error
	 */
	public FormatError(String e) {
		exception = e;
	}

	/**
	 * Retrieve the error message associated with this error
	 * 
	 * @return the associated error message
	 */
	public String getException() {
		return exception;
	}

	public String toString() {
		return exception;
	}
}
