import java.io.File;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * FormatChecker checks the specified files to make sure they meet the following
 * file format:
 * <ul>
 * 		<li>The first row contains two white-space-separated, positive integers.</li>
 * 			<ul>
 * 				<li>The first integer specifies the number of rows in a grid.</li>
 * 				<li>The second integer specifies the number of columns in the grid.</li>
 *		   </ul>
 * <li>Each subsequent row represents one row of the grid and should contain
 * exactly one white-space-separated double value for each grid column</li>
 * </ul>
 * For example:
 * <table>
 * 	<tr>
 * 		<td>5 6</td>
 * 	</tr>
 * 	<tr>
 * 		<td>2.5 0 1 0 0 0</td>
 * 	</tr>
 *  <tr>
 * 		<td>0 -1 4 0 0 0</td>
 * 	</tr>
 * <tr>
 * 		<td>0 0 0 0 1.1 0</td>
 * </tr>
 * <tr>
 * 		<td>0 2 0 0 5 0</td>
 * </tr>
 * <tr>
 * 		<td>0 -3.14 0 0 0 0</td>
 * </tr>
 * </table>
 * @author kanna
 * @version 1.0
 */
public class FormatChecker {
	/*
	 * The number of dimension cannot exceed this value. We only care about row
	 * & col; anything more the file is invalid
	 */
	private static final int MAX_GRID_DIMENSION = 2;
	private static int row, col;
	private static String file;

	/**
	 * Validate the specified file
	 * 
	 * @param fileName
	 *            the grid file to validate
	 * @return <code>FormatError</code> if an error is encountered; null
	 *         otherwise
	 * @see FormatError
	 */
	public static FormatError check(String fileName) {
		try {
			file = fileName;
			Scanner firstLineScanner = new Scanner(new File(file));
			// get first line and remove all white space to
			// get accurate token count
			String firstLine = firstLineScanner.nextLine().replaceAll(" ", "");
			firstLineScanner.close();
			if (firstLine.length() != MAX_GRID_DIMENSION) {
				return new FormatError("Grid dimensions exceed the expected max (" + MAX_GRID_DIMENSION
						+ "). Specified: " + firstLine.length());
			}

			FormatError error = readRow();
			if (error != null) {
				return error;
			}

			error = readCol();
			if (error != null) {
				return error;
			}

			error = rowMismatch();
			if (error != null) {
				return error;
			}
			
			error = colMismatch();
			if (error != null) {
				return error;
			}

			error = readFile();
			if (error != null) {
				return error;
			}
			// returning null means the file is valid as no errors were found
			return null;
		} catch (FileNotFoundException e) {
			return new FormatError(e.toString());
		}
	}

	/**
	 * MAIN method: valid the specified files
	 * @param args files to validate
	 */
	public static void main(String[] args) {
		final int EXIT_ERROR_CODE = 1;
		if (args.length < 1) {
			System.out.println("Usage: \n\t" + "java FormatChecker file1 [file2 ... fileN]");
			System.exit(EXIT_ERROR_CODE);
		}

		for (int i = 0; i < args.length; i++) {
			System.out.println(args[i]);
			FormatError e = check(args[i]);
			if (e != null) {
				System.out.println(e.getException());
			} else
				System.out.println("VALID");
		}
	}

	/**
	 * Check the columns in the grid to determine if they match the specified
	 * dimenstion
	 * 
	 * @return <code>FormatError</code> if the grid column does not match the
	 *         specified column; null otherwise
	 * @throws FileNotFoundException
	 *             if the specified file is invalid
	 * @see FormatError
	 */
	private static FormatError colMismatch() throws FileNotFoundException {
		Scanner scan = new Scanner(new File(file));
		// skip row & col since those have been read
		scan.nextLine();
		while (scan.hasNext()) {
			String[] line = scan.nextLine().split(" ");
			if (col != line.length) {
				scan.close();
				return new FormatError(
						"Grid column does match the expected max (" + col + ")." + " Grid col: " + line.length);
			}
		}
		scan.close();
		return null;
	}

	/**
	 * Capture the grid column from the file
	 * 
	 * @return <code>FormatError</code> if an error is encountered; null
	 *         otherwise
	 * @throws FileNotFoundException
	 *             if the specified file is invalid
	 * @see FormatError
	 */
	private static FormatError readCol() throws FileNotFoundException {
		Scanner dimScanner = new Scanner(new File(file));
		// skip row
		dimScanner.nextInt();
		try {
			col = Integer.parseInt(dimScanner.next());
			dimScanner.close();
			return null;
		} catch (NumberFormatException e) {
			dimScanner.close();
			return new FormatError(e.toString());
		} catch (InputMismatchException e) {
			dimScanner.close();
			return new FormatError(e.toString());
		}
	}

	/**
	 * Read the body of the file
	 * 
	 * @return <code>FormatError</code> if any error is encountered; null
	 *         otherwise
	 * @throws FileNotFoundException
	 *             if the specified file is invalid
	 * @see FormatError
	 */
	private static FormatError readFile() throws FileNotFoundException {
		Scanner scan = new Scanner(new File(file));
		// skip over row & col since
		// the scanner is reset to
		// the beginning of the file
		scan.nextLine();
		try {
			while (scan.hasNext()) {
				String str = scan.next();
				Double.parseDouble(str);
			}
			scan.close();
		} catch (NumberFormatException e) {
			scan.close();
			return new FormatError(e.toString());
		} catch (InputMismatchException e) {
			scan.close();
			return new FormatError(e.toString());
		}
		return null;
	}

	/**
	 * Capture the grid row from the file
	 * 
	 * @return <code>FormatError</code> if an error is encountered; null
	 *         otherwise
	 * @throws FileNotFoundException
	 *             if the specified file is invalid
	 * @see FormatError
	 */
	private static FormatError readRow() throws FileNotFoundException {
		Scanner dimScanner = new Scanner(new File(file));
		try {
			row = Integer.parseInt(dimScanner.next());
			dimScanner.close();
			return null;
		} catch (NumberFormatException e) {
			dimScanner.close();
			return new FormatError(e.toString());
		} catch (InputMismatchException e) {
			dimScanner.close();
			return new FormatError(e.toString());
		}
	}

	/**
	 * Check the rows in the grid to determine if they match the specified
	 * dimenstion
	 * 
	 * @return <code>FormatError</code> if the grid row does not match the
	 *         specified column; null otherwise
	 * @throws FileNotFoundException
	 *             if the specified file is invalid
	 * @see FormatError
	 */
	private static FormatError rowMismatch() throws FileNotFoundException {
		int lineCount = 0;
		Scanner scan = new Scanner(new File(file));
		// skip row & col since those have been read
		scan.nextLine();
		while (scan.hasNext()) {
			scan.nextLine();
			lineCount++;
		}
		scan.close();
		if (row != lineCount) {
			return new FormatError(
					"Grid row does not match the expected max (" + row + ")." + " Grid row: " + lineCount);
		}
		return null;
	}
}
